<?php

// Plugin Name: MM Track Leads
// Description: Sends leads to the client CRM, according to the Client Code and Site Code that are defined in general settings.
// Client Code and Site Code must match what is defined in CDE for this client.

add_action('admin_init', 'register_fields');
add_action('gform_after_submission', 'createLeadsAfterFormSubmission', 10, 2);

function createLeadsAfterFormSubmission($entry, $form) {
    write_log('tracking lead for ' . $form['title']);
    $lead = array();
    $lead['source'] = 'Website';
    $lead['form'] = $form['title'];
    $lead['entry'] = $entry;

    if (isset($_COOKIE['__ss_tk'])) {
        $lead['trackingId'] = $_COOKIE['__ss_tk'];
    }

    if (isset($_COOKIE['traffic_src'])) {
        $lead['trafficSource'] = $_COOKIE['traffic_src'];
    }

    $fields = array();

    foreach ($form['fields'] as $f) {
        $fields[] = array(
            'id' => (string)$f->id,
            'type' => $f->type,
            'label' => $f->label
        );

        if (isset($f->inputs)) {
            foreach ($f->inputs as $input) {
                $fields[] = $input;
            }
        }
    }

    $lead['fields'] = $fields;
    post($lead);
}


function clientCode() {
    $clientCode = get_option('CLIENT_CODE');
    if (empty($clientCode) && defined('CLIENT_CODE')) $clientCode = CLIENT_CODE;

    return $clientCode;
}


function siteCode() {
    $siteCode = get_option('SITE_CODE');
    if (empty($siteCode) && defined('SITE_CODE')) $siteCode = SITE_CODE;

    return $siteCode;
}


/**
 * @param array $lead
 */
function post(array $lead) {
    $clientCode = clientCode();
    $siteCode = siteCode();

    if (empty($clientCode) || empty($siteCode) || 'CLIENT_CODE' === $clientCode || 'SITE_CODE' === $siteCode) {
        mail('steve@mobile-marketing.agency',
            'missing config for ' . get_option('blogname'),
            'client code=' . $clientCode . ', site code=' . $siteCode);
    }

    try {
        $ch = curl_init();

        write_log('got client data: ' . $clientCode . '_' . $siteCode);

        $url = "https://crm.mm-api.agency/$clientCode/$siteCode/lead";
        $json = json_encode($lead);
        write_log("sending $json to $url");

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Content-Length: ' . strlen($json)));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        if (curl_exec($ch) === false) {
            write_log(curl_error($ch));
        }

        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if ('200' != $status) {
            mail('steve@mobile-marketing.agency', 'failed lead submission', $status . ' for ' . $url . '\n\n' . $json);
            write_log($status . ' for ' . $url . '\n' . $json);
        };

        curl_close($ch);
    } catch (Exception $e) {
        write_log('error executing curl: ' . $e->getMessage());
        mail('steve@mobile-marketing.agency', 'curl issue on ' . $clientCode . '_' . $siteCode, $e->getMessage());
    }
}


function register_fields() {
    register_setting('general', 'CLIENT_CODE', 'esc_attr');
    register_setting('general', 'SITE_CODE', 'esc_attr');

    add_settings_field(
        'client_code',
        '<label for="client_code">' . __('Lead Tracking Client Code', 'CLIENT_CODE') . '</label>',
        'client_html',
        'general'
    );

    add_settings_field(
        'site_code',
        '<label for="site_code">' . __('Lead Tracking Site Code', 'SITE_CODE') . '</label>',
        'site_html',
        'general'
    );
}


function client_html() {
    $value = get_option('CLIENT_CODE');
    echo '<input type="text" id="client_code" name="CLIENT_CODE" value="' . esc_attr($value) . '" />';
}


function site_html() {
    $value = get_option('SITE_CODE');
    echo '<input type="text" id="site_code" name="SITE_CODE" value="' . esc_attr($value) . '" />';
}


if (!function_exists('write_log')) {
    function write_log($log) {
        if (is_array($log) || is_object($log)) {
            error_log(print_r($log, true));
        } else {
            error_log($log);
        }
    }
}



