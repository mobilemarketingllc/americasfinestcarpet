<?php
$CF7Validation     = new CF7Validation();
$all_rules         = $CF7Validation->getAllRules();
$field_types       = $CF7Validation->getFieldTypes();
$sites             = $CF7Validation->get_sites();
$example_rule_data = $CF7Validation->get_example_rule_data( true );

$all_rules = $CF7Validation->sort_for_display($all_rules);
// echo '<pre>' . print_r( $sites, true ) . '</pre>';
// echo '<pre>' . print_r( $all_rules, true ) . '</pre>';
?>
<h2><?php _e( 'Addon for CF7 Plug-in for WordPress', 'syn-custom' ) ?></h2>

<div>
	<p><?php _e( 'This plugin adds additional validation to Contact Form 7.<br><strong>Do not uninstall or delete this plugin or your website will not run properly.</strong>', 'syn-custom' ) ?></p>
</div>

<div>
	<ul>
		<li><strong><?php _e( 'Useful Tool:', 'syn-custom' ) ?></strong></li>
		<li><a href="http://regex101.com/" target="_blank"><?php _e( 'Regular Expression Tool', 'syn-custom' ) ?></a></li>
	</ul>
</div>

<h3><?php _e( 'Contact Form 7 Status:', 'syn-custom' ) ?>
	<?php if ( $CF7Validation->isCF7Active() ) : ?>
	<span class="good"><?php _e( '[ACTIVE]', 'syn-custom' ) ?></span>
	<?php else: ?>
	<span class="danger"><?php _e( '[NOT ACTIVE]', 'syn-custom' ) ?></span> <?php _e( 'Plugin must be active.', 'syn-custom' ) ?>
	<?php endif; ?></h3>

<div>
	<div class="add-rule-container">
		<p><button class="syncf7-btn-add-rule button ui-button ui-widget ui-corner-all ui-button-text-only" type="button"><?php _e( 'Add New Rule', 'syn-custom' ) ?></button>
        <span class="fr">
            Actions: 
            <span class="dashicons dashicons-edit"></span> Edit| 
            <span class="dashicons dashicons-no"></span> Delete | 
            <span class="dashicons dashicons-plus"></span> Duplicate
        </span>
        </p>

		<div class="syn hide syncf7-modal-edit" id="syncf7-modal-edit">
			<div class="row">
				<div class="column left">
					<h3><?php _e( 'Rule Data', 'syn-custom' ) ?></h3>

					<p class="syncf7-error txt-ctr"></p>
					<p class="syncf7-success txt-ctr"></p>
					<p class="validateTips"><a href="http://regex101.com/" target="_blank"><?php _e( 'Regular Expression Tool', 'syn-custom' ) ?></a></p>
					<p class="validateTips"><?php _e( 'All form fields are required.', 'syn-custom' ) ?></p>

					<form id="syncf7-modal-edit-form" name="syncf7-modal-edit-form" method="post">
						<input type="hidden" name="synaction" value="syncf7vadd">
						<input type="hidden" name="id" value="">

						<table>
							<tr>
								<td class="label-td"><label for="blog_id"><?php _e( 'Blog', 'syn-custom' ) ?></label></td>
								<td>
									<select class="ui-widget ui-corner-all" id="blog_id" name="blog_id">
										<option value="0"><?php _e( 'All blogs', 'syn-custom' ) ?></option>
										<?php foreach ( $sites as $site ) : ?>
										<option value="<?php echo $site['blog_id'] ?>"><?php echo $site['domain'] ?></option>
										<?php endforeach; ?>
									</select>
								</td>
							</tr>
							<tr>
								<td class="label-td"><label for="field_type"><?php _e( 'Field Type', 'syn-custom' ) ?></label></td>
								<td>
									<select class="ui-widget ui-corner-all" id="field_type" name="field_type">
										<?php foreach ( $field_types as $type ) : ?>
										<option value="<?php echo $type ?>"><?php echo $type ?></option>
										<?php endforeach; ?>
									</select>
								</td>
							</tr>
							<tr>
								<td class="label-td"><label for="field_name"><?php _e( 'Field Name', 'syn-custom' ) ?></label></td>
								<td><input type="text" name="field_name" id="field_name" value="" placeholder="<?php _e( 'Field Name or Multiple Names Delimited By Comma', 'syn-custom' ) ?>" class="text ui-widget-content ui-corner-all"></td>
							</tr>
							<tr>
								<td class="label-td"><label for="func"><?php _e( 'Function', 'syn-custom' ) ?></label></td>
								<td>
									<input type="text" name="func" id="func" value="" placeholder="<?php _e( 'Name of Custom Function', 'syn-custom' ) ?>" class="text ui-widget-content ui-corner-all"><br>
									<span class="note"><?php _e( 'Select a function from the column on the right, or enter your own', 'syn-custom' ) ?></span>
								</td>
							</tr>
							<tr>
								<td class="label-td"><label for="regex"><?php _e( 'Regular Expression', 'syn-custom' ) ?></label></td>
								<td>
									<input type="text" name="regex" id="regex" value="" placeholder="<?php _e( 'i.e. /.*/', 'syn-custom' ) ?>" class="text ui-widget-content ui-corner-all"><br>
									<span class="note"><?php _e( 'Select an expression from the column on the right, or enter your own', 'syn-custom' ) ?></span>
								</td>
							</tr>
							<tr>
								<td class="label-td"><label for="msg_err"><?php _e( 'Error Message', 'syn-custom' ) ?></label></td>
								<td><input type="text" name="msg_err" id="msg_err" value="" class="text ui-widget-content ui-corner-all"></td>
							</tr>
							<tr>
								<td class="label-td"><label for="rank"><?php _e( 'Priority', 'syn-custom' ) ?></label></td>
								<td><input type="number" name="rank" id="rank" value="" class="text ui-widget-content ui-corner-all"></td>
							</tr>
                            <tr>
                                <td></td>
                                <td id="error-message"></td>
                            </tr>
						</table>

						<p class="button-row">
                            <button class="button ui-button ui-widget ui-corner-all ui-button-text-only btn-save" type="submit"><?php _e( '<span class="syncf7-action-copy">Create</span> Rule', 'syn-custom' ) ?></button>
							<button class="syncf7-modal-close button ui-button ui-widget ui-corner-all ui-button-text-only btn-cancel" type="button"><?php _e( 'Cancel', 'syn-custom' ) ?></button>
						</p>

						<div class="clear"></div>
					</form>
				</div>

				<div class="column right">
					<h3><?php _e( 'Example Rule Data', 'syn-custom' ); ?></h3>

					<?php if ( ! empty( $example_rule_data ) ) : ?>
						<?php foreach ( $example_rule_data as $group => $rules ) : ?>
                        <div>
                            <h4 class="rule-list-title"><?php echo ucwords( $group ) ?></h4>
                            <ul class="<?php echo $group ?> example-rule-list hide ">
                                <?php foreach ( $rules as $rule ) : ?>
                                <li class="<?php echo $rule->rule_type ?> example-rule" data-rule-data="<?php echo $rule->rule_data ?>"><?php echo $rule->rule_name ?>
                                    <?php if ( ! empty( $rule->rule_description ) ) : ?>
                                    <br><span class="note"><?php echo $rule->rule_description ?></span>
                                    <?php endif; ?>
                                </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
						<?php endforeach; ?>
                        <div>
                            <h4 class="rule-list-title">Check boxes</h4>
                            <ul class="checkboxes example-rule-list hide">
                                <li class="example-rule" data-rule-data="">
                                    <span class="note">
                                        Check boxes do not work like other rules. They will only throw an error if they are not checked.
                                        No function is required and the regex must be a comma separated list of form ids to check. Check box
                                        names can not be comma separated, either. One rule per name.
                                        <pre>ex: Regular Expression: 563,785</pre>
                                    </span>
                                </li>
                            </ul>
                        </div>
					<?php else : ?>
					<p><?php _e( 'There is no example rule data available.', 'syn-custom' ) ?></p>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>

	<hr>

	<?php if ( 1 > count( $all_rules ) ) : ?>
	<p><?php _e( 'No Rules Found', 'syn-custom' ) ?></p>
	<?php
	else :
		/**
		 * @todo Clean this code block up / make it more efficient
		 */
		$guid   = '';
		$output = '';

		foreach ( $all_rules as $index => $rule ) :
			$guid_new = $rule['blog_id'] . ' | ' . strtoupper( $rule['field_type'] . ' | ' . $rule['field_name'] );

			if ( $guid != $guid_new ) :
				$guid = $guid_new;
				$output .= ! empty( $output ) ? '</tbody></table></fieldset><hr>' : '';
				$output .= '<fieldset><legend>' . $guid . '</legend><table class="rule-table">';
				$output .= '<thead><tr>';
				$output .= '<th>Actions</th>';

				foreach( $rule as $field => $value ) :
					$output .= '<th>' . ucwords( str_replace( '_', ' ', $field ) ) . '</th>';
				endforeach;

				$output .= '</tr></thead><tbody>';
			endif;

			$output .= '<tr>';
			// $output.= '<td>'.($index+1).'<span class="dashicons dashicons-no"></span></td>';
			$output .= '<td class="txt-ctr actions">';
            $output .= '<a title="Edit" class="synCF7EditRule" data-id="' . $rule['id'] . '"><span class="dashicons dashicons-edit"></span></a>';
            $output .= '<a class="synCF7DelRule" data-id="' . $rule['id'] . '" data-blog_id="' . $rule['blog_id'] . '" data-field_type="' . $rule['field_type'] . '" data-field_name="' . $rule['field_name'] . '"><span class="dashicons dashicons-no" title="Remove"></span></a>';
            $output .= '<a title="Duplicate" class="synCF7DuplicateRule" data-id="' . $rule['id'] . '"><span class="dashicons dashicons-plus"></span></a>';
            $output .= '</td>';
            
			foreach ( $rule as $field => $value ) :
				$class = in_array( $field, array( 'id', 'blog_id', 'field_type', 'field_name', 'func', 'timestamp', 'domain', 'rank' ) ) ? ' txt-ctr' : '';
				$output .= '<td class="cur-ptr' . $class . ' field_'.$field.'" data-id="' . $rule['id'] . '">' . htmlspecialchars( $value ) . '</td>';
			endforeach;

			$output .= '</tr>';
		endforeach;

		echo $output . '</tbody></table></fieldset>';
	endif;
	?>
</div>

<div class="syn hide syncf7-modal-del" id="syncf7-modal-del" title="WARNING">
	<p class="syncf7-error txt-ctr"></p>
	<p class="syncf7-success txt-ctr"></p>
	<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span><?php _e( 'Are you sure you want to delete the following rule?', 'syn-custom' ) ?></p>

	<table>
		<thead>
			<tr>
				<th><?php _e( 'Id', 'syn-custom' ) ?></th>
				<th><?php _e( 'Blog Id', 'syn-custom' ) ?></th>
				<th><?php _e( 'Field Type', 'syn-custom' ) ?></th>
				<th><?php _e( 'Field Name', 'syn-custom' ) ?></th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="syncf7-id"></td>
				<td class="syncf7-blog_id"></td>
				<td class="syncf7-field_type"></td>
				<td class="syncf7-field_name"></td>
			</tr>
		</tbody>
	</table>

	<form id="syncf7-modal-del-form" name="syncf7-modal-del-form" method="post">
		<input type="hidden" name="synaction" value="syncf7vdel">
		<input type="hidden" name="id" value="">
	</form>
</div>
