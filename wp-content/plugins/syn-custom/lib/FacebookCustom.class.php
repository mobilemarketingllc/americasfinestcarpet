<?php
/**
 * Synapse Custom FacebookCustom Class
 *
 * @category   WP Plugin
 * @package    Synapse Custom
 * @license    Synapse Marketing Solutions
 * @author     Andy Koo <akoo@synapseresults.com>
 * @copyright  Copyright (C) 2014 Synapse Marketing Solutions
 * @version    Release: 1.0
**/

class FacebookCustom {
	protected	$page_id = 'AmesTools1774';
	protected	$access_token = '264603637060774|r2jd2DLBnz7S6RRvfWbNcy9_djY';

	function __construct() {
		$this->curlTimeout = 20;
	}

	function getPosts() {
		# Get the JSON
		$url = 'https://graph.facebook.com/' . $this->page_id . '/posts?access_token=' . $this->access_token;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, $this->curlTimeout);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$dataJson = curl_exec($ch);
		curl_close($ch);

		return $dataJson;
	}
}
?>