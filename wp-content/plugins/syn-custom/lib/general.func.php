<?php
/**
 * Synapse General Functions Library
 *
 * @category	General Functions Library
 * @package		Synapse Custom Build
 * @license		Synapse Marketing Solutions
 * @author		Andy Koo, Todd Bookman
 * @copyright	Copyright (C) 2014 Synapse Marketing Solutions
 * @version		Release: Synapse Custom Build
 * @notes		Add, modify and/or delete for your build.
**/

function createStdObj($array, $recursive=false) {
	$obj = new stdClass();
	if (is_array($array)) {
		foreach ($array as $k=>$v) {
			if ($recursive && is_array($v)) {
				$obj->$k = createStdObj($v);
			} else {
				$obj->$k = $v;
			}
		}
	} elseif (is_object($array)) {
		$obj = $array;
	} else {
		$obj->$array = $array;
	}
	return $obj;
}


/*
Convert an object to an array
@param	object  $object The object to convert
@return	array
*/
function objectVar2Array($obj) {
	if (!is_object($obj) && !is_array($obj)) {
		return $obj;
	}
	if (is_object($obj)) {
		$obj = get_object_vars($obj);
	}
	return array_map('objectVar2Array', $obj);
}


// PHP array_replace_recursive (PHP 5 >= 5.3.0)
// Implemented for older PHP versions.
if (!function_exists('array_replace_recursive')) {
	function array_replace_recursive($array, $array1) {
		if (!function_exists('recurse')) {
			function recurse($array, $array1) {
				foreach ($array1 as $key => $value) {
					// create new key in $array, if it is empty or not an array
					if (!isset($array[$key]) || (isset($array[$key]) && !is_array($array[$key]))) {
						$array[$key] = array();
					}

					// overwrite the value in the base array
					if (is_array($value)) {
						$value = recurse($array[$key], $value);
					}
					$array[$key] = $value;
				}
				return $array;
			}
		}

		// handle the arguments, merge one by one
		$args = func_get_args();
		$array = $args[0];
		if (!is_array($array)) {
			return $array;
		}
		for ($i = 1; $i < count($args); $i++) {
			if (is_array($args[$i])) {
				$array = recurse($array, $args[$i]);
			}
		}
		return $array;
	}
}


// PHP str_getcsv (PHP 5 >= 5.3.0)
// Implemented for older PHP versions.
if (!function_exists('str_getcsv')) {
	function str_getcsv($input, $delimiter=',', $enclosure='"') {
		$fh = fopen('php://temp', 'r+');
		fwrite($fh, $input);
		rewind($fh);
		$array = fgetcsv($fh, 0, $delimiter, $enclosure);
		fclose($fh);
		return $array;
	}
}


if (!function_exists('str_lreplace')) {
	function str_lreplace($search, $replace, $subject) {
		$pos = strrpos($subject, $search);

		if($pos !== false) {
			$subject = substr_replace($subject, $replace, $pos, strlen($search));
		}

		return $subject;
	}
}


if (!function_exists('apache_request_headers')) {
    function apache_request_headers() {
        $headers = array();
        foreach($_SERVER as $key => $value) {
            if(substr($key, 0, 5) == 'HTTP_') {
                $headers[str_replace(' ', '-', ucwords(str_replace('_', ' ', strtolower(substr($key, 5)))))] = $value;
            }
        }
        return $headers;
    }
}


function clear_array($array, $clear_value = null) {
	foreach ($array as $k=>$v) {
		if (is_array($v)) {
			$array[$k] = clear_array($v, $clear_value);
		} else {
			$array[$k] = $clear_value;
		}
	}
	return $array;
}


function check_array_all_value_empty($array) {
	foreach ($array as $k=>$v) {
		if (is_array($v) && !check_array_all_value_empty($v)) {
			return false;
		} elseif (!empty($v)) {
			return false;
		}
	}
	return true;
}


function check_array_any_value_empty($array) {
	foreach ($array as $k=>$v) {
		if (is_array($v) && check_any_array_value_empty($v)) {
			return true;
		} elseif (empty($v)) {
			return true;
		}
	}
	return false;
}


function array_key_str_replace($str_o, $str_r, $array, $recursive=true) {
	if (is_array($array)) {
		foreach ($array as $k=>$v) {
			if (is_array($v) && $recursive) {
				$array[$k] = array_key_str_replace($str_o, $str_r, $v, $recursive);
			} else {
				$k_new = str_replace($str_o, $str_r, $k);
				$array[$k_new] = $v;
				if ($k_new!=$k) {
					unset($array[$k]);
				}
			}
		}
	}
	return $array;
}


function array_push_array(&$a1, $a2) {
	foreach ($a2 as $v) {
		$a1[] = $v;
	}
}


function is_array_hash($array) {
	// return $array !== array_values($array);
	return (bool)count(array_filter(array_keys($array), 'is_string'));
}


function inet2n($ip, $pack_bin=false) {
	if (false!==strpos($ip, '.')) {
		# ipv4
		$ip = $pack_bin ? pack('N',ip2long($ip)) : ip2long($ip);
	} elseif (false!==strpos($ip, ':')) {
		# ipv6
		$ip = explode(':', $ip);
		$res = str_pad('', (4*(8-count($ip))), '0000', STR_PAD_LEFT);
		foreach ($ip as $seg) {
			$res .= str_pad($seg, 4, '0', STR_PAD_LEFT);
		}
		$ip = $pack_bin ? pack('H'.strlen($res), $res) : $res;
	}
	return $ip;
}


function getRealIpAddr() {
	$ip = $_SERVER['REMOTE_ADDR'];
	if (isset($_SERVER['HTTP_SRC_IP']) && !empty($_SERVER['HTTP_SRC_IP'])) {
		// DealBurglar Environment
		$ip = $_SERVER['HTTP_SRC_IP'];
	} elseif (isset($_SERVER['HTTP_CLIENT_IP']) && !empty($_SERVER['HTTP_CLIENT_IP'])) {
		// Proxy
		$ip = $_SERVER['HTTP_CLIENT_IP'];
	} elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && !empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
		// Forwards
		$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	}
	return $ip;
}


/*
get the first ip and last ip from cidr(network id and mask length)
i will integrate this function into "Rong Framework" :)
@author admin@wudimei.com
@param string $cidr 56.15.0.6/16 , [network id]/[mask length]
@return array $ipArray = array( 0 =>"first ip of the network", 1=>"last ip of the network" );
                        Each element of $ipArray's type is long int,use long2ip( $ipArray[0] ) to convert it into ip string.
example:
list($long_startIp , $long_endIp) = getIpRange( "56.15.0.6/16" );
echo "start ip:" . long2ip( $long_startIp );
echo "<br />";
echo "end ip:" . long2ip( $long_endIp );
*/
function getIpRang($cidr) {
	list($ip, $mask) = explode('/', $cidr);

	$maskBinStr = str_repeat("1", $mask) . str_repeat("0", 32-$mask );			// net mask binary string
	$inverseMaskBinStr = str_repeat("0", $mask) . str_repeat("1",  32-$mask );	// inverse mask

	$ipLong = ip2long($ip);
	$ipMaskLong = bindec($maskBinStr);
	$inverseIpMaskLong = bindec($inverseMaskBinStr);
	$netWork = $ipLong & $ipMaskLong;

	// $start = $netWork+1;						// ignore network ID(eg: 192.168.1.0)
	$start = $netWork;

	// $end = ($netWork | $inverseIpMaskLong)-1;	// ignore brocast IP(eg: 192.168.1.255)
	$end = ($netWork | $inverseIpMaskLong);
	return array($start, $end);
}


function check_ip_in_range($ip, $range=null) {
	if (is_null($range)) {
		$range = array(
			'172.16.1.0/24',	// Synapse
			'64.58.224.240/32'	// DOBA
		);
	}
	$ip = ip2long($ip);
	foreach ($range as $i=>$ip_range) {
		list($ip_start, $ip_end) = getIpRang($ip_range);
		if ($ip>=$ip_start && $ip<=$ip_end) {
			return true;
		}
	}
	return false;
}


function parseDate($date, &$mon, &$day, &$year) {
	$date = str_replace(array('/','-'), '', $date);
	$year = substr($date, 0, 4);
	$mon = substr($date, 4, 2);
	$day = substr($date, 6, 2);
	if (!checkdate($mon, $dat, $year)) {
		return false;
	}
	return $date;
}


function format_date($date, $db_format=false) {
	$epoch = strtotime(str_replace(array('-','.'), '/', $date));
	if (10>strlen($date)) {
		return $date;
	} elseif (10==strlen($date)) {
		return $db_format?date('Y-m-d', $epoch):date('m-d-Y T', $epoch);
	} else {
		return $db_format?date('Y-m-d H:i:s', $epoch):date('m-d-Y h:i:s A T', $epoch);
	}
}


/*******************************************************************************
Validate an email address.
Provide email address (raw input)
Returns true if the email address has the email
address format and the domain exists.
*******************************************************************************/
function validEmail($email_address) {
	$isValid = true;
	$atIndex = strrpos($email_address, '@');
	if (is_bool($atIndex) && !$atIndex) {
		$isValid = false;
	} else {
		$domain = substr($email_address, $atIndex+1);
		$local = substr($email_address, 0, $atIndex);
		$localLen = strlen($local);
		$domainLen = strlen($domain);
		if ($localLen < 1 || $localLen > 64) {
			// local part length exceeded
			$isValid = false;
		} elseif ($domainLen < 1 || $domainLen > 255) {
			// domain part length exceeded
			$isValid = false;
		} elseif ($local[0] == '.' || $local[$localLen-1] == '.') {
			// local part starts or ends with '.'
			$isValid = false;
		} elseif (preg_match('/\.\./', $local)) {
			// local part has two consecutive dots
			$isValid = false;
		} elseif (!preg_match('/^[\p{L}0-9\-\.]+$/', $domain)) {
			// character not valid in domain part
			$isValid = false;
		} elseif (preg_match('/\.\./', $domain)) {
			// domain part has two consecutive dots
			$isValid = false;
		} elseif (!preg_match('/^(\.|[\p{L}0-9!#%&`_=\\/$\'*+?^{}|~.-])+$/', stripslashes($local))) {
			// character not valid in local part unless
			// local part is quoted
			if (!preg_match('/^"(\"|[^"])+"$/', stripslashes($local))) {
				$isValid = false;
			}
		}
		if ($isValid && (!checkdnsrr($domain,'MX') && !checkdnsrr($domain,'A'))) {
			// domain not found in DNS
			$isValid = false;
		}
	}
        
        if (!filter_var($email_address, FILTER_VALIDATE_EMAIL)) {
            $isValid = false;
        }
        
	return $isValid;
}


function blacklisted($ip) {
	// Get DNSBL at http://www.dnsbl.info/dnsbl-list.php
	// sbl.spamhaus.org <= Check just for SPAM
	// zen.spamhaus.org <= Takes too long.
	// list.dsbl.org <= Takes too long.
	// ip.v4bl.org
	// b.barracudacentral.org
	// bl.spamcop.net
	// dnsbl.sorbs.net
	// virus.rbl.jp
	// cbl.abuseat.org
    // $dnsbl_lists = array('b.barracudacentral.org','bl.spamcop.net');
	// TODO: Need to modify for IPv6

	$ip_list = explode(',', $ip);	// Accept string or comma separate string

	foreach($ip_list as $ip) {
		$flag = false;
		$dnsbl_lists = array('sbl.spamhaus.org');
		if (!preg_match('/^([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}){1}$/', $ip)) {
			$ip = gethostbyname($ip);
		}
		if ($ip && preg_match('/^([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}){1}$/', $ip)) {
			$reverse_ip = implode('.', array_reverse(explode('.', $ip)));
			$on_win = substr(PHP_OS, 0, 3) == 'WIN' ? true : false;
			foreach ($dnsbl_lists as $dnsbl_list) {
				if (function_exists('checkdnsrr')) {
					if (checkdnsrr($reverse_ip.'.'.$dnsbl_list.'.', 'MX')) {
						$flag = true;
					}
					if (checkdnsrr($reverse_ip.'.'.$dnsbl_list.'.', 'A')) {
						$flag = true;
					}
				} elseif ($on_win) {
					$lookup = '';
					@exec('nslookup -type=A '.$reverse_ip.'.'.$dnsbl_list.'.', $lookup);
					foreach ($lookup as $line) {
						if (strstr($line, $dnsbl_list)) {
							$flag = true;
						}
					}
				}
			}
			if (!$flag) {
				return false;
			}
		}
	}

	return true;
}


function file_code_error($code){
	switch ($code) {
		case UPLOAD_ERR_INI_SIZE:
			// php.ini - upload_max_filesize
			$message = "The uploaded file exceeds max upload file size";
			break;
		case UPLOAD_ERR_FORM_SIZE:
			// html form - MAX_FILE_SIZE
			$message = "The uploaded file exceeds max upload file size";
			break;
		case UPLOAD_ERR_PARTIAL:
			$message = "The uploaded file was only partially uploaded";
			break;
		case UPLOAD_ERR_NO_FILE:
			$message = "No file was uploaded";
			break;
		case UPLOAD_ERR_NO_TMP_DIR:
			$message = "Missing a temporary folder";
			break;
		case UPLOAD_ERR_CANT_WRITE:
			$message = "Failed to write file to disk";
			break;
		case UPLOAD_ERR_EXTENSION:
			$message = "File upload stopped by extension";
			break;

		default:
			$message = "Unknown upload error";
			break;
	}
	return $message;
}


function makeUrlArray(){
	$urlArray = $_SERVER['REQUEST_URI'];
	if (strpos($urlArray,'?') !== false) {
		$urlArray = substr($urlArray, 0, strrpos($urlArray, '?'));
	}
	$urlArray = explode('/', $urlArray);
	return $urlArray;
}

/*
 * string contains function
 */
function str_contains($needle, $haystack) {
    return strpos($haystack, $needle) !== FALSE;
}

/*
 * Shortcut for outputting a variable
 */
function p_out($output) {
    echo '<pre>';
    var_dump($output);
    echo '</pre>';
}