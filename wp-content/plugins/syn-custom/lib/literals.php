<?php
define('WEBSITE_NAME'		, 'Ames');
# Production domain.
define('DOMAIN'				, isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : $_SERVER['SERVER_NAME']);

# Set the flag for production or development.
define('FLAG_PROD'			, false!==strpos(constant('DOMAIN'), 'synapseresults.com') ? false : true);

# Set debug flag:
# 	Based on your development domain and error handling configurations.
define('DEBUG'				, true);

define('PATH_BASE'			, dirname(dirname(__FILE__)));	//	/directory/parentdirectory
define('PATH_LIB'			, constant('PATH_BASE').DIRECTORY_SEPARATOR.'lib');
define('PATH_CSS'			, constant('PATH_BASE').DIRECTORY_SEPARATOR.'css');
define('PATH_JS'			, constant('PATH_BASE').DIRECTORY_SEPARATOR.'js');
define('PATH_IMG'			, constant('PATH_BASE').DIRECTORY_SEPARATOR.'img');
define('PATH_TPL'			, constant('PATH_BASE').DIRECTORY_SEPARATOR.'tpl');

# Set based on server configuration and fallback to folder structure
define('PATH_DOC_ROOT'		, !empty($_SERVER['DOCUMENT_ROOT']) ? $_SERVER['DOCUMENT_ROOT'] : DIRECTORY_SEPARATOR.'www'.DIRECTORY_SEPARATOR.constant('DOMAIN').DIRECTORY_SEPARATOR.'htdocs');

// PHP_VERSION_ID is available as of PHP 5.2.7, if our version is lower than that, then emulate it.
if (!defined('PHP_VERSION_ID')) {
	$version = explode('.', PHP_VERSION);
	define('PHP_VERSION_ID', ($version[0] * 10000 + $version[1] * 100 + $version[2]));
}
if (50300>constant('PHP_VERSION_ID')) {
	// PHP < 5.3 | Add missing constants.
	define('CURLOPT_IPRESOLVE', 113);
	define('CURL_IPRESOLVE_V4', 1);
}

# Add your custom paths here.
# Example:
# 	define('PATH_CUSTOM_LIB', PATH_BASE.'custom'.DIRECTORY_SEPARATOR);

# Custom Regular Express Patterns. (PECL Based)
# Add, Modify or Remove based on your needs for the website.
# Source:	http://www.regular-expressions.info/
# Tool:	http://regex101.com
// define('REX_01'				, '/^(0|1){1}$/');
// define('REX_ALPHA_DIGITS'	, '/^(?i)[a-z0-9]+$/');
// define('REX_INT_POS'		, '/^[0-9]+$/');
// define('REX_INT'			, '/^(\-)?[0-9]+$/');
// define('REX_DECIMAL'		, '/^(\-)?[0-9]+(\.[0-9]+)?$/');
// define('REX_HASH_SHA256'	, '/^[a-z0-9]{64}$/');
// define('REX_CURRENCY'		, '/^(\-)?[0-9]+(\.[0-9]{2})?$/');
// define('REX_DATE'			, '/^[0-9]{4}\-[0-9]{2}\-[0-9]{2}$/');
// define('REX_DATETIME'		, '/^[0-9]{4}\-[0-9]{2}\-[0-9]{2}\s[0-9]{2}:[0-9]{2}:[0-9]{2}$/');
// define('REX_EMAIL'			, '/^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*\.(([0-9]{1,3})|([a-zA-Z]{2,3})|(aero|coop|info|museum|name))$/');
// define('REX_NAME'			, '/^(?i)[a-z][a-z0-9#'."'".'\"\,\/\s\-\.\&]{1,64}$/');
// define('REX_NAME50'			, '/^(?i)[a-z][a-z0-9#'."'".'\"\,\/\s\-\.\&]{1,50}$/');
// define('REX_NAME100'		, '/^(?i)[a-z][a-z0-9#'."'".'\"\,\/\s\-\.\&]{1,100}$/');
// define('REX_STR'			, '/^(?i)[\x{0020}-\x{007E}]+$/');
// define('REX_STR50'			, '/^(?i)[\x{0020}-\x{007E}]{1,50}$/');
// define('REX_STR100'			, '/^(?i)[\x{0020}-\x{007E}]{1,100}$/');
// define('REX_SEARCH' 		, '/^(?i)[a-z][a-z0-9\-\,\.\@\s'."'".']+$/');
// define('REX_TEXT'			, '/^(?i)[\x{0000}\x{0009}-\x{000D}\x{0020}-\x{007E}]*$/');
// define('REX_US_PHONE'		, '/^(?:(?:\+?1\s*(?:[.-]\s*)?)?(?:\(\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\s*\)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?$/');
// define('REX_US_STREET_ADDR'	, '/^(?i)[a-z0-9\s\.\-\#'."'".']{1,100}$/');
// define('REX_US_CITY'		, '/^(?i)[a-z0-9\s\.\-'."'".']{1,50}$/');
// define('REX_US_STATE'		, '/^(?i)[a-z]{2}$/');
// define('REX_US_POSTAL_CODE'	, '/^(?!0{5})(\d{5})(?!-?0{4})(-?\d{5})?(-\d{4})?$/');
// define('REX_YN'				, '/^(Y|N){1}$/');
// define('REX_PHONE'			, '/^[0-9]{7,20}$/');
// define('REX_STREET_ADDR'	, '/^(?i)[a-z0-9\s\.\-\#'."'".']{1,100}$/');
// define('REX_CITY'			, '/^(?i)[a-z0-9\s\.\-'."'".']{1,50}$/');
// define('REX_ZIP'			, '/^(?i)[a-z0-9\s\-]{5,15}$/');
// define('REX_USER_LIB_TYPE'  , '/^(page|doc){1}$/');


# Autoload class files.
function SynCustomAutoload($classname) {
	# Can't use __DIR__ as it's only in PHP 5.3+
	$filename = dirname(__FILE__).DIRECTORY_SEPARATOR.$classname.'.class.php';
	if (is_readable($filename)) {
		require_once $filename;
	}
}
if (version_compare(PHP_VERSION, '5.1.2', '>=')) {
	# SPL autoloading was introduced in PHP 5.1.2
	if (version_compare(PHP_VERSION, '5.3.0', '>=')) {
		spl_autoload_register('SynCustomAutoload', true, true);
	} else {
		spl_autoload_register('SynCustomAutoload');
	}
} else {
	# Fall back to traditional autoload for old PHP versions
	# @param string $classname The name of the class to load
	function __autoload($classname) {
		SynCustomAutoload($classname);
	}
}
?>