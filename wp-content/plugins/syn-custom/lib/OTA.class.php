<?php
/**
 * Synapse Custom OTA Class
 *
 * @category   WP Plugin
 * @package    Synapse Custom
 * @license    Synapse Marketing Solutions
 * @author     Andy Koo <akoo@synapseresults.com>
 * @copyright  Copyright (C) 2014 Synapse Marketing Solutions
 * @version    Release: 1.0
 * @notes
 
ORACLE TRANSPORT AGENT
http://docs.oracle.com/cd/E18727_01/doc.121/e12954/T319774T319783.htm


ORACLE TRANSPORT AGENT POST MESSAGE
===================================
Two OTA servers communicate by sending and receiving a series of name/value pairs in the HTTP body of an HTTP POST/RESPONSE. Following is an example post from the sending OTA server (Note: the header authorization encryption follows the W3C standard):

HTTP Header
Http-Version: HTTP/1.1
Authorization: Digest username="myusername",
realm="testrealm@host.com",
 nonce="dcd98b7102dd2f0e8b11d0f600bfb0c093",
 uri="/dir/index.html",
qop=auth,
nc=00000001,
cnonce="0a4f113b",
response="6629fae49393a05397450978507c4ef1",
opaque="5ccc069c403ebaf9f0171e9517f40e41"
 Content-length: 12345
Content-type: text/html

HTTP Body
TRANSPORT_PROTOCOL=OXTA
 TRANSPORT_PROTOCOL_VERSION=1.0
REQUEST_TYPE=SEND
MESSAGE_ID=A1234567890ZZ0987654321
MESSAGE_TYPE=XML
MESSAGE_STANDARD=OAG
 TRANSACTION_TYPE=PO
TRANSACTION_SUBTYPE=PROCESS
 DOCUMENT_NUMBER=12345
 PARTYID=9999
PARTY_SITE_ID=8888
 PROTOCOL_TYPE=HTTPS-OXTA
 PROTOCOL_ADDRESS=HTTPS://www.me.com/servlets/oracle.ecx.oxta. transportAgentServer
USERNAME=myusername
PASSWORD=myloginpassword
 ATTRIBUTE1=
 ATTRIBUTE2=
 ATTRIBUTE3=
 ATTRIBUTE4=
 ATTRIBUTE5=
 PAYLOAD=<xml   ... ...> 


ORACLE TRANSPORT AGENT RESPONSE MESSAGE
=======================================
The OTA server uses standard HTTP response codes to determine if the HTTP post (at the HTTP protocol level) was successful. If successful, OTA will examine the HTTP header to determine if the OTA message was successfully delivered. If the HTTP response does not equal 200, OTA assumes the post failed and will requeue the message for retry (until maximum retry has been reached).

The following is an example of an HTTP response:

HTTP Header
 HTTP/1.1 200 OK
 Server: Apache/1.2.0
 Date: Fri, 15 Jun 2002 16:20:46 GMT
 Content-Length: 567
 STATUS_CODE: 1000
 STATUS_DESCRIPTION: OK
 MESSAGE_RECEIPT_ID: A9876543210987654321
 Content-type: text/html

HTTP Body
 <HTML><BODY> <TABLE>   <TR><TD>Status Code</TD><TD>1000</TD></TR> <TR><TD>Status Description</TD><TD>Message Successfully received</TD></TR>  <TR><TD>Message Receipt ID</TD><TD>A9876543210987654321</TD> </TR> </TABLE> </BODY></HTML>

**/

class OTA {
	protected	$fp;
	protected	$xmlResponse;

	function __construct() {
		$this->fp = array('rawInput'=>null);
		$this->xmlResponse = array();
	}

	function getIncomingPostXml() {
		$this->fp['rawInput'] = fopen('php://input', 'r');
		$flagRecord = false;
		$xmlData = null;
		while(!feof($this->fp['rawInput'])) {
			$line = trim(fgets($this->fp['rawInput']));
			if (!$flagRecord && false!==strpos($line, 'PAYLOAD=')) {
				$flagRecord = true;
				$xmlData = preg_replace('/PAYLOAD\=/', '', $line, 1);
			} elseif ($flagRecord) {
				$xmlData.= $line;
			}
		}
		fclose($this->fp['rawInput']);
		return $xmlData;
	}

	function addXmlResponseMsg($type, $msg) {
		$this->xmlResponse[$type][] = $msg;
	}

	function getXmlResponse() {
		$response = null;
		foreach($this->xmlResponse as $type=>$msg) {
			$response.= '<'.$type.'>'.implode(',', $msg).'</'.$type.'>';
		}
		return '<answer>'.$response.'</answer>';
	}

	function getOTAStatusCode($statusCode) {
		$this->statusCodes = array(
			1000=>array('code'=>'OK', 'msg'=>'Request handled successfully.'),
			2000=>array('code'=>'ECX_OXTA_DB_UNAVAIL', 'msg'=>'Database unavailable. Cannot get connection to the database.'),
			2001=>array('code'=>'ECX_OXTA_SERVER_ERR', 'msg'=>'Unexpected server-side error. Client should retry.'),
			3000=>array('code'=>'ECX_OXTA_BAD_REQ', 'msg'=>'Missing TRANSPORT_PROTOCOL, TRANSPORT_PROTOCOL_VERSION, or MESSAGE_TYPE.'),
			3001=>array('code'=>'ECX_OXTA_UNKNOWN_REQ', 'msg'=>'Invalid value for MESSAGE_TYPE.'),
			3002=>array('code'=>'ECX_OXTA_AUTH_MISSVAL', 'msg'=>'Incomplete credentials. Username or Password not available for request.'),
			3003=>array('code'=>'ECX_OXTA_AUTH_FAILURE', 'msg'=>'Authentication failure. Invalid user password.'),
			3004=>array('code'=>'ECX_OXTA_PROTCL_NOTSUPP', 'msg'=>'Oracle Transport Agent protocol version not supported by server.'),
			3005=>array('code'=>'ECX_OXTA_PAYLOAD_NULL', 'msg'=>'Message has NULL payload.'),
			3100=>array('code'=>'ECX_OXTA_SEND_MISSVAL', 'msg'=>'Required parameters for the SEND post are missing.'),
			3101=>array('code'=>'ECX_OXTA_LEN_MISS', 'msg'=>'Request header content length attribute not set.'),
			3102=>array('code'=>'ECX_OXTA_LEN_TOOLARGE', 'msg'=>'Size of content larger than specified in request header content length attribute.'),
			3200=>array('code'=>'ECX_OXTA_AUTH2_MISSVAL', 'msg'=>'Parameter missing for AUTH2 request. Verify that the party_site_id and the transaction_type are valid.'),
			3201=>array('code'=>'ECX_OXTA_AUTH2_FAILURE	AUTH2', 'msg'=>'request failed.'),
			3300=>array('code'=>'ECX_OXTA_EME_MISSVAL', 'msg'=>'E-mail address or payload was not passed in the EME request.'),
			3301=>array('code'=>'ECX_OXTA_EME_INVALID_EMAIL', 'msg'=>'Incorrect e-mail address format for EME request.'),
			3302=>array('code'=>'ECX_OXTA_EME_SMTP_NOTSET', 'msg'=>'Mail server not set up in config.'),
			4000=>array('code'=>'ECX_OXTA_UNKNOWN_PROTCL', 'msg'=>'Invalid protocol type for SEND request.'),
			4001=>array('code'=>'ECX_OXTA_TIMEOUT', 'msg'=>'Time out for this transport request reached.'),
			4002=>array('code'=>'ECX_OXTA_CLIENT_ERR', 'msg'=>'Unexpected client side exception.'),
			4003=>array('code'=>'ECX_OXTA_MAX_ATTEMPTS', 'msg'=>'Exceeded the max number of attempts for transport.'),
			4100=>array('code'=>'ECX_OXTA_SMTP_NOTSET', 'msg'=>'Mail server not set up in config.'),
			4101=>array('code'=>'ECX_OXTA_INVALID_EMAIL', 'msg'=>'Incorrect e-mail address format.'),
			4102=>array('code'=>'ECX_OXTA_MAIL_ERR', 'msg'=>'Error when trying to send mail.'),
			4103=>array('code'=>'ECX_OXTA_MAILJAR_NOT_EXISTS', 'msg'=>'SMTP not enabled. Ensure that mail.jar is present in your Java classpath.'),
			4104=>array('code'=>'ECX_OXTA_ACTJAR_NOT_EXISTS', 'msg'=>'SMTP not enabled. Ensure that activation.jar is present in your Java classpath.'),
			4200=>array('code'=>'ECX_OXTA_INVALID_URL', 'msg'=>'Incorrect URL format.'),
			4201=>array('code'=>'ECX_OXTA_PROXY_FAILURE', 'msg'=>'Cannot open connection to proxy server.'),
			4202=>array('code'=>'ECX_OXTA_CONNECT_FAILURE', 'msg'=>'Cannot connect to host:port.'),
			4203=>array('code'=>'ECX_OXTA_UNKNOWN_RES', 'msg'=>'Response from the server not in a format understood by the client.'),
			4300=>array('code'=>'ECX_OXTA_INVALID_CACERT', 'msg'=>'Failed to open the certificate file/failed to read certificate from the file.'),
			4301=>array('code'=>'ECX_OXTA_SSLHANDSHAKE_FAILURE', 'msg'=>'Failed to perform handshake when getting SSL connection.'),
			4302=>array('code'=>'ECX_OXTA_SSLVERICHAIN_FAILURE', 'msg'=>'Error when verifying chain certificate.'),
			5000=>array('code'=>'ECX_OXTA_PROTOCOL_MISS', 'msg'=>'Protocol value missing from inbound message.'),
			5001=>array('code'=>'ECX_OXTA_USERNAME_MISS', 'msg'=>'Username value missing from the inbound message.'),
			5002=>array('code'=>'ECX_OXTA_PASSWORD_MISS', 'msg'=>'Password value missing from the inbound message.')
		);

	}
}
?>