<?php

/* 
 * This is an override and fix for a new CF7 class that is preventing error overrides
 * 
 * We need to watch that updates to CF7 don't require updates here
 */

class SynWPCF7_Validation {
    public $invalid_fields = array();
    
    public function cast($parent) {
        $this->invalid_fields = $parent->get_invalid_fields();
        
        foreach($this->invalid_fields as $key=>$values) {
            if(strpos($key, '::SYN') !== FALSE) {
                $new_key = str_replace('::SYN', '', $key);
                $this->invalid_fields[$new_key] = $values;
            }
        }
    }
    
    public function get_invalid_fields() {
        return $this->invalid_fields;
    }
    
    public function is_valid( $name = null ) {
		if ( ! empty( $name ) ) {
			return ! isset( $this->invalid_fields[$name] );
		} else {
			return empty( $this->invalid_fields );
		}
	}
}

