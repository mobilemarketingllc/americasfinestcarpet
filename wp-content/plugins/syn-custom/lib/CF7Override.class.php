<?php
/**
 * Synapse Marketing Solutions Plugin For Customized Builds
 *
 * @license		Synapse Marketing Solutions
 * @copyright	Copyright (C) 2014 Synapse Marketing Solutions
 * @version		Release: Synapse Custom Build
 */

/**
 * Synapse Custom Library for WP Contact Form 7 Email Overrides
 *
 * @category	CF7
 * @package		WordPress CF7 Plugin
 * @author		Ryan Consylman
 * @todo		Clean this class up to meet {@link https://make.wordpress.org/core/handbook/coding-standards/php/ WordPress Coding Standards} and document methods properly - Joseph L.
 */
class CF7Override  {
    private $options = array();
    private $email_fields = array(
        'cc_email', 'cc_addresses', 'override_email', 'override_addresses'
    );
    
    function __construct() {
        foreach($this->email_fields as $efield) {
            $this->options[$efield] = get_option('syn_'.$efield);
        }
        
        # Run after plugins are loaded and active.
		add_action('plugins_loaded', array(&$this, 'init'));
    }
    
    function init() {
        add_action('wpcf7_before_send_mail', array(&$this, 'before_send_mail'));
        
        // Only apply this filter on the front end (it will break back end)
        if(!is_admin()) {
            add_filter('wpcf7_contact_form_properties', array(&$this, 'form_override_from_file'), 10, 2);
        }
    }
    
    function before_send_mail($contact_form) {
        if($this->options['override_email'] == 'yes') {
            $this->override_email($contact_form);
        }
        else if($this->options['cc_email'] == 'yes') {
            $this->cc_email($contact_form);
        }
        else {
            return;
        }
    }
    
    /*
     * Completely override outgoing mail recipients
     */
    function override_email($contact_form) {
        $current_properties = $contact_form->get_properties();
        $new_properties = $current_properties;

        $new_properties['mail']['recipient'] = $this->options['override_addresses'];

        $mail2 = $current_properties['mail_2'];
        if($mail2['active']) {
            $new_properties['mail_2']['recipient'] = $this->options['override_addresses'];
        }
        $contact_form->set_properties($new_properties);
    }
    
    /*
     * BCC people on all emails
     */
    function cc_email($contact_form) {
        $current_properties = $contact_form->get_properties();
        $new_properties = $current_properties;
        
        $header = $current_properties['mail']['additional_headers'];
        $new_header = $header." 
                            Bcc: ".$this->options['cc_addresses'];
        $new_properties['mail']['additional_headers'] = $new_header;
        
        $mail2 = $current_properties['mail_2'];
        if($mail2['active']) {
            $header = $mail2['additional_headers'];
            $new_header = $header." 
                                Bcc: ".$this->options['cc_addresses'];
            $new_properties['mail_2']['additional_headers'] = $new_header;
        }
        $contact_form->set_properties($new_properties);
    }
    
    function form_override_from_file($properties, $contact_form) {
        $form = $properties['form'];
    
        if(strrpos($form, '[from_file', -strlen($form)) !== FALSE) {
            $tag_parts = explode('"', $form);
            $file_path = $tag_parts[1];

            if(!file_exists($file_path)) {
                $properties['form'] = 'That file could not be found.';
                return $properties;
            }
            $properties['form'] = file_get_contents($file_path);
        }

        $mail1 = $properties['mail'];
        if( isset($mail1['body']) && strrpos($mail1['body'], '[from_file', -strlen($mail1['body'])) !== FALSE) {
            $tag_parts = explode('"', $mail1['body']);
            $file_path = $tag_parts[1];

            if(!file_exists($file_path)) {
                $properties['form'] = 'The file for the first email template could not be found.';
                return $properties;
            }
            $properties['mail']['body'] = file_get_contents($file_path);
        }

        $mail2 = $properties['mail_2'];
        if( isset($mail2['body']) && strrpos($mail2['body'], '[from_file', -strlen($mail2['body'])) !== FALSE) {
            $tag_parts = explode('"', $mail2['body']);
            $file_path = $tag_parts[1];

            if(!file_exists($file_path)) {
                $properties['form'] = 'The file for the second email template could not be found.';
                return $properties;
            }
            $properties['mail_2']['body'] = file_get_contents($file_path);
        }

        return $properties;
    }
}
