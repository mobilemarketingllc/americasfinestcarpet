<?php

/* 
 * A collection of utility functions
 */

class SynUtility {
    
    function __construct() {
        
    }
    
    function formatBytes($size, $precision = 2) {
        $base = log($size) / log(1024);
        $suffixes = array('B', 'KB', 'MB', 'GB', 'TB');   

        return round(pow(1024, $base - floor($base)), $precision).' '.$suffixes[floor($base)];
    }

    function formatMoney($num) {
        return '$' . number_format($num, 2);
    }

    function formatPercent($num){
        $num = $num * 100;
        return number_format($num, 2)."%";
    }

    function formatTime($time, $meridian) {
        $t = new DateTime($time);
        if($meridian){
            return $t->format('h:ia');
        }
        else {
            return $t->format('h:i');
        }
    }
    
    // String functions
    function str_contains($haystack, $needle) {
        if(!strpos($haystack, $needle)) {
            return false;
        }
        return true;
    }

    function startsWith($haystack, $needle) {
        return $needle === "" || strpos($haystack, $needle) === 0;
    }

    function endsWith($haystack, $needle) {
        return $needle === "" || substr($haystack, -strlen($needle)) === $needle;
    }

}

