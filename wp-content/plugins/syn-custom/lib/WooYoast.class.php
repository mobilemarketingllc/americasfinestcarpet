<?php

/* 
 * A class to deal with various changes required to
 * sites with both Woocommerce and Yoast
 */

class WooYoast {
    
    public function __construct() {
        // Check if required plugins are active
        if($this->required_plugins_active()) {
            add_filter('wpseo_title', array(&$this, 'seo_title_fixes'));
            add_filter('wpseo_metadesc', array(&$this,'seo_metadesc_fixes'));
        }
    }
    
    # Check if Woocommerce and Yoast are active.
	function required_plugins_active() {
		$required_plugins = array(
            'woocommerce/woocommerce.php',
            'wordpress-seo/wp-seo.php'
        );
        
		$plugin_on = true;
        
        foreach($required_plugins as $rp) {
            if (!is_plugin_active($rp)) {
                $plugin_on = false;
            }
        }
		return $plugin_on;
	}
    
    function seo_title_fixes($title) {
        if(is_archive()) {
            $product_base = get_option('woocommerce_permalinks');
            $clean_uri = str_replace('/', '', $_SERVER['REQUEST_URI']);

            if($clean_uri == $product_base['category_base']) {
                $title_parts = explode('|', $title);
                $seo_title = get_post_meta(get_option('woocommerce_shop_page_id'), '_yoast_wpseo_title', true);
                $title_parts[0] = $seo_title.' ';
                $title = implode('|', $title_parts);
            }
        }
        
        return $title;
    }
    
    function seo_metadesc_fixes($metadesc) {
        if(is_archive()) {
            $product_base = get_option('woocommerce_permalinks');
            $clean_uri = str_replace('/', '', $_SERVER['REQUEST_URI']);

            if($clean_uri == $product_base['category_base']) {
                $metadesc = get_post_meta(get_option('woocommerce_shop_page_id'), '_yoast_wpseo_metadesc', true);
            }
        }

        return $metadesc;
    }
}
