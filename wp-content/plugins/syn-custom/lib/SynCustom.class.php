<?php
/**
 * Synapse Custom Plugin Class
 *
 * @category   WP Plugin
 * @package    Synapse Custom
 * @license    Synapse Marketing Solutions
 * @author     Andy Koo <akoo@synapseresults.com>
 * @copyright  Copyright (C) 2014 Synapse Marketing Solutions
 * @version    Release: 1.0
**/

class SynCustom {
	protected static $SC = null;
	protected $db_version = 1.000;
	protected $wpdb;
	protected $blog_id;
	protected $dbtable;
	protected $SCA;
	protected $url_path;


	function __construct() {
		global $wpdb;										// WP Database Object
		$this->wpdb = $wpdb;								// Store WP DB in our object.
		$this->blog_id = get_current_blog_id();				// WP Current Blog ID
		$this->dbtable = $wpdb->base_prefix.'syncustom';	// Get table name.

		$this->url_path['css'] = plugin_dir_url(constant('PATH_CSS').'/styles.css');
		$this->url_path['js'] = plugin_dir_url(constant('PATH_JS').'/scripts.js');
		$this->url_path['img'] = plugin_dir_url(constant('PATH_IMG').'/a.png');

		# Add SynCustom to execute during parse request tier.
		// add_action('parse_request', array('SynCustom', 'process_form'));

		# Add action for Ajax calls for public website visitors.
		add_action('wp_ajax_nopriv_synCustomFBAjax', array(&$this, 'synCustomFBAjax'));

		# Run after plugins are loaded and active.
		add_action('plugins_loaded', array(&$this, 'postPluginsLoader'));
        
        # Enqueue any necessary files
        add_action( 'wp_enqueue_scripts', array(&$this, 'synCustomEnqueue'));
        
        # Custom Replacements for CF7
        add_filter('wpcf7_mail_components', array(&$this, 'wpcf7_mail_components_add_domain'));

		// WP Admin Only
		if (is_admin()) {
			require_once constant('PATH_LIB').'/SynCustomAdmin.class.php';

			$this->SCA = new SynCustomAdmin();
			// $this->SCA->download_excel();

			# Add WP Admin Menu
			if (is_multisite()) {
				// Add to Network Admin Navigation
				$admin_menu = 'network_admin_menu';
				// $admin_notices = 'network_admin_notices';
				// $plugin_action_links = 'network_admin_plugin_action_links_syn-custom/syn-custom.php';
				add_action($admin_menu, array(&$this->SCA, 'admin_menu'));
			}
			# Add to Site Admin Navigation
			$admin_menu = 'admin_menu';
			// $admin_notices = 'admin_notices';
			// $plugin_action_links = 'plugin_action_links_syn-custom/syn-custom.php';
			add_action($admin_menu, array(&$this->SCA, 'admin_menu'));

			# Register activation hooks.
			register_activation_hook(constant('PATH_BASE').'/syn-custom.php', array(&$this->SCA, 'install'));

			# Register deactivation hooks.
			// register_deactivation_hook(constant('PATH_BASE').'/syn-custom.php', array(&$this->SCA, 'deactivate'));

		}
	}


	# Prevent users to clone the instance
	function __clone() {
		trigger_error('Clone is not allowed.', E_USER_ERROR);
	}


	# Instantiate class
	static function init() {
		if (is_null(self::$SC)) {
			self::$SC = new self();
		}
		return self::$SC;
	}


	# Facebook Ajax
	function synCustomFBAjax() {
		$FBC = new FacebookCustom();
		return $FBC->getPosts();
	}


	# Post Plugin Loader
	#	Enter code here to process after all plugins have loaded.
	function postPluginsLoader() {
		# Secure WP Info
		remove_action('wp_head', 'rsd_link'); // Removes the Really Simple Discovery link
		remove_action('wp_head', 'wlwmanifest_link'); // Removes the Windows Live Writer link
		remove_action('wp_head', 'wp_generator'); // Removes the WordPress version
		remove_action('wp_head', 'index_rel_link'); // Removes the index page link
		remove_action('wp_head', 'start_post_rel_link', 10, 0); // Removes the random post link
		remove_action('wp_head', 'parent_post_rel_link', 10, 0); // Removes the parent post link
		remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // Removes the next and previous post links

		# Fix SEO Issues
		remove_action('wp_head', 'feed_links', 2); // Removes the RSS feeds remember to add post feed maunally (if required) to header.php
		remove_action('wp_head', 'feed_links_extra', 3); // Removes all other RSS links
	}

    function synCustomEnqueue() {
        $includes_url = plugins_url().'/syn-custom/js/';
    
        wp_enqueue_script( 'jquery-maskedinput-js', $includes_url.'jquery.maskedinput.js', array('jquery'), '1.3.1', true);
        wp_enqueue_script( 'syn-form-validation-js', $includes_url.'form_validation.js', array('jquery', 'jquery-maskedinput-js'), '1.0', true);
        
    }
    
    /*
     * Components comes in as an array of the email parts:
     * 'subject', 'sender', 'body', 'recipient', 'additional_headers', 'attachments'
     */
    function wpcf7_mail_components_add_domain($components) {
        $replacements = array(
            '[domain]'    => $_SERVER['SERVER_NAME'],
            '[year]'	=> date('Y')
        );

        $components['body'] = str_replace(array_keys($replacements), array_values($replacements), $components['body']);
        return $components;
    }
}
