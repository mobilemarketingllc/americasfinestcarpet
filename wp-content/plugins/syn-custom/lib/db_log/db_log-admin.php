<?php

add_action('admin_menu', 'set_db_log_admin_menu');

function set_db_log_admin_menu(){

    add_submenu_page(
        'synapse-custom', 
        'Database Log Table',
        'DB Log',
        'activate_plugins',
        'synapse-custom/db_log',
        'pageLogTable'
    );
}

function pageLogTable() {
    page_db_log_header();
    include dirname(__FILE__).'/db_log-admin-page.php';
    page_db_log_footer();
}

function page_db_log_header() {
    ?>
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css" />

    <script src="//cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>

    <div class="wrap">
    <?php
}

function page_db_log_footer() {
    echo '</div>';
}

function get_all_db_logs() {
    global $wpdb;
    
    $sql = "SELECT * FROM ".$wpdb->base_prefix."posts
            WHERE post_type='dblog'
            ORDER BY post_date DESC";
    
    return $wpdb->get_results($sql, ARRAY_A);
}