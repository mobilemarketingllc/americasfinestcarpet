<?php

?>
<style>
    #db-log-table td, #db-log-table th {
        border-right: 1px solid #ccc;
        border-top: 1px solid #ccc;
    }
    
    #db-log-table td.td-id {
        width: 40px;
        text-align: center;
    }
    
    #db-log-table td.td-time {
        width: 150px;
        text-align: center;
    }
    
    .action-bar {
        margin: 10px 0;
    }
    
    .action-bar button, .action-bar form {
        float: left;
    }
    
    .action-bar button {
        margin: 5px 10px !important;
    }
</style>

<div class="action-bar">
    <button type="button" class="button-primary" onclick="location.reload();">Refresh Log</button>
    <form name="clear-log" id="clear-log" method="post" action="">
        <button class="button">Clear Log</button>
    </form>
    <div class="clear"></div>
</div>

<table id="db-log-table">
    <thead>
        <th class="td-id">Log ID</th>
        <th>Title</th>
        <th class="td-time">Time</th>
        <th>Message</th>
    </thead>
    <tbody>
        <?php
        $logs = get_all_db_logs();
        foreach($logs as $log) { ?>
        <tr>
            <td class="td-id"><?=$log['ID']?></td>
            <td><?=$log['post_name']?></td>
            <td class="td-time"><?=$log['post_date']?></td>
            <td><?=$log['post_content']?></td>
        </tr>
        <?php } ?>
    </tbody>
</table>

<script type="text/javascript">
    if(typeof($) == 'undefined') {
        var $ = jQuery;
    }
    
    $(function(){
        $('#db-log-table').dataTable({
            "order": [[2, "desc"]]
        });
        
        $('#clear-log').submit(function(){
            clear_db_log();
            return false;
        });
    });
    
    function clear_db_log() {
        var r = confirm("Are you sure you want to completely clear the log?");
        if(r) {
            jQuery.ajax({
                type : "post",
                dataType : "json",
                url : '/wp-admin/admin-ajax.php',
                data : {
                    action: "clear_db_log"
                },
                success: function(data) {
                   if(data === "success") {
                       location.reload();
                   }
                   else {
                       alert(data);
                   }
               }
           });
        }
    }
</script>