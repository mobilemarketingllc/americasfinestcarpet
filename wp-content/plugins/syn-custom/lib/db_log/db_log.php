<?php

/*
Plugin Name: Database Logging
Plugin URI: 
Description: Allows logging of custom errors into the database
Author: Synapse Marketing Solutions
Version: 1.0
*/

/*
 * Register the post type
 */
function db_log_custom_post_type() {
    $args = array(
        'public'    => true,
        'label'     => 'DB Log',
        'show_in_menu'  => false
    );
    register_post_type('dblog', $args);
}
add_action('init', 'db_log_custom_post_type');

/*
 * Core functions
 */
function db_log($message, $title = NULL) {
    $db_post = array(
        'post_content'      => '<pre>'.$message.'</pre>',
        'post_name'         => $title,
        'post_status'       => 'private',
        'post_type'         => 'dblog',
        'post_date'         => date('Y-m-d H:i:s')
    );
    
    wp_insert_post($db_post);
}

function db_log_r($message, $title = NULL) {
    db_log(print_r($message, true), $title);
}

if(is_admin()){
    require_once('db_log-admin.php');
    require_once('db_log-ajax.php');
}