<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

add_action("wp_ajax_clear_db_log", "clear_db_log");

function clear_db_log() {
    global $wpdb;
    
    $sql = "DELETE FROM ".$wpdb->base_prefix."posts WHERE post_type='dblog'";
    $wpdb->query($sql);
    
    echo json_encode('success');
    die();
}