<?php
/**
 * Synapse Custom Admin Plugin Class
 *
 * @category   WP Plugin
 * @package    Synapse Custom
 * @license    Synapse Marketing Solutions
 * @author     Andy Koo <akoo@synapseresults.com>
 * @copyright  Copyright (C) 2014 Synapse Marketing Solutions
 * @version    Release: 1.1
**/

class SynCustomAdmin extends SynCustom {
	/**
	 * The WP privilege level required to use the admin interface
	 * @var string
	 */
	protected $capability_required;

	/**
	 * Name of the page holding the options
	 * @var string
	 */
	protected $page_options;

	/**
	 * Title for the plugin's settings page
	 * @var string
	 */
	protected $text_settings;

	protected $admin_slug	= 'synapse-custom';
	protected $page_admin	= 'Syn-Custom';
	protected $name			= 'Synapse Marketing Solutions';

	public function __construct() {
		// Translation already in WP combined with plugin's name.
		$this->text_settings = $this->page_admin . ' ' . __( 'Settings' );

		if (is_multisite()) {
			$this->capability_required = 'manage_network_options';
			$this->page_options = 'settings.php';
		} else {
			$this->capability_required = 'manage_options';
			$this->page_options = 'options-general.php';
		}

		# Register actions for CF7 page.
		add_action( 'wp_ajax_syncf7', array( &$this, 'syncf7_ajax_callback' ) );
		add_action( 'admin_enqueue_scripts', array( &$this, 'enqueue_scripts' ) );
	}


	/**
	 * Declares a menu item and callback for this plugin's settings page
	 *
	 * NOTE: This method is automatically called by WordPress when
	 * any admin page is rendered
	 */
	public function admin_menu() {
		add_menu_page(
			'Synapse Custom',
			'Synapse',
			$this->capability_required,
			$this->admin_slug,
			array(&$this, 'pageWelcome'),
			plugin_dir_url(constant('PATH_IMG').'/synapse-symbol.png').'synapse-symbol.png'
		);
		add_submenu_page(
			$this->admin_slug,
			'Synapse Welcome',
			'Welcome',
			$this->capability_required,
			$this->admin_slug,
			array(&$this, 'pageWelcome')
		);
		add_submenu_page(
			$this->admin_slug,
			'Contact Form 7 - Custom Validation',
			'CF7 Validation',
			$this->capability_required,
			$this->admin_slug.'/cf7-validation',
			array(&$this, 'pageCF7Validation')
		);
        add_submenu_page(
			$this->admin_slug,
			'Contact Form 7 - Email Overrides',
			'CF7 Emails',
			$this->capability_required,
			$this->admin_slug.'/cf7-overrides',
			array(&$this, 'pageCF7Overrides')
		);
	}

	# WordPress doesn't show the successs/error messages on
	# the Network Admin screen, at least in version 3.3.1,
	# so force it to happen for now.
	public function multisitePageErrorFix() {
		if (is_multisite()) {
			include_once ABSPATH . 'wp-admin/options-head.php';
		}
	}

	public function pageHeader() {
        ?>
		<div class="syn wrap">
		<div class="logo">
            <a href="http://www.synapseresults.com" title="Synapse Marketing Solutions" target="_blank">
                <h1 class="def-logo">
                    <img src="<?php echo self::$SC->url_path['img']; ?>synapse-logo.png">
                    <span><strong>syn</strong>-apse [<strong>sin</strong>-naps]</span>
                </h1>
            </a>
        </div>
        <?php
	}

	public function pageFooter() {
		echo '</div>';
	}

	public function pageGen($file) {
		$this->multisitePageErrorFix();
		$this->pageHeader();
		include $file;
		$this->pageFooter();
	}

	# The callback for rendering the page.
	public function pageWelcome() {
		$this->pageGen(constant('PATH_BASE').'/welcome.page.php');
	}

	# The callback for rendering the page.
	public function pageCF7Validation() {
		$this->pageGen(constant('PATH_BASE').'/cf7validation.page.php');
	}
    public function pageCF7Overrides() {
		$this->pageGen(constant('PATH_BASE').'/cf7overrides.page.php');
	}

	# CF7 Ajax callback.
	public function syncf7_ajax_callback() {
		if (isset($_POST['synaction'])) {
			$po = array();

			# CF7 get validation rule.
			if ('syncf7vget'==$_POST['synaction']) {
				$CF7V = new CF7Validation();
				$data = $CF7V->getRuleById($_POST['id']);
				if (!$data) {
					$po['error'] = $CF7V->error;
				} else {
					$po = $data;
				}
			}

			# CF7 delete validation rule.
			if ('syncf7vdel'==$_POST['synaction']) {
				$CF7V = new CF7Validation();
				if (!$CF7V->delRule($_POST['id'])) {
					$po['error'] = $CF7V->error;
				} else {
					$po['success'] = 'Rule Deleted Successfully';
				}
			}

			# CF7 add or update validation rule.
			if ('syncf7vadd'==$_POST['synaction']) {
				$_POST = array_map('stripslashes', $_POST);
				$CF7V = new CF7Validation();
				$data = array(
					'id'			=> $_POST['id'],
					'blog_id'		=> $_POST['blog_id'],
					'field_type'	=> $_POST['field_type'],
					'field_name'	=> $_POST['field_name'],
					'func'			=> $_POST['func'],
					'regex'			=> $_POST['regex'],
					'msg_err'		=> $_POST['msg_err'],
					'rank'			=> $_POST['rank']
				);
				if (!$CF7V->modRule($data)) {
					$po['error'] = $CF7V->error;
				} else {
					$po['success'] = 'Rule Processed Successfully';
				}
			}

			die(json_encode($po));
		}

		die;
	}


	# The callback for rendering the page.
	public function pageUserApproval() {
		$this->pageGen(constant('PATH_BASE').'/user-approval.page.php');
	}

	public function get_form_ids(&$error) {
		$sql = "select `form_id` from `".self::$SC->dbtable."` group by `form_id` order by `form_id`";
		$bind = array();
		$wpdb_r = self::$SC->wpdb->get_results(self::$SC->wpdb->prepare($sql, $bind), ARRAY_A);
		if (false===$wpdb_r) {
			$error = self::$SC->wpdb->last_error;
			return false;
		}
		return $wpdb_r;
	}

	public function get_form_fields_by_form_id($form_id, &$error) {
		$sql = "select distinct `field_name` from `".self::$SC->dbtable."` where `form_id` = %s order by `id`";
		$bind = array($form_id);
		$wpdb_r = self::$SC->wpdb->get_results(self::$SC->wpdb->prepare($sql, $bind), ARRAY_A);
		if (false===$wpdb_r) {
			$error = self::$SC->wpdb->last_error;
			return false;
		}
		return $wpdb_r;
	}

	public function get_form_records_by_form_id($form_id, &$error) {
		$sql = "select x.* from (select `request_id`, `creation_date`, `change_date`, `field_name`, `field_value` field_value from `".self::$SC->dbtable."` where `form_id` = %s order by `request_id`, `id`) x order by x.`creation_date` desc, x.`change_date` desc";
		$bind = array($form_id);
		$wpdb_r = self::$SC->wpdb->get_results(self::$SC->wpdb->prepare($sql, $bind), ARRAY_A);
		if (false===$wpdb_r) {
			$error = self::$SC->wpdb->last_error;
			return false;
		}
		return $wpdb_r;
	}

	public function get_form_data_by_form_id($form_id, &$db_field_names, &$db_records, &$errors) {
		if (!$db_field_names = $this->get_form_fields_by_form_id($form_id, $error)) {
			$errors['main'] = $error;
			return false;
		} elseif (!$db_r = $this->get_form_records_by_form_id($form_id, $error)) {
			$errors['main'] = $error;
			return false;
		}

		array_unshift($db_field_names, array('field_name'=>'request_id'));
		$db_field_names[] = array('field_name'=>'creation_date');
		$db_field_names[] = array('field_name'=>'change_date');

		$db_records = array();
		foreach($db_r as $i=>$d) {
			if (!isset($db_records[$d['request_id']])) {
				$db_records[$d['request_id']] = array(
					'request_id'	=> $d['request_id'],
					'creation_date'	=> $d['creation_date'],
					'change_date'	=> $d['change_date']
				);
			}
			$db_records[$d['request_id']][$d['field_name']] = $d['field_value'];
		}

		return true;
	}

	// function download_excel($db_field_names, $db_records) {
	public function download_excel() {
		if (!isset($_GET['page'], $_GET['fid'], $_GET['dl']) || $this->page_admin!=$_GET['page'] || !$this->check_form_id($_GET['fid'])) {
			return false;
		}

		// Get data for form id.
		$db_field_names	= array();
		$db_records		= array();
		$this->get_form_data_by_form_id($_GET['fid'], $db_field_names, $db_records, $errors);


		/** Include PHPExcel */
		require_once constant('PATH_LIB').'/PHPExcel/Classes/PHPExcel.php';


		// PDF Rendering Settings
		if ('pdf'==$_GET['dl']) {
			//	Change these values to select the Rendering library that you wish to use
			//		and its directory location on your server
			$rendererName = PHPExcel_Settings::PDF_RENDERER_TCPDF;
			// $rendererName = PHPExcel_Settings::PDF_RENDERER_MPDF;
			//$rendererName = PHPExcel_Settings::PDF_RENDERER_DOMPDF;
			$rendererLibrary = 'tcpdf';
			// $rendererLibrary = 'mPDF5.4';
			//$rendererLibrary = 'domPDF0.6.0beta3';
			$rendererLibraryPath = dirname(__FILE__).'/libs/' . $rendererLibrary;
		}


		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();

		// Set document properties
		$objPHPExcel->getProperties()
			->setCreator("Synapse Marketing Solutions")
			->setLastModifiedBy("Synapse Marketing Solutions")
			->setTitle("Office 2007 XLSX Test Document")
			->setSubject("Office 2007 XLSX Test Document")
			->setDescription("Shadowlight Group Excel Spreadsheet for Office 2007 XLSX.")
			->setKeywords("office 2007 openxml php")
			->setCategory("Shadowlight Group Excel Spreadsheet");

		// Add header row
		$col = 'A';
		$row = 1;
		foreach($db_field_names as $i=>$d) {
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($col.$row, ucwords(str_replace(array('_', '-'), ' ', $d['field_name'])));

			$objPHPExcel->getActiveSheet()->getStyle($col.$row)->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_WHITE);
			$styleArray = array(
				'font' => array(
					'bold' => true
				),
				'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
				),
				'borders' => array(
					'top' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN
					),
					'bottom' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN
					),
					'left' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN
					),
					'right' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN
					)
				),
				'fill' => array(
					'type' => PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
					'rotation' => 90,
					'startcolor' => array(
						'argb' => '00AAFF',
					),
					'endcolor' => array(
						'argb' => '0066AA',
					)
				)
			);
			$objPHPExcel->getActiveSheet()->getStyle($col++.$row)->applyFromArray($styleArray);

			// $objPHPExcel->getActiveSheet()->getStyle($col.$row)->getFill()
			// ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
			// ->getStartColor()->setARGB('00AAFF');

			// $objPHPExcel->getActiveSheet()->getStyle($col.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			// $objPHPExcel->getActiveSheet()->getStyle($col.$row)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			// $objPHPExcel->getActiveSheet()->getStyle($col.$row)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			// $objPHPExcel->getActiveSheet()->getStyle($col.$row)->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			// $objPHPExcel->getActiveSheet()->getStyle($col++.$row)->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		}
		$row++;

		// Add data records
		foreach($db_records as $ri=>$rd) {
			$col = 'A';
			foreach($db_field_names as $fni=>$fnd) {
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue($col++.$row, $rd[$fnd['field_name']]);
			}
			$row++;
		}

		$col = 'A';
		foreach($db_field_names as $i=>$d) {
			$objPHPExcel->getActiveSheet()->getColumnDimension($col++)->setAutoSize(true);
		}

		// Rename worksheet
		$objPHPExcel->getActiveSheet()->setTitle($_GET['fid']);


		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);

		// Set page orientation.
		$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

		// Set page scaling: fit to width by infinite pages tall.
		$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
		$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToHeight(0);

		// Set page margins.
		$objPHPExcel->getActiveSheet()->getPageMargins()->setTop(0.5);
		$objPHPExcel->getActiveSheet()->getPageMargins()->setRight(0.5);
		$objPHPExcel->getActiveSheet()->getPageMargins()->setLeft(0.5);
		$objPHPExcel->getActiveSheet()->getPageMargins()->setBottom(0.5);


		if ('pdf'==$_GET['dl']) {
			// Render PDF
			if (!PHPExcel_Settings::setPdfRenderer(
					$rendererName,
					$rendererLibraryPath
				)) {
				exit(
					'NOTICE: Please set the $rendererName and $rendererLibraryPath values' .
					'<br />' .
					'at the top of this script as appropriate for your directory structure'
				);
			}


			// Redirect output to a client�s web browser (PDF)
			header('Content-Type: application/pdf');
			header('Content-Disposition: attachment;filename="form-'.$_GET['fid'].'.'.date('Ymd').'.pdf"');
			header('Cache-Control: max-age=0');

			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'PDF');
		} else {
			// Redirect output to a client�s web browser (Excel2007)
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment;filename="form-'.$_GET['fid'].'.'.date('Ymd').'.xlsx"');
			header('Cache-Control: max-age=0');

			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		}
		$objWriter->save('php://output');
		exit;
	}

	public function check_form_id($form_id) {
		$db_form_ids = $this->get_form_ids($error);

		$fid_pattern = null;
		foreach($db_form_ids as $fid) {
			$fid_pattern.= (!is_null($fid_pattern) ? '|' : null).$fid['form_id'];
		}

		if (!preg_match('/^('.$fid_pattern.'){1}$/', $form_id)) {
			return false;
		}
		return true;
	}

	public function install() {
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	}

	public function deactivate() {
	}

	/**
	 * Enqueue JavaScripts and Style Sheets needed for the CF7 Validation Admin page
	 *
	 * @param string $hook
	 * @author Joseph Leedy <jleedy@synapseresults.com>
	 */
	public function enqueue_scripts( $hook ) {
        if(!isset($_GET['page']) || strpos($_GET['page'], 'synapse-custom') === FALSE) {
            return;
        }
        
		if ( 'synapse_page_synapse-custom/cf7-validation' === $hook ) {
			wp_register_script(
                'cf7validation-page',
                plugins_url( 'js/syn-cf7validation.page.js', dirname( __FILE__ ) ),
                array(),
                false,
                true
            );
            wp_enqueue_script( 'cf7validation-page' );
		}
        
        wp_enqueue_script(
			'syn-custom',
			plugins_url( 'js/scripts.js', dirname( __FILE__ ) ),
			array('jquery'),
			false, true
		);
        wp_enqueue_style(
			'syn-custom',
			plugins_url( 'css/styles.css', dirname( __FILE__ ) ),
			array()
		);
        
        /*
         * VModal base, added 03-20-2015, Ryan C
         */
        wp_enqueue_script(
			'vmodal',
			plugins_url( 'js/vmodal.js', dirname( __FILE__ ) ),
			array('jquery'),
			false, true
		);
        wp_enqueue_style(
			'vmodal',
			plugins_url( 'css/vmodal.css', dirname( __FILE__ ) ),
			array()
		);
	}
}
