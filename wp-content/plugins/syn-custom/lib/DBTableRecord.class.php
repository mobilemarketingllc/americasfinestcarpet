<?php
/**
 * Implementation of Database Table Record
 *
 * @category   Database
 * @package    Synapse Database Framework
 * @license    Synapse Marketing Solutions
 * @version    @version@
 * @author     Andy Koo
 * @copyright  Copyright (C) 2014 Synapse Marketing Solutions
 * @version    Release: 1.0
**/

class DBTableRecord {
	protected	$table;
	protected	$tablePK;
	/**
	 * @var object $DB reference to database object. (Instance of WordPress DB Object)
	 * @access protected
	**/
	protected	$DB;
	protected	$data;
	protected	$dataOld;
	protected	$dataValidate;
	protected	$dataEditableFields;
	public		$error;
	public		$dataValidError;


	/**
	 * Create a new instance of this object.
	 *
	 * @param object $db object to access the underlying database.
	 * @param string $table is the table name.
	 * @return object instance of {@link DBTableRecord}
	**/
	function __construct($table=null) {
		global $wpdb;			// WP Database Object
		$this->DB = $wpdb;		// Store WP DB in our object.
		$this->data = array();
		$this->table = esc_sql($table);
		$this->genTableData();
		$this->clearError();
		$this->clear();
	}


	/**
	 *
	**/
	protected function getTableColumns() {
		$dbr = $this->DB->get_results("show columns from `".$this->table."`", ARRAY_A);
		if (!$dbr) {
			$this->error = $this->DB->last_error;
			return false;
		}
		return $dbr;
	}


	/**
	 * Generate Table Object
	**/
	protected function genTableData() {
		if (!$dbr = $this->getTableColumns()) {
			return false;
		}
		// $dbr = $this->DB->get_results("show columns from `".$this->table."`");
		// if (!$dbr) {
			// $this->error = $this->DB->last_error;
			// return false;
		// }
		foreach($dbr as $i=>$d) {
			if (isset($d['Key']) && 'PRI'==$d['Key']) {
				$this->tablePK[] = $d['Field'];
			}
			$d['Null'] = strtoupper($d['Null']);

			// Setup array.
			$this->data[$d['Field']] = null;

			// Setup regex validation.
			$regex = '.*';
			if (preg_match('/(?i)([a-zA-Z]+){0,1}(\(.+\)){0,1}(\s.*){0,1}/', $d['Type'], $typeData)) {
				if (isset($typeData[1])) {
					$typeData[1] = strtoupper($typeData[1]);

					// Setup integer regex.
					if (in_array($typeData[1], array('INTEGER', 'INT', 'SMALLINT', 'TINYINT', 'MEDIUMINT', 'BIGINT'))) {
						if (isset($typeData[3]) && 'unsigned'==strtolower(ltrim($typeData[3]))) {
							$regex = '[0-9]';
						} else {
							$regex = '(\-)?[0-9]';
						}

						if ('auto_increment'==$d['Extra']) {
							$regex.= '*';
						} elseif (isset($typeData[2])) {
							$typeData[2] = trim($typeData[2],'()');
							if (!empty($typeData[2])) {
								$min = 'NO'==$d['Null'] ? 1 : 0;
								$regex.= '{'.$min.','.$typeData[2].'}';
							} else {
								$regex.= '*';
							}
						} elseif ('NO'==$d['Null']) {
							$regex.= '+';
						} else {
							$regex.= '*';
						}
					} elseif (in_array($typeData[1], array('DECIMAL', 'NUMERIC', 'FLOAT', 'DOUBLE'))) {
						$regex = '((\-)?[0-9](\.[0-9]+)?)';
						if (isset($typeData[2])) {
							$typeData[2] = explode(',', trim($typeData[2],'()'));
							if (is_array($typeData[2]) && 2==count($typeData[2])) {
								$min = 'NO'==$d['Null'] ? 1 : 0;
								$regex = '(\-)?[0-9]{'.$min.','.$typeData[2][0].'}(\.[0-9]{'.$min.','.$typeData[2][1].'})?';
							} else {
								$regex.= '*';
							}
						} elseif ('NO'==$d['Null']) {
							$regex.= '+';
						} else {
							$regex.= '*';
						}
					} elseif ('ENUM'==$typeData[1]) {
						$regex = '.';
						if (isset($typeData[2]) && 2<strlen($typeData[2])) {
							$enum_data = str_getcsv($typeData[2], ',', "'");
							foreach($enum_data as $edi=>$edd) {
								// $enum_data[$edi] = preg_quote($edd);
								$enum_data[$edi] = str_replace(array('"', "'"), '', $edd);
							}
							$regex = '('.implode('|', $enum_data).')'.('NO'==$d['Null'] ? '+' : '*');
// error_log(__LINE__.':'.print_r($regex, true));
						} elseif ('NO'==$d['Null']) {
							$regex.= '+';
						} else {
							$regex.= '*';
						}
					} elseif (in_array($typeData[1], array('CHAR', 'VARCHAR'))) {
						$regex = '.';
						if (isset($typeData[2])) {
							$typeData[2] = trim($typeData[2],'()');
							if (!empty($typeData[2])) {
								$min = 'NO'==$d['Null'] ? 1 : 0;
								$regex.= '{'.$min.','.$typeData[2].'}';
							} else {
								$regex.= '*';
							}
						} elseif ('NO'==$d['Null']) {
							$regex.= '+';
						} else {
							$regex.= '*';
						}
					} elseif (in_array($typeData[1], array('TINYTEXT', 'TEXT', 'MEDIUMTEXT', 'LONGTEXT', 'TINYBLOB', 'BLOB', 'MEDIUMBLOB', 'LONGBLOB'))) {
						// Do not validate potential binary data.
						// $regex = '(.|[^a-zA-Z0-9])*';
						$regex = false;
					}
				}
			}
			if (false!==$regex) {
				$this->dataValidate[$d['Field']] = '/^'.$regex.'$/';
			}
		}
		$this->dataEditableFields = $this->dataOld = $this->data;
		return true;
	}


	function genSQLWhereID() {
		$sql_where = null;
		foreach($this->tablePK as $i=>$d) {
			$sql_where.= (!is_null($sql_where) ? ' and ' : null).'`'.$d."` = '".esc_sql($this->data[$d])."'";
		}
		return $sql_where;
	}


	function open() {
		$sql_where = $this->genSQLWhereID();
		$sql = "select * from `".$this->table."` where ".$sql_where." limit 1";
// error_log(__CLASS__.':'.__LINE__.': open: '.print_r($sql,true));
		$dbr = $this->DB->get_results($sql, ARRAY_A);
		if (false===$dbr) {
			$this->error = $this->DB->last_error;
			return false;
		}
		return isset($dbr[0]) ? $dbr[0] : $dbr;
	}


	/**
	 * Save database table record.
	 * You will need to populate ID's that are not set via auto increment.
	**/
	function save($forceInsert=false) {
		$insert	= true;
		$sql	= null;
		$sqli	= null;

		// Check primary key(s), if not forced to insert.
		if (!$forceInsert) {
// error_log(__CLASS__.':'.__LINE__.': tablePK: '.print_r($this->tablePK,true));
			foreach($this->tablePK as $i=>$d) {
				if (isset($this->data[$d]) && 0<strlen($this->data[$d])) {
					$insert = false;
					break;
				}
			}
		} else {
			// Clear old data to force new insert.
			$this->clearOld();
		}

		// Check if any data changed and
		// dynamically create SQL for insert or update.
        db_log_r($this->dataOld);
		foreach($this->dataEditableFields as $k=>$v) {
            if($k == 'func') {
                db_log($this->dataOld[$k].', '.$this->data[$k]);
            }
			if ($this->dataOld[$k] != $this->data[$k]) {
				$sqli.= (!is_null($sqli) ? ',' : null).'`'.$k."` = '".esc_sql($this->data[$k])."'";
			}
		}

		if (!is_null($sqli)) {
			if ($insert) {
				$sql = "insert into `".$this->table."` set ".$sqli;
				// Setup Crypt Info
				// $c_key = self::encryptHash($this->data['email']);
				// $c = new Crypt($c_key);
				// $c->genIV();
				// $c_iv = $c->getIV();
				// $sql.= ", crypt_key = ?, crypt_iv = ?";
				// $bind[] = $c_key;
				// $bind[] = $c_iv;
			} else {
				$sql_where = $this->genSQLWhereID();
				$sql = "update `".$this->table."` set ".$sqli." where ".$sql_where;
			}
// error_log(__CLASS__.': '.$sql);
			$dbr = $this->DB->query($sql);
			// if (false===$dbr) {
			if (!$dbr && $this->DB->last_error) {
				$this->error = $this->DB->last_error;
				return false;
			} else {
				if ($insert && 1==count($this->tablePK)) {
					$this->data[$this->tablePK[0]] = $this->DB->insert_id;
				}
				$this->dataOld = $this->data;
			}
		}

		return true;
	}


	function del() {
		$sql_where = $this->genSQLWhereID();
		$sql = "delete from `".$this->table."` where ".$sql_where;
		$dbr = $this->DB->query($sql);
		if (false===$dbr) {
			$this->error = $this->DB->last_error;
			return false;
		} else {
			$this->dataOld = $this->data;
		}
		return true;
	}


	function clearError() {
		$this->error = null;
	}

	function clear() {
		foreach($this->data as $k=>$v) {
			$this->data[$k] = null;
		}
		$this->dataOld = $this->data;
	}
	
	function clearOld() {
		foreach($this->data as $k=>$v) {
			$this->dataOld[$k] = null;
		}
	}
	

	function checkDataValid($d) {
		$return = true;
		foreach($this->dataValidate as $k=>$v) {
			if (isset($d[$k]) && !preg_match($v, $d[$k])) {
// error_log(__CLASS__.':'.__LINE__.': '.$k.' - '.$v.' - '.$d[$k]);
				$this->dataValidError[$k] = true;
				$return = false;
			} else {
// error_log(__CLASS__.':'.__LINE__.':'.$k);
				$this->dataValidError[$k] = false;
			}
		}
		return $return;
	}


	function populate($d=null) {
		if (!$this->checkDataValid($d)) {
			$this->error = 'Data population error.';
			return false;
		} else {
			foreach($this->data as $k=>$v) {
				if (array_key_exists($k, $d)) {
					$this->data[$k] = $d[$k];
				}
			}
		}
		return true;
	}


	function setPK($id) {
		if (!$this->checkDataValid($id)) {
			$this->error = 'Set primary key error.';
			return false;
		}
		foreach($this->tablePK as $i=>$d) {
			if (isset($id[$d]) && 0<strlen($id[$d])) {
				$this->data[$d] = $id[$d];
			}
		}
		return true;
	}


	function getPK() {
		$pk = array();
		foreach($this->tablePK as $i=>$d) {
			$pk[$d] = $this->data[$d];
		}
		return $pk;
	}
	

	function loadData($id) {
		$this->clearError();
		$this->clear();
		$this->setPK($id);
		$dbr = $this->open();
		if (!$dbr) {
			return false;
		} else {
			$this->dataOld = $this->data = isset($dbr) && is_array($dbr) ? $dbr : $this->data;
			return true;
		}
		return false;
	}


	function getDB() {
		return $this->DB;
	}


	function getData() {
		return $this->data;
	}


	function getError() {
		return $this->error;
	}

	function getDataValidErrors() {
		return $this->dataValidError;
	}
}
?>