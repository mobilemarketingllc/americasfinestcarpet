<?php
/**
 * EMAIL CLASS
 *
 * @category   Email - phpmailer
 * @phpmailer  http://phpmailer.worxware.com/?pg=methods
 * @package    Synapse Website Framework
 * @license    Synapse Marketing Solutions
 * @author     Andy Koo <akoo@synapseresults.com>
 * @copyright  Copyright (C) 2014 Synapse Marketing Solutions
 * @version    Release: 1.0
**/


class Email {
	# Class variables.
	protected $email_link		= null;
	protected $fb_share_link	= null;
	protected $unsubscribe_link	= null;
	protected $email_tpl		= null;
	protected $user			= array();
	protected $email_opt		= array();


	function __construct() {
		# Default values.
		$this->email_link		= 'http:'.constant('URL_HTTP').'email/';
		$this->fb_share_link	= 'http:'.constant('URL_HTTP');
		$this->unsubscribe_link	= 'http:'.constant('URL_HTTP').'email/unsubscribe/';
		$this->email_tpl		= 'autoresponder';
		$this->email_opt		= array();
		$this->user				= array(
			'email'	=> null,
			'name'	=> null
		);

		# Current one email responder, default.
		$this->email_opt = array(
                    'autoresponder' => array(
                            'from'			=> array(
                                    'name'		=> 'United Disabilities Services',
                                    'email'		=> constant('FLAG_PROD') ? 'webtech@synapseresults.com' : 'webtech@synapseresults.com'
                            ),
                            'subject'		=> 'None',
                            'tpl'			=> dirname(__FILE__).'/email_templates/',
                            'tpl_fields'	=> array(
                                    '[[BrowserLink]]'		=> $this->email_link,
                                    '[[DOMAIN]]'			=> $_SERVER['HTTP_HOST'],
                                    '[[FB_SHARE_LINK]]'		=> $this->fb_share_link,
                                    '[[UnsubscribeLink]]'	=> $this->unsubscribe_link
                            )
                    )
		);
	}

        function addTemplate($tpl_name, $tpl_options) {
            $this->email_opt[$tpl_name] = $tpl_options;
        }
        
        function sendTemplateEmail($users, $tpl_name) {
            $email_tpl_opt = $this->email_opt[$tpl_name];
            
            # Process template.
            $body_html = str_replace(array_keys($email_tpl_opt['tpl_fields']), array_values($email_tpl_opt['tpl_fields']), file_get_contents($email_tpl_opt['tpl']));
            $body_text = str_replace(array_keys($email_tpl_opt['tpl_fields']), array_values($email_tpl_opt['tpl_fields']), file_get_contents($email_tpl_opt['text_tpl']));
            $this->email($email_tpl_opt['from'], $users, $email_tpl_opt['subject'], $body_html, $body_text, $error);
        }

	function setTemplate($tpl_index) {
		if (isset($this->email_opt[$tpl_index])) {
			$this->email_tpl = $tpl_index;
		}
	}


	# Process selected email template.
	function processEmail($email_flag=false) {
		$email_tpl_opt = $this->email_opt[$this->email_tpl];

		# Process template.
		$body_html = str_replace(array_keys($email_tpl_opt['tpl_fields']), array_values($email_tpl_opt['tpl_fields']), file_get_contents($email_tpl_opt['tpl']));

		if (!$email_flag) {
			return $body_html;
		} else {
			$this->email($email_tpl_opt['from'], array($this->user), $email_tpl_opt['subject'], $body_html, null, $error);
		}
	}



	/*
	FUNCTION	: notifyUs
	DESCRIPTION	: Wrapper function for PHPMailer
	PARAMETERS	:
		from	- array('email'=>string, 'name'=>string)
				- Array for "from" email address and name.
		to		- array(array('email'=>string, 'name'=>string), ...)
				- Array of "to" email address and name.
		subject	- string (Subject of email)
		data	- array (Form data)
	*/
	function notifyUs($from, $to, $subject, $data, &$error) {
		require_once dirname(__FILE__).'/PHPMailer/class.phpmailer.php';

		$body = '<pre>';
		foreach($data as $k=>$v) {
			if (is_array($v)) {
			} else {
				$body.= ucwords(str_replace('_', ' ', $k)).': '.$v.'<br>';
			}
		}
		$body.= '</pre>';

		$mail = new PHPMailer();
		$mail->ClearAllRecipients();

		$mail->SetFrom($from['email'], $from['name']);
		$mail->AddReplyTo($from['email'], $from['name']);
		foreach($to as $i=>$d) {
			$mail->AddAddress($d['email'], $d['name']);
		}

		$mail->Subject = $subject;

		$mail->AltBody = strip_tags(str_replace('<br>', "\r\n", $body)); // TEXT
		$mail->WordWrap = 72;
		$mail->MsgHTML($body);
		$mail->IsHTML(true);

		if (!$mail->Send()) {
			$error = $mail->ErrorInfo;
			return false;
		}

		return true;
	}


	/*
	FUNCTION	: email
	DESCRIPTION	: Wrapper function for PHPMailer
	PARAMETERS	:
		from	- array('email'=>string, 'name'=>string)
				- Array for "from" email address and name.
		to		- array(array('email'=>string, 'name'=>string), ...)
				- Array of "to" email address and name.
		subject	- string (Subject of email)
		html	- string (Body HTML)
		text	- string (Body Text)
	*/
	function email($from, $to, $subject, $html, $text, &$error) {
		require_once 'PHPMailer/class.phpmailer.php';

		$mail = new PHPMailer();

		$mail->SetFrom($from['email'], $from['name']);
		$mail->AddReplyTo($from['email'], $from['name']);
		foreach($to as $i=>$d) {
			$mail->AddAddress($d['email'], $d['name']);
		}
                // Temp add BCC for debugging
                $mail->addBCC('rconsylman@synapseresults.com');
                
		$mail->Subject = $subject;

		if (empty($text)) {
			$text = str_replace(array('<br>', '<br/>', '<br />'), "\n", $html);
		}
		$mail->AltBody = strip_tags($text); // TEXT
		$mail->WordWrap = 72;
		$mail->MsgHTML($html);
		$mail->IsHTML(true);

		if (!$mail->Send()) {
			error_log(__FILE__.': '.__LINE__.': '."Mailer Error: " . $mail->ErrorInfo);
			$error = $mail->ErrorInfo;
			return false;
		}

		return true;
	}
}
?>