<?php
/**
 * Synapse Marketing Solutions Plugin For Customized Builds
 *
 * @license		Synapse Marketing Solutions
 * @copyright	Copyright (C) 2014 Synapse Marketing Solutions
 * @version		Release: Synapse Custom Build
 */

/**
 * Synapse Custom Library for WP Contact Form 7 Validation
 *
 * @category	Validation
 * @package		WordPress CF7 Plugin
 * @author		Andy Koo, Todd Bookman, Ryan Consylman, Joseph Leedy <jleedy@synapseresults.com>
 * @todo		Clean this class up to meet {@link https://make.wordpress.org/core/handbook/coding-standards/php/ WordPress Coding Standards} and document methods properly - Joseph L.
 */
class CF7Validation extends DBTableRecord {
	// protected	$wpdb;		// WP DB Object
	protected	$types;		// CF7 Types
	protected	$blog_id;	// WP Blog ID
	protected	$cf7vRules;	// Cached CF7 Rules
	public		$error;

	/**
	 * @var string
	 */
	protected $example_rule_data_table = 'syn_cf7validation_example_rule_data';

	/**
	 * Constructor
	 */
	public function __construct() {
		global $wpdb;							// WP Database Object
		$this->DB = $wpdb;						// Store WP DB in our object.
		$this->blog_id = get_current_blog_id();	// WP Current Blog ID
		$this->cf7vRules = array();				// Cached CF7 Rules.
		$this->types = array('text', 'email', 'tel', 'url', 'textarea', 'select', 'checkbox');

		# Initialize Database
		$this->data = array();
		$this->table = $this->DB->base_prefix.'synCF7Rules';
		$this->genTableData();
		$this->clearError();
		$this->clear();

		# Run after plugins are loaded and active.
		add_action('plugins_loaded', array(&$this, 'postPluginsLoader'));

		register_activation_hook(
			plugin_dir_path( dirname( __FILE__ ) ) . 'syn-custom.php',
			array( &$this, 'activate_plugin' )
		);
	}

	/**
	 * Prevent users from cloning this object
	 */
	public function __clone() {
		trigger_error( 'Clone is not allowed.', E_USER_ERROR );
	}

	/**
	 * Get Field Types
	 *
	 * @return array
	 */
	public function getFieldTypes() {
		return $this->types;
	}

	/**
	 * Post Plugin Loader
	 *     Enter code here to process after all plugins have loaded.
	 */
	public function postPluginsLoader() {
		$this->addValidationFilter();
	}

	/**
	 * Check if Contact Form 7 plugin is active.
	 */
	public function isCF7Active() {
		$required_plugin = 'contact-form-7/wp-contact-form-7.php';
		$plugin_on = false;
		if ( is_plugin_active( $required_plugin ) ) {
			$plugin_on = true; // Example for yes, it's active
		}
		return $plugin_on;
	}


	/**
	 * Validation for email type:
	 *	<input type="text" />
	 *	<input type="email" />
	 *
	 * @return array
	 */
	public function cf7_syn_validation_filter_func( $result, $tag ) {
		$tag   = new WPCF7_Shortcode( $tag );
		$value = $_POST[ $tag->name ];
        $current_invalid_fields = $result->get_invalid_fields();

		if ( ! $this->validateField( $tag->type, $tag->name, $value ) ) {
            if(isset($current_invalid_fields[$tag->name])) {
                $tag->name .= '::SYN';
            }
            $result->invalidate( $tag, $this->error);
		}

		return $result;
	}

	/*
	 * intended for values not enclosed in normal cf7 tags.
	 * select2 items can be caught here if javascript is applied.
	 * Checkbox arrays are validated to make sure something is selected
	 *
	 * @return array
	 */
	public function cf7_syn_validation_other($result, $tags) {
        $current_invalid_fields = $result->get_invalid_fields();
		$types = array('other', 'checkbox');

        // Needs work before being uncommented
//		foreach($_POST as $name=>$value) {
//			foreach($types as $type) {
//				if(count($this->getRule($type, $name))>0) {
//					if(is_array($value)) {
//						if(count($value) == 0) {
//							$result['valid'] = false;
//							$result['reason'][$name] = $this->error;
//						}
//					}
//					else {
//						if (!$this->validateField($type, $name, $value)) {
//							$result['valid'] = false;
//							$result['reason'][$name] = $this->error;
//						}
//					}
//				}
//			}
//		}

		/*
		 * Check for checkboxes values with nothing set
		 * For checkboxes, the regex is the form id delimited by commas
		 */
		$checkbox_rules = $this->getRulesByType('checkbox');
		foreach($checkbox_rules as $crule) {
			$form_ids_to_check = explode(',', $crule['regex']);
			// Check if this form id is one to check
			if(in_array($_POST['_wpcf7'], $form_ids_to_check)) {
				if(!isset($_POST[$crule['field_name']])) {
                    foreach($tags as $t) {
                        //echo $t['name'].'<br>';
                        if($t['name'] == $crule['field_name']) {
                            db_log_r($crule);
                            $tag = new WPCF7_Shortcode($t);
                            if(isset($current_invalid_fields[$tag->name])) {
                                $tag->name .= '::SYN';
                            }
                            $result->invalidate( $tag, $crule['msg_err']);
                        }
                    } 
				}
			}
		}

		return $result;
	}

	public function cf7_syn_validation_filter_checkbox_func($results, $tag) {
		$type	= 'checkbox';
		$name	= $tag['name'];
		if(isset($_POST[$name])) {
			$value	= $_POST[$name];
			$rules = $this->getRulesByType($type);
		}

		return $results;
	}
    
    /*
     * With new CF7 object you cannot override existing errors from cf7, at least not
     * that I could see. Instead these are appended with ::SYN and the override class
     * below converts them and creates a psuedo copy of the cf7 validation class to return
     */
    public function cf7_syn_error_filter($result) {
        $SynWPCF7_Validation = new SynWPCF7_Validation();
        $SynWPCF7_Validation->cast($result);
        
        return $SynWPCF7_Validation;
    }

	/*
	 * based on CF7 module file.php
	 * Tells the user what files types are required if needed
	 */
	public function cf7_syn_validation_filter_file_func_override( $result, $tag ) {
		require_once WP_CONTENT_DIR.'/plugins/contact-form-7/includes/functions.php';
		require_once WP_CONTENT_DIR.'/plugins/contact-form-7/includes/shortcodes.php';
		require_once WP_CONTENT_DIR.'/plugins/contact-form-7/modules/file.php';

		$tag = new WPCF7_Shortcode( $tag );

		$name = $tag->name;
		$id = $tag->get_id_option();

		$file = isset( $_FILES[$name] ) ? $_FILES[$name] : null;

		if ( $file['error'] && UPLOAD_ERR_NO_FILE != $file['error'] ) {
			$result['valid'] = false;
			$result['reason'][$name] = wpcf7_get_message( 'upload_failed_php_error' );
			$result['idref'][$name] = $id ? $id : null;
			return $result;
		}

		if ( empty( $file['tmp_name'] ) && $tag->is_required() ) {
			$result['valid'] = false;
			$result['reason'][$name] = wpcf7_get_message( 'invalid_required' );
			$result['idref'][$name] = $id ? $id : null;
			return $result;
		}

		if ( ! is_uploaded_file( $file['tmp_name'] ) )
			return $result;

		$allowed_file_types = array();

		if ( $file_types_a = $tag->get_option( 'filetypes' ) ) {
			foreach ( $file_types_a as $file_types ) {
				$file_types = explode( '|', $file_types );

				foreach ( $file_types as $file_type ) {
					$file_type = trim( $file_type, '.' );
					$file_type = str_replace( array( '.', '+', '*', '?' ),
						array( '\.', '\+', '\*', '\?' ), $file_type );
					$allowed_file_types[] = $file_type;
				}
			}
		}

		$allowed_file_types = array_unique( $allowed_file_types );
		$file_type_pattern = implode( '|', $allowed_file_types );

		$allowed_size = 1048576; // default size 1 MB

		if ( $file_size_a = $tag->get_option( 'limit' ) ) {
			$limit_pattern = '/^([1-9][0-9]*)([kKmM]?[bB])?$/';

			foreach ( $file_size_a as $file_size ) {
				if ( preg_match( $limit_pattern, $file_size, $matches ) ) {
					$allowed_size = (int) $matches[1];

					if ( ! empty( $matches[2] ) ) {
						$kbmb = strtolower( $matches[2] );

						if ( 'kb' == $kbmb )
							$allowed_size *= 1024;
						elseif ( 'mb' == $kbmb )
							$allowed_size *= 1024 * 1024;
					}

					break;
				}
			}
		}

		// File type validation //

		// Default file-type restriction
		if ( '' == $file_type_pattern )
			$file_type_pattern = 'jpg|jpeg|png|gif|pdf|doc|docx|ppt|pptx|odt|avi|ogg|m4a|mov|mp3|mp4|mpg|wav|wmv';

		$file_type_pattern = trim( $file_type_pattern, '|' );
		$file_type_pattern = '(' . $file_type_pattern . ')';
		$file_type_pattern = '/\.' . $file_type_pattern . '$/i';

		if ( ! preg_match( $file_type_pattern, $file['name'] ) ) {
			$result['valid'] = false;
			$result['reason'][$name] = wpcf7_get_message( 'upload_file_type_invalid' ).' Please use one of the following file types: '.implode(', ', $allowed_file_types);
			$result['idref'][$name] = $id ? $id : null;
			return $result;
		}

		// File size validation //

		if ( $file['size'] > $allowed_size ) {
			$result['valid'] = false;
			$result['reason'][$name] = wpcf7_get_message( 'upload_file_too_large' );
			$result['idref'][$name] = $id ? $id : null;
			return $result;
		}

		return $result;
	}

	public function getBlogId() {
		return $this->blog_id;
	}

	public function setBlogId($blog_id) {
		if (is_numeric($blog_id)) {
			return $this->blog_id = $blog_id;
		}
		return false;
	}

	/**
	 * Get domain
	 *
	 * @return string
	 */
	public function get_domain() {
		return preg_replace('/(http|https)\:\/\//', '', get_site_url());
	}

	/**
	 * Get WordPress Sites
	 *
	 * @return array
	 */
	public function get_sites() {
		if (is_multisite()) {
			return wp_get_sites();
		} else {
			return array(array('blog_id'=>$this->blog_id,'domain'=>$this->get_domain()));
		}
	}

	# Get all CF7 rules from database or cache.
	public function getAllRules() {
		# Use cached rules unless one of the criteria are not met.
		if (1>count($this->cf7vRules) || !isset($this->cf7vRules[0]['blog_id']) || $this->blog_id!=$this->cf7vRules[0]['blog_id']) {
			$bind = array($this->blog_id);
			if (is_multisite()) {
				$sql = "select cf7r.*, b.`domain` from `".$this->table."` cf7r left outer join `".$this->DB->base_prefix."blogs` b on b.`blog_id` = cf7r.`blog_id` where cf7r.`blog_id` = %d or cf7r.`blog_id` = 0 order by cf7r.`blog_id`,cf7r.`id`, cf7r.`field_type`, cf7r.`field_name`, cf7r.`rank`";
			} else {
				$domain = $this->get_domain();
				$sql = "select cf7r.*, '".$domain."' as 'domain' from `".$this->table."` cf7r where cf7r.`blog_id` = %d or cf7r.`blog_id` = 0 order by cf7r.`blog_id`, cf7r.`id`, cf7r.`field_type`, cf7r.`field_name`, cf7r.`rank`";
			}
			$wpdb_r = $this->DB->get_results($this->DB->prepare($sql, $bind), ARRAY_A);
			if (false===$wpdb_r) {
				$this->error = self::$SC->wpdb->last_error;
				return false;
			}
			$this->cf7vRules = $wpdb_r;
		}
		return $this->cf7vRules;
	}

	# Get CF7 rule by Id.
	public function getRuleById($id) {
		$this->loadData(array('id'=>$id));
		return $this->getData();
	}

	# Add / Edit CF7 rule.
	public function modRule($data) {
        if(isset($data['id']) && $data['id'] != '' && $data['id'] > 0) {
            $this->loadData(array('id'=>$data['id']));
        }
		if (!$this->populate($data)) {
			return false;
		}
		return $this->save();
	}

	# Delete CF7 rule.
	public function delRule($id) {
		$data = array('id'=>$id);
		if (!$this->populate($data)) {
			return false;
		}
		return $this->del();
	}

	public function getRulesByType($type) {
		$return = array();
		$this->getAllRules();
		foreach($this->cf7vRules as $cf7vRules_d) {
			if($type == $cf7vRules_d['field_type']) {
				$return[] = $cf7vRules_d;
			}
		}
		return $return;
	}

	# Search for rule.
	public function getRule($type, $field_name=null) {
		$return = array();
		$type = rtrim($type, '*');
		$this->getAllRules();
		$delimiters = array(',', '|');
		foreach($this->cf7vRules as $cf7vRules_i=>$cf7vRules_d) {
			$field_name_list = array();
			foreach($delimiters as $delimiter) {
				if (false!==strpos($cf7vRules_d['field_name'], $delimiter)) {
					$field_name_list = explode($delimiter, $cf7vRules_d['field_name']);
					break;
				}
			}

			# Match field type and field name. Field name = null = wildcard.
			if ($type==$cf7vRules_d['field_type'] && (is_null($cf7vRules_d['field_name']) || $field_name==$cf7vRules_d['field_name'] || in_array($field_name, $field_name_list))) {
				$return[] = $cf7vRules_d;
			}
		}
		return $return;
	}

	# Execute function
	#	This function is used instead of "eval" to avoid security issue.
	#	Parameters: (function_name, parameter1, parameter2, ...)
	public function execFunc() {
		if (1>func_num_args()) {
			return false;
		}
		$parameters = func_get_args();
		switch($parameters[0]) {
			case 'empty':
				return empty($parameters[1]);
			case 'characterCount':
				return !characterCount($parameters[1]);
			case 'duplicateAccount':
				return email_exists($parameters[1]);
			case 'validEmail':
				return !validEmail($parameters[1]);
			case 'validEmailUniqueness':
				return !validEmailUniqueness($parameters[1]);
			case 'validateUsername':
				return !validateUsername($parameters[1]);
		}
		return null;
	}

	# Validate field based on validation rules.
	#	This handled custom function calls and regular expressions.
	public function validateField($type, $name, $value) {
		//db_log('Type:'.print_r($type, true).', Name:'.print_r($name, true).', Value:'.print_r($value, true));
		$rules = $this->getRule($type, $name);
        if(count($rules) > 1) {
            usort($rules,'rank_sort');
        }
        
		foreach($rules as $rule) {
			if ((isset($rule['func']) || isset($rule['regex'])) && isset($rule['msg_err'])) {
				if (!empty($rule['func'])) {
					$ef = $this->execFunc($rule['func'], $value);
					if (!is_null($ef) && $ef) {
						$this->error = $rule['msg_err'];
						return false;
					}
				} elseif (!empty($rule['regex'])) {
					if (!preg_match($rule['regex'], $value)) {
						$this->error = $rule['msg_err'];
						return false;
					}
				}
			}
		}
		return true;
	}

	# Check for duplicate field submissions to the form
	public function is_already_submitted($formName, $fieldName, $fieldValue) {
		require_once(ABSPATH . 'wp-content/plugins/contact-form-7-to-database-extension/CFDBFormIterator.php');
		$exp = new CFDBFormIterator();
		$atts = array();
		$atts['show'] = $fieldName;
		$atts['filter'] = "$fieldName=$fieldValue";
		$exp->export($formName, $atts);
		$found = false;
		while ($row = $exp->nextRow()) {
			$found = true;
		}
		return $found;
	}


	# Add custom CF7 filters to WordPress.
	public function addValidationFilter() {
        $filter_types = array('email','tel','text','textarea','url','select');
        foreach($filter_types as $ftype) {
            add_filter('wpcf7_validate_'.$ftype, array(&$this, 'cf7_syn_validation_filter_func'), 10, 2);
            add_filter('wpcf7_validate_'.$ftype.'*', array(&$this, 'cf7_syn_validation_filter_func'), 10, 2);
        }
		add_filter('wpcf7_validate_file', array(&$this, 'cf7_syn_validation_filter_file_func_override'), 10, 2);
		add_filter('wpcf7_validate_file*', array(&$this, 'cf7_syn_validation_filter_file_func_override'), 10, 2);
		add_filter('wpcf7_validate', array(&$this, 'cf7_syn_validation_other'),10, 2);
        add_filter('wpcf7_validate', array(&$this, 'cf7_syn_error_filter'),11);
	}

	/**
	 * Run necessary tasks when the plug-in is activated
	 *
	 * @author Joseph Leedy <jleedy@synapseresults.com>
	 */
	public function activate_plugin() {
		$this->create_rule_table();
		$this->create_example_rule_data_table();
	}
    
        public function sort_for_display($rules) {
        $sorted_rules = array();
        
        foreach($rules as $rule) {
            foreach($rules as $r) {
                if($r['field_name'] == $rule['field_name'] && !in_array($r, $sorted_rules) && $r['rank'] < $rule['rank']) {
                    $sorted_rules[] = $r;
                }
            }
            if(!in_array($rule, $sorted_rules)) {
                $sorted_rules[] = $rule;
            }
            foreach($rules as $r) {
                if($r['field_name'] == $rule['field_name'] && !in_array($r, $sorted_rules) && $r['rank'] > $rule['rank']) {
                    $sorted_rules[] = $r;
                }
            }
        }
        
        return $sorted_rules;
    }

	/**
	 * Setup the database table that will contain the validation rules
	 *
	 * @author Joseph Leedy <jleedy@synapseresults.com>
	 */
	public function create_rule_table() {
        $table_name = $this->table;
        $prefix = $this->DB->prefix;
        $base_table_name = str_replace($prefix, '', $table_name);
		$this->create_database_table($base_table_name);
	}

	/**
	 * Setup the database table that contains the example rule data
	 *
	 * @author Joseph Leedy <jleedy@synapseresults.com>
	 */
	public function create_example_rule_data_table() {
		$this->create_database_table($this->example_rule_data_table);
	}

	/**
	 * Get example validation rule data
	 *
	 * @param boolean $return_grouped Whether to group the rule data by type (ex. "function" or "regex")
	 * @return array Array of stdClass objects representing the example data with the following properties:
	 *     rule_id: The rule's unique identifier
	 *     rule_type: The type of example rule (ex. "function" or "regex")
	 *     rule_name: The name of the example rule
	 *     rule_data: The value of the example rule
	 *     rule_description: A summary of what the example rule does
	 * @author Joseph Leedy <jleedy@synapseresults.com>
	 */
	public function get_example_rule_data( $return_grouped = false ) {
		$table     = $this->DB->prefix . $this->example_rule_data_table;
		$rule_data = $this->DB->get_results( "SELECT * FROM {$table};" );

		if ( $rule_data && $return_grouped ) {
			$grouped_rule_data = array();

			foreach ( $rule_data as $rule ) {
				if ( ! array_key_exists( $rule->rule_type, $grouped_rule_data ) ) {
					$grouped_rule_data[ $rule->rule_type ] = array();
				}

				$grouped_rule_data[ $rule->rule_type ][] = $rule;
			}

			$rule_data = $grouped_rule_data;
		}

		return $rule_data;
	}

	/**
	 * Creates a new table in the WordPress database from a data file
	 *
	 * @param string $table_name
	 * @author Joseph Leedy <jleedy@synapseresults.com>
	 */
	protected function create_database_table( $table_name ) {
        $table_prefix = $this->DB->prefix;
        // Check if the table prefix is already on the table name
        if(strrpos($table_name, $table_prefix, -strlen($table_name)) !== FALSE) {
            // Table prefix is already appended
            $base_table_name = str_replace($table_prefix, '', $table_name);
        }
        else {
            $base_table_name = $table_name;
            $table_name = $table_prefix.$base_table_name;
        }
        
		// Bail if the database table was already created
		if ( get_option( $table_name . '_installed', false ) ) {
			return;
		}

		
		$sql = file_get_contents( plugin_dir_path( dirname( __FILE__ ) ) . '/data/' . $base_table_name . '.sql' );
		$error = 'Could not create database table "' . $table_name . '"!';

		if ( empty( $sql ) ) {
			throw new Exception( $error.': '.$sql );
		}

		$sql = str_replace( '{{prefix}}', $table_prefix, $sql );
		$queries = explode( "\n\n", $sql );

		foreach ( $queries as $query ) {
            if($query == '') continue;
            
			if ( $this->DB->query( $query ) === false ) {
				throw new Exception( $error.': Error on sql query call: '.$query );
			}
		}
        
        // Check if rules table is empty
        // If it is, install initial rules
        $rules = $this->getAllRules();
        if(count($rules) == 0) {
            $sql = file_get_contents( plugin_dir_path( dirname( __FILE__ ) ) . '/data/initial_rules.sql' );
            $error = 'Could not insert initial rules for "' . $table_name;
            if ( empty( $sql ) ) {
                throw new Exception( $error.': Could not gather sql: '.$sql);
            }
            $sql = str_replace( '{{prefix}}', $table_prefix, $sql );
            $this->DB->query($sql);
        }

		// Set a flag stating that the database table has been created
		add_option( $table_name . '_installed', true );
	}
}

function rank_sort($a, $b) {
    return strcmp($a['rank'], $b['rank']);
}