<?php
$errors = array('main'=>null);
$db_field_names = array();
$db_records = array();
$uri_fid = null;

// echo '<pre>'.print_r($_GET, true).'</pre>';

// Get all forms ids
$db_form_ids = $this->get_form_ids($error);
// echo '<pre>'.print_r($db_form_ids,true).'</pre>';

if (isset($_GET['page'], $_GET['fid']) && $this->page_admin==$_GET['page'] && !empty($_GET['fid']) && $this->check_form_id($_GET['fid'])) {
	$uri_fid = '&fid='.urlencode($_GET['fid']);

	$this->get_form_data_by_form_id($_GET['fid'], $db_field_names, $db_records, $errors);

// echo '<pre>'.print_r($db_records,true).'</pre>';

	// Download must be down at plugin initialization before any action or output.
	// if (isset($_GET['dl']) && 'excel'==$_GET['dl']) {
		// $this->download_excel($db_field_names, $db_records);
	// }
}

$uri_parse = explode('&', $_SERVER['REQUEST_URI']);
$uri_base = $uri_parse[0];
?>
<style>
table { width:100%; }
thead { background-color:#ddd; }
tbody tr:hover { background-color:#ffc; }
</style>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

<script>

</script>

<div class="wrap">
	<h2><?php echo $this->name; ?> - Custom Build</h2>
	<h3>Custom plug-in for WordPress</h3>
	<h4>
		
<?php
$o = null;
foreach($db_form_ids as $i=>$d) {
	$o.= (!is_null($o) ? ' | ' : null).'<a href="'.$uri_base.'&fid='.urlencode($d['form_id']).'">'.$d['form_id'].'</a>';
}
echo !is_null($o) ? 'Choose a form to see data:<br/>'.$o : 'No form data is found. Please check back later when form data is available.';
?>
	</h4>

<?php
if (!is_null($uri_fid)) {
?>
	Download: <a href="<?= $uri_base.$uri_fid ?>&dl=excel">Excel</a>
	<table class="wp-list-table widefat">
		<thead>
			<tr>
<?php
	$o = null;
	foreach($db_field_names as $i=>$d) {
		$o.= '<td>'.ucwords(str_replace(array('_', '-'), ' ', $d['field_name'])).'</td>';
	}
	echo $o;
?>
			</tr>
		</thead>
		<tbody>
<?php
	$o = null;
	foreach($db_records as $ri=>$rd) {
		$o.= '<tr>';
		foreach($db_field_names as $fni=>$fnd) {
			$o.= '<td>'.$rd[$fnd['field_name']].'</td>';
		}
		$o.= '</tr>';
	}
	echo $o;
?>
		</tbody>
	</table>
<?php
}
?>
</div>