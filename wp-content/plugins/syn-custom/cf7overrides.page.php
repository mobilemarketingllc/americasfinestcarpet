<?php
$email_fields = array(
    'cc_email', 'cc_addresses', 'override_email', 'override_addresses'
);

if(isset($_POST['cc_email'])) {
    cf7_email_overrides_submission($email_fields);
}

$email_values = array();
foreach($email_fields as $efield) {
    $option = get_option('syn_'.$efield);
    if(!$option) {
        switch($efield) {
            case 'cc_email':
            case 'override_email':
                $option = 'no';
                break;
            case 'cc_addresses':
            case 'override_addresses':
                $option = '';
                break;
        }
    }
    $email_values[$efield] = $option;
}
?>
<h2><?php _e( 'Addon for CF7 Plug-in for WordPress', 'syn-custom' ) ?></h2>

<div>
	<p><?php _e( 'This plugin adds additional validation to Contact Form 7.<br><strong>Do not uninstall or delete this plugin or your website will not run properly.</strong>', 'syn-custom' ) ?></p>
</div>

<h3><?php _e( 'Contact Form 7 Email Overrides', 'syn-custom' ) ?></h3>

<div class="form-wrapper">
    <form class="style2-form" method="post" action="">
        <div class="form-row">
            <div class="row-label">BCC all outgoing emails?</div>
            <div class="row-fields radio-group">
                <label>
                    Yes
                    <input type="radio" name="cc_email" value="yes" <?=$email_values['cc_email']=='yes'?'checked':''?>/>
                </label>
                <label>
                    No
                    <input type="radio" name="cc_email" value="no" <?=$email_values['cc_email']=='no'?'checked':''?>/>
                </label>
            </div>
        </div>
        <div class="clear"></div>
        <div class="form-row">
            <div class="row-label">BCC Email Address(es):</div>
            <div class="row-fields">
                <input type="text" name="cc_addresses" value="<?=$email_values['cc_addresses']?>"/>
                <span class="field-comment">
                    (comma-separated list)
                </span>
            </div>
        </div>
        <div class="clear"></div>
        <br>
        <div class="form-row">
            <div class="row-label">Override all outgoing emails?</div>
            <div class="row-fields radio-group">
                <label>
                    Yes
                    <input type="radio" name="override_email" value="yes" <?=$email_values['override_email']=='yes'?'checked':''?>/>
                </label>
                <label>
                    No
                    <input type="radio" name="override_email" value="no" <?=$email_values['override_email']=='no'?'checked':''?>/>
                </label>
                <span class="field-comment">
                    (this will block all outgoing mail and divert it to the address(es) below)
                </span>
            </div>
        </div>
        <div class="clear"></div>
        <div class="form-row">
            <div class="row-label">Override Email Address(es):</div>
            <div class="row-fields">
                <input type="text" name="override_addresses" value="<?=$email_values['override_addresses']?>"/>
                <span class="field-comment">
                    (comma-separated list)
                </span>
            </div>
        </div>
        <div class="clear"></div>
        <div class="form-row button-row">
            <button class="button-primary">Save</button>
            <span class="error-message"></span>
        </div>
        <div class="clear"></div>
    </form>
</div>

<script type="text/javascript">
if(typeof($) == 'undefined') {
    var $ = jQuery;
}
    
$(function(){
    $('form.style2-form').on('submit', function(){
        return validateOverrideForm(this);
    });
    
    $('form.style2-form input[name=override_email]').on('change', function(){
        overrideEmailChange();
    });
});
   
function validateOverrideForm(form) {
    var cc_value = $('form.style2-form input[name=cc_email]:checked').val();
    if(cc_value == 'yes') {
        var cc_addresses = $.trim($('form.style2-form input[name=cc_addresses]').val());
        if(cc_addresses == '') {
            $(form).find('.error-message').html('Please enter at least one CC email address.');
            return false;
        }
    }
    
    var override_value = $('form.style2-form input[name=override_email]:checked').val();
    if(override_value == 'yes') {
        var override_addresses = $.trim($('form.style2-form input[name=override_addresses]').val());
        if(override_addresses == '') {
            $(form).find('.error-message').html('Please enter at least one override email address.');
            return false;
        }
    }
    
    return true;
}

function overrideEmailChange() {
    var value = $('form.style2-form input[name=override_email]:checked').val();
    
    if(value == 'yes') {
        $('form.style2-form input[name=cc_email][value=no]').prop('checked', true);
    }
}
</script>

<?php 
function cf7_email_overrides_submission($fields) {
    foreach($fields as $field) {
        if(isset($_POST[$field])) {
            update_option('syn_'.$field, $_POST[$field]);
        }
    }
}