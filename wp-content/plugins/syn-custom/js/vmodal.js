/* 
 * A JS object to handle modals
 * 
 * Built by Ryan Consylman, 2014
 */


var VModal = {
    ModalContainerID: 'vmodal-modal-container',
    OverlayID: 'vmodal-modal-overlay',
    FadeInSpeed: 400,
    FadeOutSpeed: 200,
    
    Block: {
        
        /*
         * A UI blocking overlay to be called while a background
         * operation is in process and you do not want the user to perform
         * any actions.
         * 
         * Parameters in the object: (all are optional)
         *      html:           Any html to display in the blocked ui
         *      classes:        A class or classes to give the specific block element
         */
        Show: function(o) {
            var $container = VModal._Init();
            var block_id = 'vmodal-block-ui';
            
            VModal._ShowOverlay();
            
            $('#'+VModal.OverlayID).css({
                zIndex: 200
            });
            
            var $modal = $('#'+block_id);
            
            if($('#'+block_id).length == 0) {
                // Create the html
                $modal = $('<div id="'+block_id+'" class="vmodal-block-ui" />');
                
                $container.append($modal);
            }
            
            $modal.html(o.html ? o.html : '');
            if(o.classes) {
                $modal.removeClass();
                $modal.addClass('vmodal-block-ui '+o.classes);
            }
            
            VModal._ShowModal($modal);
        },
        
        Hide: function() {
            var $modal = $('#vmodal-block-ui');
            $('#'+VModal.OverlayID).css({
                zIndex: 90
            });
            VModal._CloseModal($modal);
        }
    },

    /*
     * Checks if the modal container exists and creates
     * it if it does not.
     * Returns the jQuery object for the container.
     */
    _Init: function() {
        var id = VModal.ModalContainerID;
        var overlay_id = VModal.OverlayID;

        if($('#'+id).length == 0) {
            // Create the modal container
            var $container = $('<div id="'+id+'" />');

            $('body').append($container);

            // Create the overlay html
            var $overlay = $('<div id="'+overlay_id+'" />');

            $('body').append($overlay);

            return $container;
        }
        else {
            return $('#'+id);
        }
    },

    _AddButton: function($modal, b) {
        var $button = $('<button />');
        $button.html(b.html);

        if(b.classes) {
            $button.addClass(b.classes);
        }
        else {
            $button.addClass('button red');
        }

        $button.click(function(){
            b.click($modal);
        });

        $modal.find('.vmodal-modal-footer').append($button);
    },

    _AddExitButton: function($modal) {
        var $exit_button = $modal.find('.vmodal-exit-button');
        if($exit_button.length <= 0) {
            var $exit_button = $('<button class="vmodal-exit-button">x</button>');
            $modal.prepend($exit_button);
        }
        
        $exit_button.unbind();
        $exit_button.click(function(){
            VModal._CloseModal($modal);
        });
    },

    _AddTitleIcon: function($modal, icon) {
        var $icon = $('<span class="vmodal-title-icon"></span>');
        $icon.append(icon);
        $modal.find('.vmodal-modal-header').prepend($icon);
    },

    /*
     * Centers the modal height, or if the modal is
     * large than the screen, sets it top to a minimum
     * distance
     */
    _CenterHeight: function($modal) {
        var min_distance = 40;
        var screenHeight = $(window).height();
        // Get current display setting
        var display = $modal.css('display');

        // Transform to get the auto set height of the modal
        $modal.css({
            position:   'absolute', 
            visibility: 'hidden',
            display:    'block'
        });

        var modalHeight = $modal.outerHeight();

        // Transform back
        $modal.css({
            visibility: 'visible',
            display:    display
        });

        var shift = (screenHeight - modalHeight) / 2;
        shift = Math.max(shift, min_distance);

        var vert_offset = $(document).scrollTop();
        shift += vert_offset;

        $modal.css('top',shift+'px');
    },

    _CenterWidth: function($modal) {
        var modal_width = $modal.outerWidth();
        var page_width = $('body').outerWidth();
        $modal.css('left', (page_width-modal_width)/2+'px');
    },

    _CloseModal: function($modal) {
        var speed = VModal.FadeOutSpeed;
        if($modal.data('fadeoutspeed')) {
            speed = $modal.data('fadeoutspeed');
        }
        
        $modal.fadeOut(speed, function(){
            VModal._RemoveOverlay();
            var onClose = $modal.data('onClose');
            if(typeof onClose !== 'undefined') {
                if(typeof(onClose) === 'function') {
                    onClose();
                }
                else {
                    eval(onClose);
                }
            }
        });
    },

    _GetFooter: function() {
        var $footer = $('<div class="vmodal-modal-footer modal-element-clear" />');
        return $footer;
    },

    _GetBody: function() {
        var $body = $('<div class="vmodal-modal-body modal-element-clear" />');
        return $body;
    },

    _GetHeader: function() {
        var $header = $('<div class="vmodal-modal-header modal-element-clear" />');
        return $header;
    },

    _HandleScroll: function($modal) {
        var screenHeight = $(window).height();
        var modalHeight = $modal.outerHeight();
        var currentPosition = $(document).scrollTop();
        var offset = 40;

        if(modalHeight+(offset*2) > screenHeight) {
            // Determine whether the user is scrolling up or down
            var modalTop = parseInt($modal.css('top'));

            if(VModal.LastPosition - currentPosition >= 0) {
                // scrolling up
                if(currentPosition+40 < modalTop) {
                    $modal.css('top',currentPosition+40+'px');
                }
            }
            else {
                // scrolling down
                var bottomOfViewport = currentPosition + screenHeight;
                var bottomOfModal = modalTop + modalHeight;
                if((bottomOfModal + offset) < bottomOfViewport) {
                    var shift = $(document).scrollTop();
                    shift += screenHeight - offset;
                    shift -= modalHeight;

                    $modal.css('top',shift+'px');
                }
            }
        }
        else {
            VModal._CenterHeight($modal);
        }

        VModal.LastPosition = currentPosition;
    },

    _ShowModal: function($modal, onBeforeOpen, onOpen) {
        VModal._CenterWidth($modal);
        VModal._CenterHeight($modal);
        
        if(onBeforeOpen) {
            if(typeof(onBeforeOpen) === 'function') {
                onBeforeOpen();
            }
            else {
                eval(onBeforeOpen);
            }
        }
        
        var speed = VModal.FadeInSpeed;
        if($modal.data('fadeinspeed')) {
            speed = $modal.data('fadeinspeed');
        }
        
        $modal.fadeIn(speed, function(){
            if(onOpen) {
                if(typeof(onOpen) === 'function') {
                    onOpen();
                }
                else {
                    eval(onOpen);
                }
            }
        });

        $(window).resize(function(){
            VModal._CenterWidth($modal);
            VModal._CenterHeight($modal);
        });

        $(document).scroll(function(){
            VModal._HandleScroll($modal);
        });
    },

    _ShowOverlay: function() {
        $('#'+VModal.OverlayID).css('display', 'block');
    },

    _RemoveOverlay: function() {
        $('#'+VModal.OverlayID).css('display', 'none');
    },

    /*
     * Create a basic modal that imitates the
     * old 'alert'. Takes an object containing 
     * the following parameters:
     * REQUIRED:
     *      message:        Text displayed in the modal
     * OPTIONAL:
     *      title:          A title to display. Without one, the modal header does not show and an extra class is added to the body
     *      icon:           html icon element to display in the title bar
     *      width:          defaults to none, allowing css to define the width
     *      hasExitButton:  Sets whether a header exit button is present in the title, defaults to true
     *      onClose:        optional callback on close
     *      classes:        a string of any extra classes you want added to the modal
     */
    Alert: function(o) {
        o = $.extend({
            width: o.width ? o.width+'px' : '',
            hasExitButton: o.hasExitButton ? o.hasExitButton : true,
            classes: o.classes ? o.classes : ''
        },o);

        var $container = VModal._Init();
        var alert_id = 'vmodal-alert-modal';

        VModal._ShowOverlay();
        var $modal = $('#'+alert_id);

        if($('#'+alert_id).length == 0) {
            // Create the html
            $modal = $('<div id="'+alert_id+'" class="vmodal-modal '+o.classes+'" />');

            if(o.title) {
                $modal.append(VModal._GetHeader());
                $modal.find('.vmodal-modal-header').html(o.title);
                if(o.hasExitButton) {
                    VModal._AddExitButton($modal);
                }
                if(o.icon) {
                    VModal._AddTitleIcon($modal, o.icon);
                }
            }
            else {
                $modal.find('.vmodal-modal-body').addClass('vmodal-modal-body-no-header');
            }
            $modal.append(VModal._GetBody());
            $modal.append(VModal._GetFooter());

            // Alert gets a basic confirmation button
            var $close_button = $('<button>OK</button>');
            $close_button.click(function() {
                VModal._CloseModal($modal);
            });
            $modal.find('.vmodal-modal-footer').append($close_button);

            $container.append($modal);
        }
        else {
            // Modify existing html to our needs

            if(o.title) {
                if($modal.find('.vmodal-modal-header').length == 0) {
                    $modal.prepend(VModal._GetHeader());
                }
                $modal.find('.vmodal-modal-header').html(o.title);

                if(o.icon) {
                    VModal._AddTitleIcon($modal, o.icon);
                }

                if(o.hasExitButton) {
                    if($modal.find('.vmodal-modal-header .vmodal-exit-button').length == 0) {
                        VModal._AddExitButton($modal);
                    }
                }

                if($modal.find('.vmodal-modal-header').hasClass('vmodal-modal-body-no-header')) {
                    $modal.find('.vmodal-modal-header').removeClass('vmodal-modal-body-no-header');
                }
            }
            else {
                if($modal.find('.vmodal-modal-header').length > 0) {
                    $modal.find('.vmodal-modal-header').remove();
                    $modal.find('.vmodal-modal-body').addClass('vmodal-modal-body-no-header');
                }
            }

            // Strip any extra classes that might have been added and add necessary ones
            $modal.removeClass();
            $modal.addClass('vmodal-modal '+o.classes);
        }
        if(o.onClose) {
            $modal.data('onClose', o.onClose);
        }
        $modal.css('width', o.width);
        $modal.find('.vmodal-modal-body').html(o.message);
        VModal._ShowModal($modal);
    },

    /*
     * Create a modal with your own html
     * Takes an object containing 
     * the following parameters:
     * REQUIRED:
     *      html:           Html content to display with the modal

     * OPTIONAL:
     *      width:          defaults to none, allowing css to define the width
     *      classes:        a string of any extra classes you want added to the modal
     *      onBeforeOpen:   function to call when the modal is rendered but not yet visible
     *      onOpen:         function to call when the modal is fully visible
     *      onClose:        optional callback on close
     */
    Clean: function(o) {
        o = $.extend({
            width: o.width ? o.width+'px' : '',
            classes: o.classes ? o.classes : ''
        },o);

        var $container = VModal._Init();
        var modal_id = 'vmodal-clean-modal';

        VModal._ShowOverlay();
        var $modal = $('#'+modal_id);

        if($('#'+modal_id).length == 0) {
            // Create the html
            $modal = $('<div id="'+modal_id+'" class="vmodal-modal '+o.classes+'" />');

            $modal.append(VModal._GetBody());

            $container.append($modal);
        }
        else {
            // Strip any extra classes that might have been added and add necessary ones
            $modal.removeClass();
            $modal.addClass('vmodal-modal '+o.classes);
        }

        if(o.onClose) {
            $modal.data('onClose', o.onClose);
        }
        $modal.css('width', o.width);
        $modal.find('.vmodal-modal-body').html(o.html);
        VModal._ShowModal($modal, o.onBeforeOpen, o.onOpen);
    },

    /*
     * Create a basic confirm modal
     * Takes an object containing 
     * the following parameters:
     * REQUIRED:
     *      title:          A title for the modal
     *      message:        Text displayed in the modal
     *      onAccept:       function to execute on acceptance
     * OPTIONAL:
     *      onReject:       function to execute on rejection, defaults to just closing the modal
     *      acceptText:     Text for accept button, defaults to 'Yes'
     *      rejectText:     Text for reject button, defaults to 'No'
     *      acceptClass:    Any additional classes for the accept button
     *      rejectClass:    Any additional classes for the reject button
     *      icon:           html icon element to display in the title bar (or any html)
     *      width:          defaults to none, allowing css to define the width
     *      hasExitButton:  Sets whether a header exit button is present in the title, defaults to true
     *      classes:        a string of any extra classes you want added to the modal
     *      onBeforeOpen:   function to call when the modal is rendered but not yet visible
     *      onOpen:         function to call when the modal is fully visible
     *      onClose:        optional callback on close
     */
    Confirm: function(o) {
        o = $.extend({
            width: o.width ? o.width+'px' : '',
            hasExitButton: o.hasExitButton ? o.hasExitButton : true,
            classes: o.classes ? o.classes : ''
        },o);

        var $container = VModal._Init();
        var modal_id = 'vmodal-confirm-modal';

        VModal._ShowOverlay();
        var $modal = $('#'+modal_id);

        if($('#'+modal_id).length == 0) {
            // Create the html
            $modal = $('<div id="'+modal_id+'" class="vmodal-modal '+o.classes+'" />');

            $modal.append(VModal._GetHeader());
            $modal.append(VModal._GetBody());
            $modal.append(VModal._GetFooter());

            $container.append($modal);
        }
        else {
            $modal.find('.vmodal-modal-footer').empty();

            // Strip any extra classes that might have been added and add necessary ones
            $modal.removeClass();
            $modal.addClass('vmodal-modal '+o.classes);
        }

        var buttons = [
            {
                classes: o.rejectClass ? o.rejectClass : '',
                html: o.rejectText ? o.rejectText : 'No',
                click: o.onReject ? o.onReject : function() {
                    VModal._CloseModal($modal);
                }
            },
            {
                classes: o.acceptClass ? o.acceptClass : '',
                html: o.acceptText ? o.acceptText : 'Yes',
                click: o.onAccept
            }
        ];

        // Add buttons
        $.each(buttons, function(a, b){
            VModal._AddButton($modal, b);
        });

        $modal.find('.vmodal-modal-header').html(o.title);
        if(o.hasExitButton) {
            VModal._AddExitButton($modal);
        }
        if(o.icon) {
            VModal._AddTitleIcon($modal, o.icon);
        }
        if(o.onClose) {
            $modal.data('onClose', o.onClose);
        }
        $modal.css('width', o.width);
        $modal.find('.vmodal-modal-body').html(o.message);
        VModal._ShowModal($modal, o.onBeforeOpen, o.onOpen);
    },

    /*
     * Create a modal with your own html and whatever buttons you need
     * Takes an object containing 
     * the following parameters:
     * REQUIRED:
     *      title:          A title for the modal
     *      html:           Html content to display with the modal
     *      buttons:        An array of objects containting definitions for the buttons in the following format:
     *                      [   
     *                          {
     *                              html:   (html for the button text)
     *                              classes:(string of classes for the button)
     *                              click:  (click function for the button)
     *                                      ** Note to call for the modal to close call the function:
     *                                         VModal.close($modal)
     *                          }
     *                          example:
     *                          {
                                    classes: 'btn',
                                    html: 'Yes',
                                    click: function($modal) {
                                        alert("yes clicked");
                                        VModal.close($modal);
                                    }
                                }
     *                      ]
     * OPTIONAL:
     *      icon:           html icon element to display in the title bar (or any html)
     *      width:          defaults to none, allowing css to define the width
     *      hasExitButton:  Sets whether a header exit button is present in the title, defaults to true
     *      classes:        a string of any extra classes you want added to the modal
     *      onBeforeOpen:   function to call when the modal is rendered but not yet visible
     *      onOpen:         function to call when the modal is fully visible
     *      onClose:        optional callback on close
     */
    General: function(o) {
        o = $.extend({
            width: o.width ? o.width+'px' : '',
            hasExitButton: o.hasExitButton ? o.hasExitButton : true,
            classes: o.classes ? o.classes : ''
        },o);

        var $container = VModal._Init();
        var modal_id = 'vmodal-general-modal';

        VModal._ShowOverlay();
        var $modal = $('#'+modal_id);

        if($('#'+modal_id).length == 0) {
            // Create the html
            $modal = $('<div id="'+modal_id+'" class="vmodal-modal '+o.classes+'" />');

            $modal.append(VModal._GetHeader());
            $modal.append(VModal._GetBody());
            $modal.append(VModal._GetFooter());

            $container.append($modal);
        }
        else {
            $modal.find('.vmodal-modal-footer').empty();

            // Strip any extra classes that might have been added and add necessary ones
            $modal.removeClass();
            $modal.addClass('vmodal-modal '+o.classes);
        }

        // Add buttons
        if(o.buttons) {
	        $.each(o.buttons, function(a, b){
	            VModal._AddButton($modal, b);
	        });
        }

        $modal.find('.vmodal-modal-header').html(o.title);
        if(o.hasExitButton) {
            VModal._AddExitButton($modal);
        }
        if(o.icon) {
            VModal._AddTitleIcon($modal, o.icon);
        }
        if(o.onClose) {
            $modal.data('onClose', o.onClose);
        }
        $modal.css('width', o.width);
        $modal.find('.vmodal-modal-body').html(o.html);
        VModal._ShowModal($modal, o.onBeforeOpen, o.onOpen);
    },
    
    /*
     * Render a modal based on the Wordpress custom post
     */
    Modal: function(vmodal_id) {
        var data = $('#vmodal-'+vmodal_id).data();
        var o = { };
        
        $.each(data, function(i,j){
            if(j != '') {
                o[i] = j;
            }
        });

        var $container = VModal._Init();
        var modal_id = 'vmodal-modal-'+vmodal_id;

        VModal._ShowOverlay();
        var $modal = $('#'+modal_id);

        if($('#'+modal_id).length == 0) {
            // Create the html
            $modal = $('<div id="'+modal_id+'" class="vmodal-modal '+o.classes+'" />');

            $modal.append(VModal._GetBody());

            $container.append($modal);
        }
        else {
            // Strip any extra classes that might have been added and add necessary ones
            $modal.removeClass();
            $modal.addClass('vmodal-modal '+o.classes);
        }

        $modal.find('.vmodal-modal-body').html($('#vmodal-'+vmodal_id).html());
        if(o.hasexitbutton) {
            VModal._AddExitButton($modal);
        }
        
        if(o.onclose) {
            $modal.data('onClose', o.onclose);
        }
        if(o.width) {
            $modal.css('width', o.width);
        }
        
        if(o.fadeinspeed) {
            $modal.data('fadeinspeed', o.fadeinspeed);
        }
        if(o.fadeoutspeed) {
            $modal.data('fadeoutspeed', o.fadeoutspeed);
        }
        
        VModal._ShowModal($modal, o.onbeforeopen, o.onopen);
    },

    close: function(modal) {
        VModal._CloseModal($(modal));
    },
    
    QAlert: function(message) {
        VModal.Alert({
            message: message
        });
    }
};

