// Fix to define jquery if it is not
if(typeof($) == 'undefined') {
    var $ = jQuery;
}

// Encode a set of form elements as an array of names and values.
$.fn.serializeObject = function() {
	var o = {},
		a = this.serializeArray();

	$.each(a, function() {
		if (o[this.name] !== undefined) {
			if (!o[this.name].push) {
				o[this.name] = [o[this.name]];
			}
			o[this.name].push(this.value || '');
		} else {
			o[this.name] = this.value || '';
		}
	});

	return o;
};
