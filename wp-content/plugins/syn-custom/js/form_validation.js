/* 
 * General form stuff as well as site specific form 
 * js can go here
 */

if(typeof($) === 'undefined') {
    var $ = jQuery;
}

$(function(){
    /*
     * This is a hook to catch all form submissions
     */
    $('.wpcf7-form').submit(function(){
        var form_id = $(this).find('input[name="_wpcf7"]').val();
        var $form = $(this);
        
        //cf7_submit_accents($form);
        //addSelect2Wrappers();
    });
    
    // Catch wpcf7 errors 
    $('.wpcf7').on('invalid.wpcf7', function(){
        
    });
    
    addMasks();
});

/*
 * Accent function to make cf7 submits behave better
 */
function cf7_submit_accents($form) {
    /*
     * Add a ui blocker
     * NOTE: This replies on the VModals plugin and a loading gif
     */
    var html = '<div class="block-ui-container">';
    html += 'Please wait, validating form...';
    html += '<img src="/wp-content/themes/readingtruck/assets/images/loading.GIF" />';
    html += '</div>';
    VModal.Block.Show({
        html: html,
        classes: 'cf7-form-block-ui'
    });
    
    cf7_form_control($form, 0);
}

/*
 * Adds a cf7 wrapper to select2 fields so validation
 * errors can properly display
 */
function addSelect2Wrappers() {
    $('.select2-container').each(function(i,j){
        if($(j).siblings('.wpcf7-form-control-wrap').length == 0) {
            var $validation_wrapper = $('<span class="wpcf7-form-control-wrap"></span>');
            $validation_wrapper.addClass($(j).prop('class')).removeClass('select2-container');
            $(j).after($validation_wrapper);
        }
    });
}

/*
 * Adds input masks 
 * Requires the jquery.maskedinput.js plugin
 * Info @: http://digitalbush.com/projects/masked-input-plugin/
 */
function addMasks() {
    $('input[type="tel"]').mask("(999) 999-9999? x99999");
    $('input[name="zip"], input[name="distributor-zip"], input[name="customer-zip"], input[name="Zip"]').mask("99999?-9999");
}

