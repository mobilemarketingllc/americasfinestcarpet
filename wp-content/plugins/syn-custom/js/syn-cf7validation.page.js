(function($) {
	'use strict';

	var $modalEdit     = $('#syncf7-modal-edit'),
		$modalEditForm = $modalEdit.find('form'),
		$modelDel      = $('#syncf7-modal-del'),
		$modelDelForm  = $modelDel.find('form'),
		$addRuleButton = $('.syncf7-btn-add-rule');
        
    $addRuleButton.removeAttr('disabled');

	function formDisable($form, disable) {
		// disable/enable form fields based on disabled attribute.
        if(disable) {
            $form.find('input').prop('disabled', disable);
            $form.find('button').prop('disabled', disable);
        }
        else {
            $form.find('input').removeAttr('disabled');
            $form.find('button').removeAttr('disabled');
        }
	}

	function processModalForm($form) {
		var data = $form.serializeObject();
		data['action'] = 'syncf7';

		$('.syncf7-error').html('').hide();	// clear errors
		formDisable($form, true);			// disable form elements

		$.ajax({
			url: ajaxurl,
			type: 'POST',
			dataType: 'json',
			data: data,
			error: function(request, status, exception) {
				$('.syncf7-error').html('<div class="ui-state-error">Error processing request, please try again.</div>').slideDown();
			},
			success: function(data, status, request) {
				if (!data['error']) {
					$('.syncf7-success').html('<div class="ui-state-highlight">' + data['success'] + '</div>').slideDown();
					window.location.reload();
				} else {
					$('.syncf7-error').html('<div class="ui-state-error">' + data['error'] + '</div>').slideDown();
				}
			},
			complete: function(request, status) {
				formDisable($form, false);	// enable form elements
			}
		});
	}
    
    function validateEditModal($form) {
        var $error = $form.find('#error-message');
        
        var priority = $form.find('input[name=rank]').val();
        if(priority == '') {
            $error.html('Please enter a priority.');
            return false;
        }
        
        var field_name = $form.find('input[name=field_name]').val();
        if(field_name == '') {
            $error.html('Please enter at least one field name.');
            return false;
        }
        
        $error.html('');
        processModalForm($form);
        return true;
    }

	function clearEditForm() {
		$modalEditForm.find('.syncf7-action-copy').text('Create');
		$modalEditForm.find('input:not([type=hidden])').val('');
        $modalEditForm.find('input[name=id]').val(0);
		$modalEditForm.find('select').find('option:first').prop('selected', true);
        $('#syncf7-modal-edit-form').siblings('h3').html('Rule Data');
	}
    
    function populateEditModal(a, duplicate) {
        var $self         = $(a),
			id            = $self.data('id'),
			$parentTable  = $self.closest('table'),
			$nextEditForm = $parentTable.next('.syncf7-modal-edit'),
			qdata         = {
				'action':    'syncf7',
				'synaction': 'syncf7vget',
				'id':         id
			};

		$.ajax({
			url: ajaxurl,
			type: 'POST',
			dataType: 'json',
			data: qdata,
			error: function(request, status, exception) {
				$('.syncf7-error').html('<div class="ui-state-error">Error processing request, please try again.</div>').slideDown();
			},
			success: function(data, status, request) {
				if (!data['error']) {
					$('#syncf7-modal-edit-form input[name=id]').val(id);
					$('#syncf7-modal-edit-form select[name=blog_id]').val(data['blog_id']);
					$('#syncf7-modal-edit-form select[name=field_type]').val(data['field_type']);
					$('#syncf7-modal-edit-form input[name=field_name]').val(data['field_name']);
					$('#syncf7-modal-edit-form input[name=func]').val(data['func']);
					$('#syncf7-modal-edit-form input[name=regex]').val(data['regex']);
					$('#syncf7-modal-edit-form input[name=msg_err]').val(data['msg_err']);
					$('#syncf7-modal-edit-form input[name=rank]').val(data['rank']);
					$('#syncf7-modal-edit-form .syncf7-action-copy').html('Modify');

					formDisable($modalEdit, false);

					if ($nextEditForm.length === 0) {
						$addRuleButton.prop('disabled', true);
						$modalEdit.appendTo($parentTable.parent()).slideDown();
					}
                    if(duplicate) {
                        $modalEditForm.find('input[name=id]').val('');
                        $('#syncf7-modal-edit-form').siblings('h3').html('Rule Data (Duplicating)');
                        $modalEditForm.find('.syncf7-action-copy').text('Create');
                    }
                    else {
                        $('#syncf7-modal-edit-form').siblings('h3').html('Rule Data');
                    }
				} else {
					$('.syncf7-error').html('<div class="ui-state-error">'+data['error']+'</div>').slideDown();
				}
			}
		});
    }

	$modalEditForm.on('submit', function(event) {
		event.preventDefault();
		validateEditModal($modalEditForm);
	});

	$modelDelForm.on('submit', function(event) {
		event.preventDefault();
	});

	$addRuleButton.on('click', function(event) {
		clearEditForm();

		formDisable($modalEditForm, false);

		$modalEdit.slideDown('fast', function() {
			$addRuleButton.prop('disabled', true);
		});
	});

	$('.synCF7EditRule').on('click', function() {
        populateEditModal(this, false);
	});
    
    $('.synCF7DuplicateRule').on('click', function(){
        populateEditModal(this, true);
    });

	$('.syncf7-modal-close').on('click', function(event) {
		formDisable($modalEditForm, false);

		$modalEdit.slideUp('fast', function() {
			if (!$modalEdit.parent().is('.add-rule-container')) {
				$modalEdit.appendTo('.add-rule-container');
			}

			$addRuleButton.removeAttr('disabled');
		});
	});

	$('.synCF7DelRule').on('click', function(event) {
		var id = $(this).data('id');

		$('.syncf7-modal-del input[name=id]').val(id);
		$('.syncf7-id').html($(this).data('id'));
		$('.syncf7-blog_id').html($(this).data('blog_id'));
		$('.syncf7-field_type').html($(this).data('field_type'));
		$('.syncf7-field_name').html($(this).data('field_name'));

		formDisable($modelDel, false);

        openDeleteDialog();
	});
    
    function openDeleteDialog() {
        VModal.Confirm({
            title: 'Confirm Rule Delete',
            message: 'Are you sure you want to delete this rule?',
            onAccept: function() {
                processModalForm($modelDelForm, $modelDel);
            }
        });
    }

	$('.example-rule').on('click', function(event) {
		var $self = $(this),
			data  = $self.data('rule-data'),
			$input;

		if ($self.is('.function')) {
			$input = $("#func");
		} else {
			$input = $("#regex");
		}

		$input.val(data);
	});

	$('.rule-list-title').on('click', function(event) {
		var $self = $(this);

		$self.next('.example-rule-list').slideToggle('fast', function() {
			$self.toggleClass('active');
		});
	});
})(jQuery);
