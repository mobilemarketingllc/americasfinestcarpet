<h2>Custom Plug-in for WordPress</h2>
<div>
	<p>
		This is a custom build WordPress plug-in for your website.<br>
		Do not uninstall or delete this plugin or your website will not run properly.
	</p>
</div>
<div>
	<ul>
		<li><strong>Useful Tools:</strong></li>
		<li><a href="http://checkip.synapseresults.com/" target="_blank">Check External IP Address</a></li>
	</ul>
</div>
<?php
$missingExt = array();
if (!function_exists('curl_init')) {
	$missingExt[] = 'SynCustom needs the CURL PHP extension.';
}
if (!function_exists('json_decode')) {
	$missingExt[] = 'SynCustom needs the JSON PHP extension.';
}
if (0<count($missingExt)) {
?>
<div>
	<ul>
		<li><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span><strong>Missing Required PHP Extension(s):</strong></li>
<?php
	$o = null;
	foreach($missingExt as $missingExtErrMsg) {
		$o.= '<li class="ui-state-error">'.$missingExtErrMsg.'</li>';
	}
	echo $o;
?>
	</ul>
</div>
<?php
}
$CF7V = new CF7Validation();
if (!$CF7V->isCF7Active()) {
	echo '<h3>Contact Form 7 Status: <span class="danger">[NOT ACTIVE]</span> Plugin must be active.</h3>';
}
?>
