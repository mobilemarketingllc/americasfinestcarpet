<?php
/*
Plugin Name: Syn-Custom
Plugin URI: http://www.synapseresults.com
Description: Synapse Marketing Solutions plugin for customized builds.
Version: 2.0
Author: Synapse Marketing Solutions
Author URI: http://www.synapseresults.com
License: Synapse Marketing Solutions
Text Domain: syn-custom
DomainPath: /languages/
*/
/**
 * Synapse Custom WordPress Plugin
 *
 * @category   WP Plugin
 * @package    Synapse WP Plugin
 * @license    Synapse Marketing Solutions
 * @author     Synapse Back End Development Team
 * @copyright  Copyright (C) 2014 Synapse Marketing Solutions
 * @version    2.0
**/

ini_set('display_errors',1);
error_reporting(E_ALL);

# Include WP Libraries
require_once constant('ABSPATH').'wp-admin/includes/plugin.php';

# Include Plugin Libraries
require_once dirname(__FILE__).'/lib/literals.php';			// Defined Literals & Class Autoloader
require_once constant('PATH_LIB').'/general.func.php';
require_once constant('PATH_LIB').'/SynUtility.class.php';

if (!function_exists('curl_init')) {
	throw new Exception('SynCustom needs the CURL PHP extension.');
}
if (!function_exists('json_decode')) {
	throw new Exception('SynCustom needs the JSON PHP extension.');
}

// Set up a general utility class
$SynUtility = new SynUtility();

# Instantiate Contact Form 7 Validation - Hook Validation Filters
$CF7V = new CF7Validation();

// Yoast/Woocommerce helper
$WooYoast = new WooYoast();

# Instantiate Synapse Custom Plugin
$SC = SynCustom::init();

# Run download process if found.
if (is_admin()) {
	$SCA = new SynCustomAdmin();
	$SCA->download_excel();
}
require_once constant('PATH_LIB').'/db_log/db_log.php';

# Instantiate Contact Form 7 Validation - Hook Validation Filters
$CF7Override = new CF7Override();
