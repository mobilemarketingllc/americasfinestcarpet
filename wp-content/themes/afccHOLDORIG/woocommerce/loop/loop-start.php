<?php
/**
 * Product Loop Start
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */
global $post;
if( is_tax('product_cat', 'window-treatments') ){
	if( !empty($post) ) {
		$args = array( 'taxonomy' => 'product_cat',);
		$terms = wp_get_post_terms($post->ID,'product_cat', $args);
		if( !empty($terms) ) {
			$term = array_pop($terms);
		}
		if( !empty($term) ) {
			$secHead = get_field('secondary_headline', 'product_cat_'.$term->term_id);
			$secSubHead = get_field('secondary_sub_headline', 'product_cat_'.$term->term_id);
		}
	}
	echo '
		<div class="row">
			<div class="twelve columns center">
				<h2 class="h1-style">'. $secHead .'</h2>
				<p class="small">'. $secSubHead .'</p>
			</div>
		</div>
	';
}
?>
<ul class="products">
