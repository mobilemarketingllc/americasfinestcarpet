<?php
/**
 * The template for displaying product content within loops.
 *
 * Override this template by copying it to yourtheme/woocommerce/content-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product, $woocommerce_loop, $post;

// Store loop count we're currently on
if ( empty( $woocommerce_loop['loop'] ) )
	$woocommerce_loop['loop'] = 0;

// Store column count for displaying the grid
if ( empty( $woocommerce_loop['columns'] ) )
	$woocommerce_loop['columns'] = apply_filters( 'loop_shop_columns', 4 );

// Ensure visibility
if ( ! $product || ! $product->is_visible() )
	return;

// Increase loop count
$woocommerce_loop['loop']++;

// Extra post classes
$classes = array();
if ( 0 == ( $woocommerce_loop['loop'] - 1 ) % $woocommerce_loop['columns'] || 1 == $woocommerce_loop['columns'] )
	$classes[] = 'first';
if ( 0 == $woocommerce_loop['loop'] % $woocommerce_loop['columns'] )
	$classes[] = 'last';
$classes[] = 'center';
$classes[] = 'product';
$classes[] = 'wt';
$classes[] = 'remove-padd';
$attrs = array('title', 'brand');


?>
<li <?php post_class( $classes ); ?>>

	<?php do_action( 'woocommerce_before_shop_loop_item' ); ?>
		<a href="#" class="center no-link">
			<?php
				/**
				 * woocommerce_before_shop_loop_item_title hook
				 *
				 * @hooked woocommerce_show_product_loop_sale_flash - 10
				 * @hooked woocommerce_template_loop_product_thumbnail - 10
				 */
				do_action( 'woocommerce_before_shop_loop_item_title' );
			?>
			<div class="contain-white-sub">
				<?php
					foreach ($attrs as $attr) {
						if( $attr === 'title' ) {
							 echo '<h3 class="nm">'.get_the_title().'</h3>';
						} else {
							$terms = get_the_terms( $post->ID, 'pa_'.$attr );
						    if( !empty($terms) ) {
								foreach ($terms as $term) {
								    echo '<p class="'. $attr .'-space small nm">' . $term->name . '</p>';
								    break;
								}
							}
						}

					}
				?>
			</div>
		</a>
	<?php

		/**
		 * woocommerce_after_shop_loop_item hook
		 *
		 * @hooked woocommerce_template_loop_add_to_cart - 10
		 */
		//do_action( 'woocommerce_after_shop_loop_item' );

	?>
	<div class="contain-sub-links">
		<a href="<?php echo get_permalink_with_requested_product( 1429, $product->id ); ?>" class="center no-abs cta"><?php _e( 'Free Measurement', 'afcc' ) ?></a>
		<div class="clear"></div>
		<a class="fl red-link" href="<?php echo get_permalink(68); ?>"><?php _e( 'Make an Appointment', 'afcc' ) ?></a>
		<a class="fr red-link req-measurement" href="<?php echo get_permalink_with_requested_product( 1060, $product->id ); ?>"><?php _e( 'Request Info', 'afcc' ) ?></a>
		<div class="clear"></div>
	</div>
</li>
