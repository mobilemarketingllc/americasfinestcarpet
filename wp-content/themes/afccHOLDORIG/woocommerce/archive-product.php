<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive.
 *
 * Override this template by copying it to yourtheme/woocommerce/archive-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */
global $wp_query, $post;
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

// functions.php
function my_product_count() {
	$cat = get_queried_object()->term_id;
   	$catid = get_term($cat, 'product_cat' );
    $product_count = 0;

    // Get count for all product categories
    foreach (get_terms($cat, 'product_cat') as $term)
        $product_count = $term->count;

    return $product_count;
}

$bgImg = getPageHeaderBg();
if(is_numeric($bgImg)) {
    $bgImg = wp_get_attachment_url($bgImg);
}


$page_id = 4;
$page_object = get_page( $page_id );
get_header( 'shop' ); ?>

<section data-stellar-vertical-offset="700" data-stellar-background-ratio="0.5" class="parallax all-prods-top center" style="background-image: url(<?php echo $bgImg; ?>);">
		<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
				<?php if( !is_shop() ) : ?>
				<h1 class="page-title h1-style">Online <?php woocommerce_page_title(); ?> Catalog</h1>
				<p class="nm small center">
					<?php
						$per_page = $wp_query->get( 'posts_per_page' );
						$total    = $wp_query->found_posts;
						if ( 1 == $total ) {
							_e( 'Viewing the single result', 'woocommerce' );
						} else {
							printf( __( 'Viewing %d Products', 'woocommerce' ), $total );
						}
					?>
				</p>
				<?php else: ?>
				<h1 class="headline nm h1-style"><?php woocommerce_page_title(); ?></h1>
				<?php if ( $page_object instanceof WP_Post ) : ?>
				<p class="nm large"><?php echo $page_object->post_content; ?></p>
				<?php endif; ?>
				
				<?php endif; ?>
		<?php endif; ?>

		<?php //do_action( 'woocommerce_archive_description' ); ?>
		<div class="row">
			<div class="twelve columns">
				<?php if( !is_shop() ) :?>
				<hr />
				<?php endif; ?>
				<?php
					/**
					 * woocommerce_before_shop_loop hook
					 *
					 * @hooked woocommerce_result_count - 20
					 * @hooked woocommerce_catalog_ordering - 30
					 */
					do_action( 'woocommerce_before_shop_loop' );
				?>
			</div>
		</div>
</section>
<section id="breadcrumbs" <?php if( is_product() ) { echo 'class="top-section"'; }  ?>>
	<div class="row">
		<div class="twelve columns">
			<?php
				/**
				 * woocommerce_before_main_content hook
				 *
				 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
				 * @hooked woocommerce_breadcrumb - 20
				 */
				do_action( 'woocommerce_before_main_content' );
			?>
		</div>
	</div>	
</section>
<section class="all-products-wrapper">
	<div class="row">
		<div class="twelve columns">
			<?php if ( have_posts() ) : ?>

			

				<?php woocommerce_product_loop_start(); ?>

					<?php 
						if( !is_shop() ) :
							woocommerce_product_subcategories();
						else :
							if ( get_option( 'woocommerce_shop_page_display' ) == 'subcategories' ) {
								$wp_query->post_count    = 0;
								$wp_query->max_num_pages = 0;
							}
						endif;
					?>

					<?php while ( have_posts() ) : the_post(); ?>

						<?php wc_get_template_part( 'content', 'product' ); ?>

					<?php endwhile; // end of the loop. ?>

				<?php woocommerce_product_loop_end(); ?>

				<?php
					/**
					 * woocommerce_after_shop_loop hook
					 *
					 * @hooked woocommerce_pagination - 10
					 */
					do_action( 'woocommerce_after_shop_loop' );
				?>

			<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

				<?php wc_get_template( 'loop/no-products-found.php' ); ?>

			<?php endif;	
				if ( is_shop()
				&& get_option( 'woocommerce_shop_page_display' ) == 'subcategories' ) {
					wc_get_template('content-shop-landing.php', true);
				}
			?>
		</div>
	</div>
</section>

	<?php
		/**
		 * woocommerce_after_main_content hook
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
		do_action( 'woocommerce_after_main_content' );
	?>

	<?php
		/**
		 * woocommerce_sidebar hook
		 *
		 * @hooked woocommerce_get_sidebar - 10
		 */
		do_action( 'woocommerce_sidebar' );
	?>

<?php get_footer( 'shop' ); ?>
