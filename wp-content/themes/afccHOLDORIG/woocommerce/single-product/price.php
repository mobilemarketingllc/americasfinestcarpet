<?php
/**
 * Single Product Price, including microdata for SEO
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

$product_categories = get_the_terms($product->id, 'product_cat');
foreach($product_categories as $pcat) {
    $curCat = $pcat;
    break;
}
if( is_object($curCat) ) {	
	$displayPrice = get_field('show_pricing_on_cat', 'product_cat_'.$curCat->term_id);
} else {
	$displayPrice = 'no';
}
?>
<div itemprop="offers" itemscope itemtype="http://schema.org/Offer">
    <?php if($product->price != 0.0 && $product->is_in_stock() && $displayPrice == 'yes') { ?>
	<p class="price center nm"><span><?php echo $product->get_price_html(); ?></span> sq/ft</p>
    <?php } ?>
	<meta itemprop="price" content="<?php echo $product->get_price(); ?>" />
	<meta itemprop="priceCurrency" content="<?php echo get_woocommerce_currency(); ?>" />
	<link itemprop="availability" href="http://schema.org/<?php echo $product->is_in_stock() ? 'InStock' : 'OutOfStock'; ?>" />

</div>
