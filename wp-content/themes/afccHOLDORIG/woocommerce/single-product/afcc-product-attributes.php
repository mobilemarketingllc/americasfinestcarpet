<?php 
	global $post, $woocommerce, $product;
	$product_categories = get_the_terms($product->id, 'product_cat');
	foreach($product_categories as $pcat) {
	    $curCat = $pcat;
	    break;
	}
	if( is_object($curCat) ) {	
		$displayPrice = get_field('show_pricing_on_cat', 'product_cat_'.$curCat->term_id);
	} else {
		$displayPrice = 'no';
	}
?>
<div id="contain-attrs" <?php if( $displayPrice == 'no' || !$product->is_in_stock() ) : ?>class="no-price-option"<?php endif; ?>>
	<div class="center">
		<?php
			$attrs = array('tone', 'color', 'title', 'brand');
			foreach ($attrs as $attr) {
				if( $attr === 'title' ) {
					 echo '<h3 class="nm title-space">'.get_the_title().'</h3>';
				} else {
					$terms = get_the_terms( $post->ID, 'pa_'.$attr );
				    if( !empty($terms) ) {
						foreach ($terms as $term) {
						    echo '<p class="'. $attr .'-space small nm">' . $term->name . '</p>';
						    break;
						}
					}
				}
				
			}
		?>
	</div>
	<?php if($product->price != 0.0 && $product->is_in_stock() && $displayPrice == 'yes') : ?>
	<hr class="small less-mar">
	<?php endif; ?>

