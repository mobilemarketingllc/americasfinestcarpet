<?php
	global $post, $product;
	$terms = get_the_terms( $post->ID, 'product_cat' );
    $nterms = get_the_terms( $post->ID, 'product_tag'  );
    foreach ($terms  as $term  ) {
        $product_cat_id = $term->term_id;
        $product_cat_name = $term->name;
        break;
    }
	if( is_object($term) ) {	
		$displayPrice = get_field('show_pricing_on_cat', 'product_cat_'.$term->term_id);
	} else {
		$displayPrice = 'no';
	}

?>
<?php if( $displayPrice === 'yes' && $product->is_in_stock() ): ?>
	<p class="small installed center nm"><?php _e('Installed')?></p>
<?php endif; ?>
</div>
<div class="contain-prod-ctas">
	<a href="<?php echo get_permalink(1058); ?>" class="cta special">Get Coupon</a>
	<a class="prod-info-link first req-appointment-link" href="<?php echo get_permalink(68); ?>">Request Appointment</a>
    <a class="prod-info-link req-measurement-link" href="<?php echo get_permalink_with_requested_product( 1060, $post->ID ); ?>" class="center req-info"><?php _e( 'Request Info', 'afcc' ) ?></a>
	<a class="prod-info-link req-design-consult" href="<?php echo get_permalink(69); ?>">Design Consultation</a>
</div>
