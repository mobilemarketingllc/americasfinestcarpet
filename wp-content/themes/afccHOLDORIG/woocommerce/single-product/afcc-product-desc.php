<?php
/**
 * Single Product Thumbnails
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post, $product, $woocommerce;
$secHead = get_field('secondary_headline');
$secSubHead = get_field('secondary_sub_headline');

if( $secHead ){

?>
	<h3 class="h1-style center"><?php echo $secHead; ?></h3>
	<p class="small center"><?php echo $secSubHead; ?></p>
	<hr class="small">
<?php
	}
?>
	<div class="lh-lots"><?php echo $post->post_content; ?></div>
