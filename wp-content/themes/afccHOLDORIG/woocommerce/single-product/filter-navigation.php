<?php
/******************************************************************************
 *              ___   _   _   _ __     __ _   _ __    ___    ___              *
 *             / __| | | | | | '_ \   / _` | | '_ \  / __|  / _ \             *
 *             \__ \ | |_| | | | | | | (_| | | |_) | \__ \ |  __/             *
 *             |___/  \__, | |_| |_|  \__,_| | .__/  |___/  \___|             *
 *                     __/ |                 | |                              *
 *                    |___/                  |_|                              *
 *                                                                            *
 *                    m a r k e t i n g  s o l u t i o n s                    *
 ******************************************************************************/

/**
 * Product Filter Navigation
 *
 * @author     Joseph Leedy <jleedy@synapseresults.com>
 * @category   AFCC
 * @package    AFCC_Theme
 * @copyright  Copyright 2015 Synapse Marketing Solutions (http://synapseresults.com/)
 * @license    Proprietary
 */

global $wp_query;

if ( ! defined( 'ABSPATH' ) ) :
	exit; // Bail if accessed directly
endif;

$sibling_products = get_product_siblings_in_filter();
$prev_product_url = '';
$next_product_url = '';

if ( ! empty( $sibling_products ) ) :
	$filters = get_query_var( 'product_filter' );

	if ( isset( $sibling_products['previous'] ) && is_object( $sibling_products['previous'] ) ) :
		$prev_product_url = user_trailingslashit( get_permalink( $sibling_products['previous']->ID ) . 'filter/' . $filters );
	endif;

	if ( isset( $sibling_products['next'] ) && is_object( $sibling_products['next'] ) ) :
		$next_product_url = user_trailingslashit( get_permalink( $sibling_products['next']->ID ) . 'filter/' . $filters );
	endif;
endif;
?>
<div class="twelve columns">
	<div id="product-filter-navigation-container">
		<?php if ( ! empty( $prev_product_url ) ) : ?>
		<div id="product-filter-navigation-left" class="product-filter-navigation fl">
			<a href="<?php echo esc_url( $prev_product_url ); ?>" class="previous-product prod-filter-navi">
				<span class="icon icon-thin-arrow-left" title="<?php _e( 'Previous Filtered Product', 'afcc' ) ?>" aria-hidden="true"></span>
				<span class="icon-label screen-reader-text"><?php _e( 'Previous Filtered Product', 'afcc' ) ?></span>
			</a>
		</div>
		<?php endif; ?>

		<?php wc_get_template( 'single-product/title.php' ) ?>

		<?php if ( ! empty( $next_product_url ) ) : ?>
		<div id="product-filter-navigation-right" class="product-filter-navigation fr">
			<a href="<?php echo esc_url( $next_product_url ); ?>" class="next-product prod-filter-navi">
				<span class="icon icon-thin-arrow-right" title="<?php _e( 'Next Filtered Product', 'afcc' ) ?>" aria-hidden="true"></span>
				<span class="icon-label screen-reader-text"><?php _e( 'Next Filtered Product', 'afcc' ) ?></span>
			</a>
		</div>
		<?php endif; ?>
	</div>
</div>
