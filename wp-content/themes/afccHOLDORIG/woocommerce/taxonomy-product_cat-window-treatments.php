<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive.
 *
 * Override this template by copying it to yourtheme/woocommerce/archive-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */
global $wp_query, $post;
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

// functions.php
function my_product_count() {
	$cat = get_queried_object()->term_id;
   	$catid = get_term($cat, 'product_cat' );
    $product_count = 0;

    // Get count for all product categories
    foreach (get_terms($cat, 'product_cat') as $term)
        $product_count = $term->count;

    return $product_count;
}

get_header( 'shop' );

$bgImg = getPageHeaderBg();

?>

<section data-stellar-vertical-offset="700" data-stellar-background-ratio="0.5" class="parallax all-prods-top center" style="background-image: url(<?php echo $bgImg; ?>);">
		<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
				<h1 class="page-title h1-style"><?php woocommerce_page_title(); ?></h1>
				<p class="nm small center">Product Options</p>
		<?php endif; ?>

		<?php //do_action( 'woocommerce_archive_description' ); ?>
		<div class="row">
			<div class="twelve columns">
				<?php
					/**
					 * woocommerce_before_shop_loop hook
					 *
					 * @hooked woocommerce_result_count - 20
					 * @hooked woocommerce_catalog_ordering - 30
					 */
					do_action( 'woocommerce_before_shop_loop' );
				?>
			</div>
		</div>
</section>
<section id="breadcrumbs">
	<div class="row">
		<div class="twelve columns">
			<?php
				/**
				 * woocommerce_before_main_content hook
				 *
				 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
				 * @hooked woocommerce_breadcrumb - 20
				 */
				do_action( 'woocommerce_before_main_content' );
			?>
		</div>
	</div>
</section>

<section class="all-products-wrapper">
	<div class="row">
		<div class="twelve columns">
			
			<?php /* if ( have_posts() ) : ?>



				<?php woocommerce_product_loop_start(); ?>

					<?php woocommerce_product_subcategories(); ?>

					<?php while ( have_posts() ) : the_post(); ?>

					<?php wc_get_template_part( 'content', 'product-window-treatments' ); ?>

					<?php endwhile; // end of the loop. ?>

				<?php woocommerce_product_loop_end(); ?>

				<?php
					/**
					 * woocommerce_after_shop_loop hook
					 *
					 * @hooked woocommerce_pagination - 10
					 *//*
					do_action( 'woocommerce_after_shop_loop' );
				?>

			<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

				<?php wc_get_template( 'loop/no-products-found.php' ); ?>

			<?php endif; */ ?>
			<?php
				$category = get_active_product_category();
				$args = array(
					'hierarchical' => 1,
					'show_option_none' => '',
					'hide_empty' => 0,
					'parent' => $category->term_id,
					'taxonomy' => 'product_cat'
				);

				$subcats = get_categories($args);
				if( count( $subcats ) > 0 ) :
					woocommerce_product_loop_start();
					foreach ($subcats as $category) :
						wc_get_template( 'content-product-window-treatments.php', array(
				          'category' => $category
				        ));
					endforeach;
					woocommerce_product_loop_end();
				else :
					wc_get_template( 'loop/no-products-found.php' );
				endif;
			?>
		</div>
	</div>
</section>

	<?php
		/**
		 * woocommerce_after_main_content hook
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
		do_action( 'woocommerce_after_main_content' );
	?>

	<?php
		/**
		 * woocommerce_sidebar hook
		 *
		 * @hooked woocommerce_get_sidebar - 10
		 */
		do_action( 'woocommerce_sidebar' );
	?>

<?php get_footer( 'shop' ); ?>
