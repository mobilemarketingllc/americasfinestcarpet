<?php
	$args = array(
	'taxonomy'     => 'product_cat',
	'orderby'      => 'name',
	'show_count'   => 0,
	'pad_counts'   => 0,
	'hierarchical' => 1,
	'title_li'     => '',
	'hide_empty'   => 0,
	'parent'       => 0
	);
	
	$all_categories = get_categories( $args );
	$i = 0;
	foreach ($all_categories as $cat) {
		$i++;
	    if($cat->category_parent == 0) {
	        $category_id = $cat->term_id;
		    $thumbnail_id = get_woocommerce_term_meta( $cat->term_id, 'thumbnail_id', true ); 
		    $image = wp_get_attachment_url( $thumbnail_id ); 
				if ( $i % 2 != 0 ) {
					$pad = ' npr';
				} else {
					$pad = '';
				}
				if ( $i % 4 == 0 || $i % 4 == 1 ) {
					$colW = 'five';
					$mar = 'msnry';
				}
				else {
					$colW = 'seven npr-mobile';
					$mar = '';
				}
?>
				<div class="<?php echo $colW.$pad; ?> columns">
					<div class="grid-bg tall full-size <?php echo $mar; ?>" style="background: url(<?php echo $image; ?>) no-repeat">
						<a href="<?php  echo get_term_link($cat->slug, 'product_cat'); ?>" class="contain-white short">
							Explore <?php echo $cat->name; ?>
							<span class="small"><?php echo $cat->description; ?></span>
						</a>
					</div>
				</div>
<?php
			if( $i % 2 == 0 ){
				echo '<div class="clear added"></div>';
			}
		}
	}
?> 
