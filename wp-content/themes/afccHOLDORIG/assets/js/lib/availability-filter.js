(function( $, _ ) {
	function find_index_by_match( needle, haystack ) {
		var regex, i;

		regex = new RegExp( '^' + needle );

		for ( i = 0; i < haystack.length; i++ ) {
			if ( haystack[ i ].match( regex ) ) {
				return i;
			}
		}

		return -1;
	}

	function query_string_to_array( query_string ) {
		if ( query_string.indexOf( '&' ) ) {
			query_string = query_string.split( '&' );
		} else {
			query_string = [ query_string ];
		}

		return query_string;
	}

	function add_query_var( url, new_query_var ) {
		var qs_position  = -1,
			query_args   = [],
			query_vars   = [];

		if ( ! _.isArray( new_query_var ) ) {
			new_query_var = query_string_to_array( new_query_var );
		}

		qs_position = url.indexOf( '?' );

		if ( qs_position !== -1 ) {
			var query_string = url.substr( qs_position + 1 );
			url = url.substr( 0, qs_position );

			query_args = query_string_to_array( query_string );
		}

		query_args = query_args.concat( new_query_var );
		query_args = _.uniq( query_args );

		_.each( query_args, function( item, index ) {
			var key, key_index;

			if ( item.indexOf( '=' ) !== -1 ) {
				var query_parts = item.split( '=' );

				key = query_parts[0];
			} else {
				key = item;
			}

			var key_index = find_index_by_match( key, query_vars );

			if ( key_index !== -1 ) {
				query_vars[ key_index ] = item;
			} else {
				query_vars.push( item );
			}
		} );

		if ( query_vars.length > 0 ) {
			url += ( '?' + query_vars.join( '&' ) );
		}

		return url;
	}

	var query_vars = [];
	query_vars.push( 'post_type=product' );

	if ( 'OnOff' in jQuery ) {
		$( '#myonoffswitch' ).onoff();
		$( 'input.onoffswitch-checkbox' ).on( 'change', function() {
			var $self    = $( this ),
				selected = $self.prop( 'checked' );

			if ( selected ) {
				// in stock
				query_vars.push( 'instock_products=in' );
				$self.addClass( 'in-active' );
			} else {
				// next day
				query_vars.push( 'instock_products=out' );
			}

			window.location.href = add_query_var( window.location.href, query_vars );
		});
	}
})( window.jQuery, window._ );
