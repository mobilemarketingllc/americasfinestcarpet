var AFCC_Map_Data = AFCC_Map_Data || [{}];
(function($) {
	var date = new Date();
	var hours = ['9:00am - 6:00pm','9:00am - 6:00pm','9:00am - 6:00pm','9:00am - 6:00pm', '9:00am - 6:00pm','9:00am - 6:00pm','12:00pm - 5:00pm'];
	var defaults = {
		"location" : {
			"address" : "730 Design Court, Chula Vista, CA, United States",
			"lat" : "32.5954043", 
			"lng" : "-117.0224641" 
		}, 
		"locationName" : "America's Finest Carpet Company",
		"locationNum"  : "(619) 427-2420",
		"currentDay" : hours[date.getDay()]
	};

	if( !'google' in window ) {
		return;
	}

	function setupMap() {
		var mapOptions = {
			scrollwheel: false
		};
		var bounds = new google.maps.LatLngBounds();
		var iconBase = 'http://'+window.location.host+'/wp-content/themes/afcc/assets/img/backgrounds/';
		var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
		var i;
		var infoWindow = new google.maps.InfoWindow();
		var infoWindowContent = [];
		var locCount = AFCC_Map_Data.length;
		var zoomLvl = locCount === 1 ? 17 : 8;
		var alpha = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
		for(i = 0; i < locCount; i++) {
			var markerInfo = $.extend(true, defaults, AFCC_Map_Data[i]);
			var contentString =
				'<div class="main-content-wrap">'+
					'<div id="infobox-content" class="center">'+
						'<h6 id="firstHeading" class="firstHeading">'+markerInfo.locationName+'</h6>'+
						'<p class="nm">'+markerInfo.location.address+'</p>'+
						'<p class="nm phone-num">'+markerInfo.locationNum+'</p>'+
						'<hr class="small">'+
						'<div id="bodyContent">'+
							'<p class="h-title caps nm">Todays Hours</p>' +
							'<p class="nm">'+markerInfo.currentDay+'</p>'+
	   					'</div>'+
	   				'</div>';
			if( 'links' in markerInfo ){
				contentString += '<ul class="block-grid two-up map-links">'+
				'<li><a href="'+markerInfo.links.permalink+'" class="db fl caps">View Location</a></li>'+
				'<li><a href="'+markerInfo.links.appointment+'" class="db fl caps">Make Appointment</a></li>'+
				'<li><a href="'+markerInfo.links.google_directions+'" class="db fl caps" target="_blank">Get Direction</a></li>'+
				'<li><a href="'+markerInfo.links.estimate+'" class="db fl caps">In-Home Estimate</a></li>'+
				'</ul>'+
				'<a href="'+markerInfo.links.coupon+'" class="cta">Get Coupon</a>';
			}
			contentString += '</div>'
			;
			var myLatlng = new google.maps.LatLng(markerInfo.location.lat,markerInfo.location.lng);
			if( alpha[i] == undefined ) {
				labelLoc = i - alpha.length + 1;
			} else { 
				labelLoc = alpha[i];
			}
			var marker = new MarkerWithLabel({
				position: myLatlng,
				map: map,
				labelContent: labelLoc,
				labelAnchor: new google.maps.Point(5, 83),
				labelClass: "marker-label",
				labelInBackground: true,
				icon: iconBase + 'pin.png'
			});
			infoWindowContent.push(contentString);
			bounds.extend(marker.position);

			google.maps.event.addListener(marker, 'click', (function(marker, i) {
				return function() {
					infoWindow.setContent(infoWindowContent[i]);
					infoWindow.open(map, marker);
				}
			})(marker, i));

		}
		map.fitBounds(bounds);
		var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
			this.setZoom(zoomLvl);
			google.maps.event.removeListener(boundsListener);
		});
	}

	google.maps.event.addDomListener(window, 'load', setupMap);
})(jQuery);
