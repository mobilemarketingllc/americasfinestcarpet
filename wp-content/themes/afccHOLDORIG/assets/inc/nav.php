<div class="phone-mobile">
    <a href="tel:8884022322">(888) 402-2322</a>
</div>

<header id="main-navigation" class="abs">
    <div class="row">
        <div class="twelve columns">
            <a href="/"><img src="<?php bloginfo( 'template_directory' ); ?>/assets/img/logos/logo.png"
                             alt="America's Finest Carpet Company" class="fl logo" width="272" height="97"/></a>
            <a href="#primary_nav_wrap_mobile" id="ham"></a>


            <?php /* Displays a navigation menu */
            $args = array(
                'menu' => 'main',
                'container' => 'nav',
                'container_id' => 'primary_nav_wrap',
                'container_class' => '',
                'menu_class' => 'main-nav fr',
                'menu_id' => '',
                'echo' => true,
                'fallback_cb' => 'wp_page_menu',
                'before' => '',
                'after' => '',
                'link_before' => '',
                'link_after' => '',
                'items_wrap' => '<ul class = "%2$s">%3$s</ul>',
                'depth' => 0,
                'walker' => ''
            );

            wp_nav_menu( $args );
            ?>
        </div>
    </div>
</header>

