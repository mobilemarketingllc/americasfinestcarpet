<section id="top-bar">
	<div class="row">
		<div class="twelve columns">
			<ul class="social-icons nm">
				<?php if( have_rows('social_icons', 'option') ):
					// loop through the rows of data
				    while ( have_rows('social_icons', 'option') ) : the_row();
				        $sTitle  = get_sub_field('social_platform_name');
				        $sLink  = get_sub_field('social_platform_link');
				?>
					<li class="<?php echo $sTitle; ?>"><a href="<?php echo $sLink; ?>" target="_blank"><span class="icon-<?php echo $sTitle; ?>"></span></a></li>
				<?php
					endwhile;
					endif;
				?>
			</ul>
            <div class="find-location fr">
				| &nbsp;<span style="color:#DE4232; font-size:0.813em;">(888) 402-2322</span>
			</div>

            <div class="find-location fr">
				<a href="<?php echo get_permalink(71); ?>"><span class="icon-pin"></span>Find a Location</a>&nbsp;&nbsp;
			</div>

		</div>
	</div>
</section>
