<?php
/******************************************************************************
 *              ___   _   _   _ __     __ _   _ __    ___    ___              *
 *             / __| | | | | | '_ \   / _` | | '_ \  / __|  / _ \             *
 *             \__ \ | |_| | | | | | | (_| | | |_) | \__ \ |  __/             *
 *             |___/  \__, | |_| |_|  \__,_| | .__/  |___/  \___|             *
 *                     __/ |                 | |                              *
 *                    |___/                  |_|                              *
 *                                                                            *
 *                    m a r k e t i n g  s o l u t i o n s                    *
 ******************************************************************************/

/**
 * Custom Breadcrumb Navigation Based On Yoast WP SEO
 *
 * @author     Joseph Leedy <jleedy@synapseresults.com>
 * @category   AFCC
 * @package    AFCC_Theme
 * @copyright  Copyright 2015 Synapse Marketing Solutions (http://synapseresults.com/)
 * @license    Proprietary
 */

/**
 * Look up a breadcrumb by a specific key and optional value
 *
 * @param 	string	$key        	The key to match
 * @param 	array 	$breadcrumbs	Where to look for the key
 * @param 	string	$value      	Item to match in found breadcrumb (optional)
 * @return	mixed	Matching breadcrumb, or false if key/value is not found
 */
function find_breadcrumb_by_key( $key, $breadcrumbs, $value = '' ) {
	$match = false;

	foreach ( $breadcrumbs as $breadcrumb ) {
		if ( ! is_array( $breadcrumb ) ) {
			continue;
		}

		if ( array_key_exists( $key, $breadcrumb ) && ( '' === $value || $breadcrumb[ $key ] == $value ) ) {
			$match = $breadcrumb[ $key ];
			break;
		}
	}

	return $match;
}

/**
 * Create breadcrumbs for product filters
 *
 * @param 	array	$filters
 * @param 	array	$breadcrumbs
 * @param 	array	$additional_filters
 * @return	array
 */
function afcc_build_filter_breadcrumbs( array $filters, array $breadcrumbs, array $additional_filters = array() ) {
	$product_taxonomies = get_object_taxonomies( 'product', 'object' );
	$exclude_taxonomies = array( 'product_cat', 'product_tag', 'characteristics', 'product_type' );
	$filter_taxonomies  = array();
	$active_taxonomies  = array();
	$active_filters     = array();
	$have_additional    = false;

	if ( count( $additional_filters ) > 0 ) {
		$have_additional = true;
	}

	if ( ! empty( $filters ) ) {
		foreach ( $filters as $key => $value ) {
			if ( ! in_array( $key, $exclude_taxonomies ) && ! empty( $value ) ) {
				if ( array_key_exists( $key, $product_taxonomies ) ) {
					$filter_taxonomies[ $key ] = $value;
					$active_taxonomies[ $key ] = $product_taxonomies[ $key ];
				} else {
					if ( ! $have_additional ) {
						$additional_filters[ $key ] = $value;
					}
				}
			}
		}
	}

	if ( count( $filter_taxonomies ) > 0 && count( $active_taxonomies ) > 0 ) {
		foreach ( $filter_taxonomies as $key => $values ) {
			if ( ! is_array( $values ) ) {
				$values = array( $values );
			}

			$term_names = array();

			foreach ( $values as $value ) {
				$term = get_term_by( 'slug', $value, $key );

				if ( $term !== false ) {
					$term_names[] = $term->name;
				}
			}

			if ( count( $term_names ) > 0 ) {
				$active_filters[] = array(
					'name'  => $active_taxonomies[ $key ]->label,
					'value' => implode( ', ', $term_names ),
				);
			}
		}
	}

	if ( ! empty( $additional_filters[ 'instock_products' ] ) ) {
		$availability_filter = array(
			'name'  => __( 'Availability', 'afcc' ),
			'value' => '',
		);

		if ( 'in' === $additional_filters[ 'instock_products' ] ) {
			$availability_filter['value'] = __( 'In Stock', 'afcc' );
		} else {
			$availability_filter['value'] = __( 'Special Order', 'afcc' );
		}

		$active_filters[] = $availability_filter;
	}

	$price_filters = array();

	if ( ! empty( $additional_filters['min_price'] ) ) {
		$price_filters[] = sprintf( '$%.2f', $additional_filters['min_price'] );
	}

	if ( ! empty( $additional_filters['max_price'] ) ) {
		$price_filters[] = sprintf( '$%.2f', $additional_filters['max_price'] );
	}

	if ( count( $price_filters ) > 0 ) {
		$active_filters[] = array(
			'name'  => __( 'Price', 'afcc' ),
			'value' => implode( ' - ', $price_filters ),
		);
	}

	if ( count( $active_filters ) > 0 ) {
		$filter_breadcrumbs = array();

		foreach ( $active_filters as $filter ) {
			$filter_breadcrumbs[] = $filter['name'] . ': ' . $filter['value'];
		}

		$breadcrumbs[] = array(
			'text' => sprintf( __( 'Browse by: %s', 'afcc' ), implode( '; ', $filter_breadcrumbs ) )
		);
	}

	return $breadcrumbs;
}

/**
 * Setup our custom breadcrumbs
 *
 * @see WPSEO_Breadcrumbs::set_crumbs
 *
 * @param 	array	$breadcrumbs	The existing breadcrumbs as previously built
 * @return	array	The modified breadcrumbs
 */
function afcc_breadcrumb_links( $breadcrumbs ) {
	global $wp_query;

	if ( is_shop() || is_product_category() || is_product_taxonomy() ) {
		$current_category      = get_active_product_category();
		$category_crumb        = find_breadcrumb_by_key( 'term', $breadcrumbs );
		$category_is_in_crumbs = false;
		$filters               = array();

		if ( $category_crumb !== false && 'product_cat' === $category_crumb->taxonomy ) {
			$category_is_in_crumbs = true;
		}

		if ( ! is_null( $current_category ) && ! $category_is_in_crumbs ) {
			$breadcrumbs[] = array(
				'url'  => esc_url( get_term_link( $current_category ) ),
				'text' => $current_category->name,
			);
		}

		if ( ! empty( $wp_query->query_vars ) ) {
			$filters = $wp_query->query_vars;
		}

		$breadcrumbs = afcc_build_filter_breadcrumbs( $filters, $breadcrumbs, $_GET );
	} elseif ( products_are_filtered() ) {
		$filters     = get_product_filters();
		$breadcrumbs = afcc_build_filter_breadcrumbs( $filters, $breadcrumbs );
	} elseif ( is_singular( 'our-locations' ) ) {
		/**
		 * Notes:
		 *     We assume here that the last breadcrumb is the store location.
		 *     This assumption should always be valid because store locations
		 *     cannot be nested within each other. If for some reason it is not
		 *     valid, I am afraid that you have run into an unfortunate edge
		 *     case. - Joseph Leedy <jleedy@synapseresults.com>, 2015-03-20
		 */
		array_splice( $breadcrumbs, -1, 0,  array( array( 'id' => get_page_id_by_slug( 'flooring-store-locations' ) ) ) );
	}

	return $breadcrumbs;
}

add_filter( 'wpseo_breadcrumb_links', 'afcc_breadcrumb_links' );
