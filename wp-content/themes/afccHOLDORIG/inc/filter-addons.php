<?php
/******************************************************************************
 *              ___   _   _   _ __     __ _   _ __    ___    ___              *
 *             / __| | | | | | '_ \   / _` | | '_ \  / __|  / _ \             *
 *             \__ \ | |_| | | | | | | (_| | | |_) | \__ \ |  __/             *
 *             |___/  \__, | |_| |_|  \__,_| | .__/  |___/  \___|             *
 *                     __/ |                 | |                              *
 *                    |___/                  |_|                              *
 *                                                                            *
 *                    m a r k e t i n g  s o l u t i o n s                    *
 ******************************************************************************/

/**
 * Additional supporting functionality for the WooCommerce Product Filter
 * extension
 *
 * @author     Joseph Leedy <jleedy@synapseresults.com>
 * @category   AFCC
 * @package    AFCC_Theme
 * @copyright  Copyright 2015 Synapse Marketing Solutions (http://synapseresults.com/)
 * @license    Proprietary
 */

/**
 * Get an array of the current product prices
 *
 * @param string|array $product_ids
 */
function afcc_get_current_product_prices( $product_ids ) {
	global $wpdb;

	$prices = array();

	if ( is_array( $product_ids ) ) {
		$product_ids = implode( ',', $product_ids );
	}

	$sql  = 'SELECT %1$s.meta_value AS price FROM %1$s ';
	$sql .= 'INNER JOIN %2$s ON (%2$s.ID = %1$s.post_id) ';
	$sql .= 'WHERE ( %1$s.meta_key = \'_price\' OR %1$s.meta_key = \'_min_variation_price\' ) ';
	$sql .= 'AND %1$s.post_id IN (%3$s);';

	$query   = $wpdb->prepare( $sql, $wpdb->postmeta, $wpdb->posts, $product_ids );
	$results = $wpdb->get_results( $query );

	if ( is_null( $results ) ) {
		return $prices;
	}

	$i = 0;

	foreach ( $results as $result ) {
		$price = $result->price;

		if ( ! array_key_exists( $price, $prices ) ) {
			$prices[ $price ] = 0;
		} else {
			$prices[ $price ] += 1;
		}
	}

	ksort( $prices, SORT_NUMERIC );

	return $prices;
}
