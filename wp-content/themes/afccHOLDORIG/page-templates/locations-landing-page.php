<?php
/**
 * Template Name: Locations Landing Page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
get_header();
?>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCPjn2cql9LnlIPgjITW8GIJJIc34ZvEwk"></script>
	<script src="<?php bloginfo('template_directory') ?>/assets/js/lib/location-landing.js"></script>

	<section id="breadcrumbs" class="top-section">
		<div class="row">
			<div class="twelve columns">
				<?php
					if ( function_exists('yoast_breadcrumb') ) {
						yoast_breadcrumb();
					}
				?>
			</div>
		</div>
	</section>
	<section>
		<div class="row">
			<div class="twelve columns center lh-lots">
				<h1 class="h1-style"><?php the_title(); ?></h1>
			</div>
		</div>
		<div class="row">
			<div class="twelve columns center lh-lots">
			</div>
		</div>
	</section>
	<section class="loc-landing-map">
		<div id="map-canvas"></div>
	</section>
	<section id="locations">
		<div class="row loc-areas">
			<?php
			$args = array(
				'post_type'   => 'our-locations',
				'posts_per_page'   => -1
			);

			$query = new WP_Query( $args );
				
			if ( $query->have_posts() ) :
				while ( $query->have_posts() ) :
					$query->the_post();
					$locationData = get_location_data( get_the_id() );
					$location = $locationData['location'];
					$locationName = $locationData['locationName'];
					$locationNum = $locationData['locationNum'];
					$getDir = urlencode($location['address']);
					
			?>
						<div class="contain-loc">
							<div class="six columns" itemprop="location" itemscope itemtype="http://schema.org/Place">
								<h4 itemprop="name"><?php the_title(); ?></h4>
								<div class="added-pad">
									<?php 
										$corpOffice = get_field('corporate_office');
										if( $corpOffice == 'yes' ) :
									?>
									<p class="small nm corp-office">Corporate Office</p>
									<?php endif; ?>
									<span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
										<p class="address-line" itemprop="streetAddress"><?php echo $location['address']; ?></p>
									</span>
									<p class="phone-number" itemprop="telephone"><?php echo $locationData['locationNum']; ?></p>
									<div class="contain-single-loc-links">
										<a href="<?php the_permalink(); ?>" class="db fl caps">View Location</a>
										<a href="https://www.google.com/maps/place/<?php echo $getDir; ?>" class="db fl caps" target="_blank">Get Directions</a>
										<a href="<?php echo get_permalink(68); ?>" class="db fl caps">Make Appointment</a>
									</div>
								</div>
							</div>
						</div>
			<?php
				endwhile;
			endif;
			wp_reset_postdata();
			?>
		</div>
	</section>

<?php
get_footer();
