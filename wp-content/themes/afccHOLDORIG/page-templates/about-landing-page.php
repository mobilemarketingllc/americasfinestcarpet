<?php
/**
 * Template Name: About Landing Page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
get_header(); ?>
<?php 
if ( has_post_thumbnail() ) {
	$large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
}
$subHead = get_field('main_sub_headline');
?>
	<section class="parallax added" data-stellar-vertical-offset="300" data-stellar-background-ratio="0.5" style="background-image: url(<?php echo $large_image_url[0]; ?>);">
		<div class="row">
			<div class="twelve columns np">
				<div class="centered-area">
					<div class="content">
						<h1 class="headline nm"><?php echo the_title(); ?></h1>
						<p class="nm large"><?php echo $subHead; ?></p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="breadcrumbs">
		<div class="row">
			<div class="twelve columns">
				<?php
					if ( function_exists('yoast_breadcrumb') ) {
						yoast_breadcrumb();
					}
				?>
			</div>
		</div>
	</section>
	<section>
		<div class="row">
			<div class="twelve columns center about-landing-content">
				<?php the_content(); ?>
			</div>
		</div>
	</section>
	<?php
		$ownerImg     = get_field('owners_image');
		$ownerName    = get_field('company_owner');
		$ownerTitle   = get_field('title');
		$subCopy      = get_field('small_sub_copy');
		$missionTitle = get_field('mission_title');
		$missionCopy  = get_field('mission_copy');
		$missionImg   = get_field('mission_bg');
		if( $ownerImg ) {
	?>
		<section class="custom-grid tall-five about-landing-page-blk">
			<div class="row">
				<div class="five columns">
					<div class="grid-bg tall full-size msnry center about-co">
						<img src="<?php echo $ownerImg['url']; ?>" alt="<?php echo $ownerImg['alt']; ?>" width="<?php echo $ownerImg['width']; ?>" height="<?php echo $ownerImg['height']; ?>" />
						<p class="carlos"><?php echo $ownerName; ?></p>
						<hr>
						<p class="small bold"><?php echo $ownerTitle; ?></p>
						<p class="small padded"><?php echo $subCopy; ?></p>
					</div>
				</div>
				<div class="seven columns npr-mobile npl">
					<div class="grid-bg tall full-size" style="background: url(<?php echo $missionImg; ?>) no-repeat">
						<span class="contain-white short">
							<?php echo $missionTitle; ?>
							<span class="small"><?php echo $missionCopy; ?></span>
						</span>
					</div>
				</div>
			</div>
		</section>
	<?php
		}
		$secContent = get_field('second_content_area');
		if($secContent) {
	?>
		<section>
			<div class="row">
				<div class="twelve columns center about-landing-content">
					<?php echo $secContent; ?>
				</div>
			</div>
		</section>
	<?php
		}
	?>

<?php
get_footer();
