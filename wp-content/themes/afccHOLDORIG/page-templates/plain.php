<?php
/**
 * Template Name: Plain Text Page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
get_header();

$headBg = get_field('header_bg');
?>

	

	<section class="parallax added more" data-stellar-background-ratio="0.5" style="background-image: url(<?php if( $headBg ){ echo $headBg; } ?>);">
		<div class="row">
			<div class="twelve columns np">
				<div class="centered-area">
					<div class="content">
						<h1 class="headline nm"><?php echo the_title(); ?></h1>
						<?php
							$subHead = get_field('main_sub_headline');
							if($subHead){
								echo '<p class="large nm">'.$subHead.'</p>';
							}
						?>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="breadcrumbs">
		<div class="row">
			<div class="twelve columns">
				<?php
					if ( function_exists('yoast_breadcrumb') ) {
						yoast_breadcrumb();
					}
				?>
			</div>
		</div>
	</section>
	<section id="plain-text-content">
		<div class="row">
			<div class="twelve columns">
				<?php the_content(); ?>
			</div>
		</div>
	</section>

<?php
get_footer();
