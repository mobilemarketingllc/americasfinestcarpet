<?php
/**
 * Template Name: Coupon NKM
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
get_header(); ?>
<?php 
if ( has_post_thumbnail() ) {
	$large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
}
$subHead = get_field('main_sub_headline');
?>
	<section id="coupon-header" class="parallax" data-stellar-background-ratio="0.5" style="background-image: url(<?php echo $large_image_url[0]; ?>); background-size: cover;">
		<div class="row">
			<div class="twelve columns">
				<div class="eight columns coupon-content" style="top:25% !important;">

                    <h1 style="color: #fff;">National Karastan Month</h1>

                    <p class="lg-title-custom">Get up to</p>
                    <p class="bold-lg-title-custom">$1,000 Back*</p>
                    <p class="sm-title-custom">On select Karastan styles</p>
                    <p style="margin-top: 20px; font-size: 14px;">Offer ends November 7, 2017</p>

					<?php /*
						$header = get_field('coupon_page_header', 'option');
						$deats  = get_field('coupon_details', 'option');
						if($header){
							echo $header;

						} */
						echo '<p class="small coupon-deats">*Receive up to $1000 back! Now is the time to save on gorgeous carpet from Karastan for a limited time only. During National Karastan Month you will find the lowest prices of the season. No Interest until January 2019. Whether the preferred decor is highstyle, trend-conscious, family living or romantic escape, Karastan has a product that helps consumers live beautifully.</p>';
					?>
                    <p style="font-size: 11px; margin-top: 20px; padding-right: 20px;">Only available from participating dealers. Please allow 8-10 weeks for processing.Applies only to first quality, running line product.Sale runs from September 21 - November 7, 2017.Entries must be postmarked by December 7, 2017.Does not apply to prior purchases and cannot be combined with other offers. Rebates will not be honored on any orders that are cancelled.Wool and Metropolitan carpet $5/yd. or $.55 per square foot. SmartStrand, WearDated, STAINMASTER, Kashmere & all other fibers $3/yd or $.33 per square foot. SmartStrand, WearDated, STAINMASTER & all other fibers $3 / yd or $.33 per square foot.</p>
				</div>
				<div class="four columns contain-coupon-form">
					<?php /*
						$formHeader = get_field('coupon_page_form_header', 'option');
						if($formHeader){
							echo $formHeader;
						} */
					?>

                    <h3 class="center"><span class="caps">Get</span> up to $1,000 Back</h3>
                    <p class="center small">To receive our flooring coupon, fill out<br>
                        the form below.</p>
					<?php //echo do_shortcode('[contact-form-7 id="8" title="Get Coupon"]'); ?>
                    <?php echo do_shortcode('[gravityform id="13" title="false" description="false"]'); ?>

				</div>
			</div>
		</div>
	</section>
	<section id="breadcrumbs">
		<div class="row">
			<div class="twelve columns">
				<?php
					if ( function_exists('yoast_breadcrumb') ) {
						yoast_breadcrumb();
					}
				?>
			</div>
		</div>
	</section>

	<section id="product-slider-container">
	<div class="row">
		<div class="twelve columns">
			<h1 class="center"><?php echo get_the_content(); ?></h1>
			<p class="center small"><?php echo $subHead; ?></p>
			<div id="product-slider">
				<?php if( have_rows('secondary_slider', 'option') ):
					// loop through the rows of data
				    while ( have_rows('secondary_slider', 'option') ) : the_row();
				        $sImg  = get_sub_field('image');
				        $sHead  = get_sub_field('headline');
				        $sContent  = get_sub_field('content');
				        $sCta  = get_sub_field('cta_text');
				        $sCtaLink  = get_sub_field('cta_link');
				?>
					<div class="slide center">
						<div class="eleven columns">
							<div class="six columns wbg getH">
								<h3><?php echo $sHead; ?></h3>
								<?php echo $sContent; ?>
								<a href="<?php echo $sCtaLink; ?>" class="cta"><?php echo $sCta; ?></a>
							</div>
							<div class="fl np set-mh with-image six columns">
								<img src="<?php echo $sImg['url']; ?>" alt="<?php echo $sImg['alt']; ?>" width="415" height="417"/>
							</div>
						</div>
					</div>
				<?php
					endwhile;
					endif;
				?>
			</div>
		</div>
	</div>
</section>

<?php
get_footer();
