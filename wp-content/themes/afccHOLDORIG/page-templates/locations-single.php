<?php
/**
 * Template Name: Single Location
 * Template Name Posts: Single Location 2
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
get_header();
global $post, $wp_query;
$mainImg = get_field('header_bg');

$locationData = get_location_data();
$location = $locationData['location'];
$locationName = $locationData['locationName'];
$locationNum = $locationData['locationNum'];

$shortLocation = explode(',',$location['address']);
$shortLocation = $shortLocation[0].' '.$shortLocation[3];
$getDir = str_replace(' ', '+', $location['address']);
$today = date('l');
$storeHours = get_store_hours();
$deats  = get_field('location_details');
$floor = get_field('header_txt');

function reformat_store_time( &$time ) {
	$time = date( 'H:i', strtotime( $time ) );
}
?>
	<section class="parallax added" data-stellar-background-ratio="0.5" style="background-image: url(<?php echo $mainImg; ?>);">
		<div class="row">
			<div class="twelve columns np">
				<div class="centered-area loc">
					<div class="content">
						<p class="med-title"><?php echo $locationName; ?></p>
						<hr class="half">
						<h1 class="headline nm">
							<?php if( $floor ) { echo $floor; } ?> <?php echo the_title(); ?>
						</h1>
						<p class="address large"><?php echo $shortLocation; ?></p>
						<p class="nm caps hours-title">Today's Hours</p>
						<?php
							if( array_key_exists($today, $storeHours) ):
								echo '<p class="nm large">'. $storeHours[$today] .'</p>';
							endif;
						?>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="breadcrumbs">
		<div class="row">
			<div class="twelve columns">
				<?php
					if ( function_exists('yoast_breadcrumb') ) {
						yoast_breadcrumb();
					}
				?>
			</div>
		</div>
	</section>
	<section>
		<div class="row">
			<div class="twelve columns center loc-copy">
				<?php the_content(); ?>
			</div>
		</div>
		<div class="row holds-map">
			<div class="seven columns">
				<div id="map-canvas"></div>
				<?php if( have_rows('location_employees') ): ?>
				<div class="team-members center">
					<?php
					// loop through the rows of data
					while ( have_rows('location_employees') ) : the_row();
						$employeePhoto  = get_sub_field('employee_photo');
						$employeeName  = get_sub_field('employee_name');
						$employeeTitle  = get_sub_field('employee_job_title');
						$employeeEmail  = get_sub_field('employee_email');

					?>
					<div>
						<!-- <div class="four columns">
							<img src="<?php //echo $employeePhoto['url']; ?>" alt="<?php //echo $employeePhoto['alt']; ?>" />
							<p class="member-name"><?php //echo $employeeName; ?></p>
							<hr class="small">
							<p class="member-title"><?php //echo $employeeTitle; ?></p>
                                                        <p class="member-title"><?php //echo $employeeEmail; ?></p>
						</div> -->
					</div>
					<?php endwhile; ?>
				</div>
				<?php endif; ?>
			</div>
			<div class="five columns" itemprop="location" itemscope itemtype="http://schema.org/Place">
				<div class="white-bg loc">
					<div class="grid-bg short added msnry" style="background: url(/wp-content/uploads/2015/02/carpet.jpg) no-repeat">
                        <a href="<?php echo get_permalink_with_requested_store_location( 1436, $post->ID ) ?>" class="contain-white short loc">
							Contact Us
							<span class="small">Have a question? Send us a message.</span>
						</a>
					</div>
					<div class="contain-loc-right-col">
						<h3 itemprop="name"><?php echo $locationName; ?></h3>
						<span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
							<p class="nm small-loc" itemprop="streetAddress"><?php echo $location['address']; ?></p>
							<?php if($deats) : ?>
							<p class="nm small-loc smaller"><?php echo $deats; ?></p>
							<?php endif; ?>
						</span>
						<p class="phone" itemprop="telephone"><?php echo $locationNum; ?></p>
						<?php if ( have_rows( 'store_hours' ) ) : ?>
						<h4 class="caps"><?php _e( 'Store Hours', 'afcc' ) ?></h4>
						<div class="contain-hours clear">
							<?php
							while ( have_rows( 'store_hours' ) ) :
								the_row();
								$hours_day      = get_sub_field( 'day_of_the_week' );
								$hours_time     = get_sub_field( 'store_hours_open_to_close' );
								$hour_time_data = explode( ' - ', $hours_time );

								array_walk( $hour_time_data, 'reformat_store_time' );

								$days = array(
									'Sunday'    => 'Su',
									'Monday'    => 'Mo',
									'Tuesday'   => 'Tu',
									'Wednesday' => 'We',
									'Thursday'  => 'Th',
									'Friday'    => 'Fr',
									'Saturday'  => 'Sa'
								);

								$hours_schema = $days[ $hours_day ] . ' ' . implode( '-', $hour_time_data );
							?>
							<div class="hours-row clear" itemprop="openingHours" datetime="<?php echo $hours_schema; ?>">
								<div class="fl day"><?php echo $hours_day; ?>:</div>
								<div class="fl hours"><?php echo $hours_time; ?></div>
							</div>
							<?php endwhile; ?>
						</div>
						<?php endif; ?>
						<div class="contain-prod-ctas">
							<a class="prod-info-link first req-appointment-link" href="<?php echo get_permalink( 68 ); ?>"><?php _e( 'Request Appointment', 'afcc' ) ?></a>
							<a class="prod-info-link req-measurement-link" href="http://www.americasfinestcarpet.com/request-information/">Request Info</a>
							<a class="prod-info-link req-design-consult" href="<?php echo get_permalink( 69 ); ?>"><?php _e( 'Design Consultation', 'afcc' ) ?></a>
							<a class="prod-info-link get-directions-link" href="<?php echo esc_url( 'https://www.google.com/maps/place/' . $getDir ); ?>" target="_blank"><?php _e( 'Get Directions', 'afcc' ) ?></a>
							<?php
								$googleView = get_field('google_360_view');
								if( $googleView ) :
							?>
							<a class="prod-info-link get-directions-link google-360" href="<?php echo $googleView; ?>" target="_blank">Virtual Tour</a>
<?php endif; ?>

                                                        <?php
								$googlePlus = get_field('google_plus');
								if( $googlePlus ) :
							?>
							<a class="prod-info-link icon-google-plus" href="<?php echo $googlePlus; ?>" target="_blank">Google+</a>
<?php endif; ?>

							<a href="<?php echo get_permalink( 1058 ); ?>" class="cta special"><?php _e( 'Get Coupon', 'afcc' ) ?></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

<?php
get_footer();