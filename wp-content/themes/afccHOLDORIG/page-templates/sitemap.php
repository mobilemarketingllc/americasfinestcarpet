<?php
/**
 * Template Name: Sitemap
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
get_header(); ?>

	<section class="top-section">
		<div class="row">
			<div class="twelve columns">
				<h1 class="h1-style"><?php the_title(); ?></h1>
				<?php the_content(); ?>

				<?php if( is_page(330) ) : ?>
					 <?php dynamic_sidebar( 'Sitemap' ); ?>
				<?php endif; ?>
			</div>
		</div>
	</section>

<?php
get_footer();
