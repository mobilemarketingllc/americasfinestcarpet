<?php
/**
 * Template Name: FAQs
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
get_header();

$headBg = get_field('header_bg');
?>

	<section class="parallax added more" data-stellar-background-ratio="0.5" style="background-image: url(<?php if( $headBg ){ echo $headBg; } ?>);">
		<div class="row">
			<div class="twelve columns np">
				<div class="centered-area">
					<div class="content">
						<h1 class="headline nm"><?php echo the_title(); ?></h1>
						<?php
							$subHead = get_field('main_sub_headline');
							if($subHead){
								echo '<p class="large nm">'.$subHead.'</p>';
							}
						?>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="breadcrumbs">
		<div class="row">
			<div class="twelve columns">
				<?php
					if ( function_exists('yoast_breadcrumb') ) {
						yoast_breadcrumb();
					}
				?>
			</div>
		</div>
	</section>
	<section id="plain-text-content">
		<div class="row">
			<div class="twelve columns">
				<?php the_content(); ?>
				<?php if( have_rows('question_and_answer') ):
					// loop through the rows of data
				    while ( have_rows('question_and_answer') ) : the_row();
				        $faqTitle  = get_sub_field('faq_question');
				        $faqAnswer  = get_sub_field('faq_answer');
				?>
					<div class="faq-wrap">	
						<div class="faq-title">
						<h4 class="lg-title"><?php echo $faqTitle; ?></h4>
						</div>
						<div class="faq-answer">
							<?php echo $faqAnswer; ?>
						</div>
					</div>
				<?php
					endwhile;
					endif;
				?>
			</div>
		</div>
		<div class="row">
			<div class="twelve columns">
				<?php
					$secContent = get_field('second_content_area');
					if($secContent){
						echo '<p class="large nm">'.$secContent.'</p>';
					}
				?>
			</div>
		</div>
	</section>

<?php
get_footer();
