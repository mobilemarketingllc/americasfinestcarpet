<?php
/**
 * Template Name: Form Page 2
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
get_header();

$pageID = get_the_id();

switch ($pageID) {
	case 1060:
		$fBoxID = 'req-info-wrap';
		$formID = '1063';
        $gformID = '3';
		$formTitle = 'Request Information';
		break;

	case 69:
		$fBoxID = 'design-consult-wrap';
		$formID = '1431';
        $gformID = '4';
		$formTitle = 'Free Design Consultation';
		break;

	case 68:
		$fBoxID = 'appointment-wrap';
		$formID = '1432';
        $gformID = '9';
		$formTitle = 'Schedule an Appointment';
		break;

	case 1423:
		$fBoxID = 'in-home-estimate-wrap';
		$formID = '1433';
        $gformID = '6';
		$formTitle = 'In-Home Estimate';
		break;

	case 1429:
		$fBoxID = 'measurement-wrap';
		$formID = '1434';
        $gformID = '7';
		$formTitle = 'Free Measurement';
		break;

	case 10552:
		$fBoxID = 'measurement-wrap';
		$formID = '10551';
        $gformID = '8';
		$formTitle = 'Carpet Cleaning Inquiry';
		break;

	case 1436:
		$fBoxID = 'contact-us-wrap';
		$formID = '1438';
        $gformID = '3';
		$formTitle = 'Contact Us';
		break;

	case 1464:
		$fBoxID = 'showroom-wrap';
		$formID = '1463';
        $gformID = '10';
		$formTitle = 'Visit Showroom';
		break;

	case 6821:
		$fBoxID = 'wt-req-info-wrap';
		$formID = '6819';
        $gformID = '3';
		$formTitle = 'Get Details';
		break;

    case 11782:
        $fBoxID = 'shop-at-home-wrap';
        $formID = '';
        $gformID = '14';
        $formTitle = 'Shop At Home';
        break;
}
?>


<script type="text/javascript">
jQuery(document).ready(function ($) {
 gform.addFilter( 'gform_datepicker_options_pre_init', function( optionsObj, formId, fieldId ) {
 if ( formId == 14 ) {
 optionsObj.minDate = '+2 D';
 }
 return optionsObj;
 } );
});
</script>


<div class="form-background">

	<section id="breadcrumbs" class="top-section">
		<div class="row">
			<div class="twelve columns">
				<?php
					if ( function_exists('yoast_breadcrumb') ) {
						yoast_breadcrumb();
					}
				?>
			</div>
		</div>
	</section>

	<section>
		<div id="<?php echo $fBoxID; ?>" class="form-container-all row">
			<div class="twelve columns">
				<h1 class="h1-style center" style="color: #fff !important; margin-bottom: 60px; font-size: 50px;"><?php the_title(); ?></h1>

			</div>
			<div class="twelve columns" style="background: #fff; margin-bottom: 60px; padding: 20px;">
                <div class="small up center"><?php the_content(); ?></div>
                <?php echo do_shortcode('[gravityform id="'.$gformID.'" title="false" description="false" ajax="true"]'); ?>
                <small>* Fields marked with an asterisk are required.<br>
                    ** Text message and data rates may apply depending on your carrier and plan.Text "STOP" to 96000 to opt-out.</small>
                <?php //echo do_shortcode('[contact-form-7 id="'.$formID.'" title="'.$formTitle.'"]'); ?>
			</div>
		</div>
	</section>
</div>
<?php
get_footer();
