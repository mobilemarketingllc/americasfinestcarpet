<?php
/**
 * Template Name: Coupon
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
get_header(); ?>
<?php 
if ( has_post_thumbnail() ) {
	$large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
}
$subHead = get_field('main_sub_headline');
?>
	<section id="coupon-header" class="parallax" data-stellar-background-ratio="0.5" style="background-image: url(<?php echo $large_image_url[0]; ?>);">
		<div class="row">
			<div class="twelve columns">
				<div class="eight columns coupon-content">
					<?php
						$header = get_field('coupon_page_header', 'option');
						$deats  = get_field('coupon_details', 'option');
						if($header){
							echo $header;

						}
						echo '<p class="small coupon-deats">'.$deats.'</p>';
					?>
				</div>
				<div class="four columns contain-coupon-form">
					<?php
						$formHeader = get_field('coupon_page_form_header', 'option');
						if($formHeader){
							echo $formHeader;
						}
					?>
					<?php //echo do_shortcode('[contact-form-7 id="8" title="Get Coupon"]'); ?>
                    <?php echo do_shortcode('[gravityform id="1" title="false" description="false"]'); ?>

				</div>
			</div>
		</div>
	</section>
	<section id="breadcrumbs">
		<div class="row">
			<div class="twelve columns">
				<?php
					if ( function_exists('yoast_breadcrumb') ) {
						yoast_breadcrumb();
					}
				?>
			</div>
		</div>
	</section>

	<section id="product-slider-container">
	<div class="row">
		<div class="twelve columns">
			<h1 class="center"><?php echo get_the_content(); ?></h1>
			<p class="center small"><?php echo $subHead; ?></p>
			<div id="product-slider">
				<?php if( have_rows('secondary_slider', 'option') ):
					// loop through the rows of data
				    while ( have_rows('secondary_slider', 'option') ) : the_row();
				        $sImg  = get_sub_field('image');
				        $sHead  = get_sub_field('headline');
				        $sContent  = get_sub_field('content');
				        $sCta  = get_sub_field('cta_text');
				        $sCtaLink  = get_sub_field('cta_link');
				?>
					<div class="slide center">
						<div class="eleven columns">
							<div class="six columns wbg getH">
								<h3><?php echo $sHead; ?></h3>
								<?php echo $sContent; ?>
								<a href="<?php echo $sCtaLink; ?>" class="cta"><?php echo $sCta; ?></a>
							</div>
							<div class="fl np set-mh with-image six columns">
								<img src="<?php echo $sImg['url']; ?>" alt="<?php echo $sImg['alt']; ?>" width="415" height="417"/>
							</div>
						</div>
					</div>
				<?php
					endwhile;
					endif;
				?>
			</div>
		</div>
	</div>
</section>

<?php
get_footer();