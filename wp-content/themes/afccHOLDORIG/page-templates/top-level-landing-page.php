<?php
/**
 * Template Name: Main Landing Page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
get_header(); ?>
<?php 
if ( has_post_thumbnail() ) {
	$large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
}
?>
	<section id="slider">
		<div class="row wide">
			<div class="twelve columns np lp-slider">
				<div id="home-slide">
					<?php if( have_rows('featured_product_slider') ):
						// loop through the rows of data
						$catalog = get_field('product_category');
					    if ( $catalog && ! is_wp_error( $catalog ) ) :
					    	while ( have_rows('featured_product_slider') ) : the_row();
					        $mImg  = get_sub_field('background_image');
					        $mHead  = get_sub_field('headline');
					        $mSubHead  = get_sub_field('sub_headline');
					        $mCta  = get_sub_field('cta_text');
					        $catLink = get_sub_field('category_link_landing');
					        if( $catLink === 'yes' ) {
					        	$mCtaLink = get_sub_field('cta_internal_link_landing');
					        } else {
					        	$mCtaLink = get_sub_field('cta_link');
					        }
					?>
						<div class="slide centered-area" style="background-image: url(<?php echo $mImg; ?>);">
							<div class="content">
								<p class="headline nm"><?php echo $mHead; ?></p>
								<p class="small-copy nm"><?php echo $mSubHead; ?></p>
								<a href="<?php echo $mCtaLink; ?>" class="cta"><?php echo $mCta; ?></a>
                                <?php
                                if ( is_page(10864) ) { ?>
                                    <a href="https://afcc.area-rugs.store/area-rugs" class="browse">Browse Catalog</a>
                              <?php  } else { ?>
                                    <a href="<?php  echo get_term_link($catalog->slug, 'product_cat'); ?>" class="browse">Browse Catalog</a>

<?php                                    }
                                ?>
							</div>
						</div>
					<?php
							endwhile;
						endif;
					endif;
					?>
				</div>
			</div>
		</div>
	</section>
	<section id="breadcrumbs">
		<div class="row">
			<div class="twelve columns">
				<?php
					if ( function_exists('yoast_breadcrumb') ) {
						yoast_breadcrumb();
					}
				?>
			</div>
		</div>
	</section>
	<section>
		<div class="row">
			<div class="twelve columns center">
				<h1 class="nm h1-style"><?php the_title(); ?></h1>
				<p class="small"><?php echo get_field('main_sub_headline'); ?></p>
			</div>
		</div>
	</section>

	<section class="lp-blks">
		<div class="row added">
			<?php
				$pageSlug = $post->post_name;
				$terms = get_terms("pa_brand");
				$termID = -1;
				$brandBg = get_field('brands_background_image');
				$term = get_field('product_category');
			?>
			<div class="five columns npr customH">
				<div class="white-bg">
					<div class="grid-bg short added msnry" style="background: url(<?php echo $brandBg; ?>) no-repeat">
						<?php if( $pageSlug == 'window-treatments' && $term ) { ?>
							<a href="<?php echo get_term_link($term->slug, 'product_cat'); ?>" class="contain-white short">
                                <?php } elseif (is_page('10864')) { ?>
                                <a href="https://afcc.area-rugs.store/area-rugs/brand" class="contain-white short">
                                <?php } else {  ?>
							<a href="<?php echo get_term_link($catalog->slug, 'product_cat'); ?>" class="contain-white short">
						<?php }  ?>
							<?php echo ucwords( str_replace('-',' ',$pageSlug) ); ?> Brands
							<span class="small"><?php echo get_the_excerpt(); ?></span>
						</a>
					</div>
					<ul class="center cat-brands">
						<?php
                        $brands = get_category_brands($catalog);

                        foreach($brands as $brand) {
                            if( $catalog->slug == 'window-treatments' ){
                                echo '<li><a href="'.get_term_link($term->slug, 'product_cat').'">'.$brand->name.'</a></li>';
                            }
                            else {
                                //echo '<li><a href="/shop/?product_cat='. $catalog->slug .'&pa_brand='. $brand->slug .'&post_type=product">'.$brand->name.'</a></li>';
                                echo '<li><a href="'.get_term_link( $catalog->slug, 'product_cat' ) .'?pa_brand='. $brand->slug .'&post_type=product&instock_products=in">'.$brand->name.'</a></li>';
                            }
                        }
						?>
                        <?php if (is_page('10864')) { ?>
                            <li><a href="https://afcc.area-rugs.store/area-rugs/brand?mohawk_brand=1917">Pantone Universe</a></li>
                            <li><a href="https://afcc.area-rugs.store/area-rugs/brand?mohawk_brand=1748">Trans Ocean Group</a></li>
                            <li><a href="https://afcc.area-rugs.store/area-rugs/brand?mohawk_brand=152">American Rug Craftsmen</a></li>
                            <li><a href="https://afcc.area-rugs.store/area-rugs/brand?mohawk_brand=150">Bob Timberlake</a></li>
                            <li><a href="https://afcc.area-rugs.store/area-rugs/brand?mohawk_brand=153">Karastan</a></li>
                            <li><a href="https://afcc.area-rugs.store/area-rugs/brand?mohawk_brand=151">Mohawk Home</a></li>
                            <li><a href="https://afcc.area-rugs.store/area-rugs/brand?mohawk_brand=154">Oriental Weavers</a></li>
                          <?php  } ?>
					</ul>
				</div>
			</div>

			<?php 
				if( $term ):
				$category_id = $term->term_id;
				$thumbnail_id = get_woocommerce_term_meta( $term->term_id, 'thumbnail_id', true ); 
				$image = wp_get_attachment_url( $thumbnail_id );

			?>
				
				<div class="seven columns npr-mobile">
					<div class="grid-bg tall customH" style="background: url(<?php echo $image; ?>) no-repeat">


                    <?php if (is_page('10864')) { ?>
                        <a href="https://afcc.area-rugs.store/area-rugs" class="contain-white tall">
                            Browse Our Area Rugs Catalog
                            <span class="small"><?php echo $term->description; ?></span>
                        </a>

                    <?php } else { ?>
                        <a href="<?php  echo get_term_link($term->slug, 'product_cat'); ?>" class="contain-white tall">
                            Browse Our <?php echo $term->name; ?> Catalog
                            <span class="small"><?php echo $term->description; ?></span>
                        </a>
                    <?php } ?>


					</div>
				</div>
			<?php endif; ?>
		</div>
	</section>

	<?php 
		if ( has_post_thumbnail() ) {
			$featImg = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
		}
		$vOff = get_field('parallax_bg_vertical_offset');
	?>
	<section data-stellar-vertical-offset="<?php echo $vOff; ?>" data-stellar-background-ratio="0.5" class=" parallax landing-parallax-contain" style="background-image: url(<?php echo $featImg[0]; ?>); background-size: cover;">
		<div class="row">
			<div class="twelve columns center landing-parallax-contain" >
				<div class="contain-black">
					<?php the_content();?>
				</div>
			</div>
		</div>
	</section>
	<section class="lp-blks second">
		<div class="row">
			<div class="twelve columns center below-scroll-headline">
				<?php echo get_field('below_scroll_headline'); ?>
			</div>
				<?php
					$args = array( 
						'post_parent' => $post->ID,
						'post_type'   => 'page',
						'orderby'     => 'menu_order',
						'order'       => 'ASC'
					);

					$the_query = new WP_Query( $args );
					$i = 0;
					// The Loop
					if ( $the_query->have_posts() ) {
						while ( $the_query->have_posts() ) {
							$i++;
							$the_query->the_post();
							$imgUrl = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
							if ( $i % 3 != 0 ) {
								$pad = ' added';
							} else {
								$pad = '';
							}
							if ( $i % 6 == 0 || $i % 6 == 1 ) {
								$colW = 'seven npr-mobile';
								$mar = 'tall';
								$height = 'tall';
							}
							else {
								$colW = 'five npl';
								$mar = 'msnry short';
								$height = 'short';
							}
							if( has_post_thumbnail() ) {
				?>
								<div class="<?php echo $colW.$pad; ?> columns">
									<div class="grid-bg full-size <?php echo $mar; ?>" style="background: url(<?php echo $imgUrl; ?>) no-repeat center center;">
										<a href="<?php the_permalink(); ?>" class="contain-white <?php echo $height; ?>">
											<?php
												if( $i === 3 ) {
													echo 'Care &amp; Maintenance';
												} else {
													the_title();
												}
											?>
											<span class="small"><?php echo get_field('main_sub_headline'); ?></span>
										</a>
									</div>
								</div>
				<?php
							}
						}
					}
					wp_reset_postdata();
				?>
<?php if (is_page('10864')) { ?>
    <div style="text-align: center; padding-bottom: 30px;">
        <a href="https://afcc.area-rugs.store/area-rugs-101" class="cta">Learn more</a>
    </div>
    <?php } ?>
		</div>
	</section>

<?php
get_footer();
