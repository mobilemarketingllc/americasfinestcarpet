<?php
/**
 * Template Name: Homepage
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
get_header(); ?>

<section id="slider">
	<div class="row wide">
		<div class="twelve columns np">
			<div id="home-slide">
				<?php if( have_rows('main_slider') ):
 $countvideo = 1;
					// loop through the rows of data
				    while ( have_rows('main_slider') ) : the_row();
				        $mImg  = get_sub_field('background_image');
				        $mHead  = get_sub_field('headline');
						if( strpos($mHead, '[') !== false ) {
				        	$mHead = apply_filters('the_content', $mHead);
						}
				        $mSubHead  = get_sub_field('sub_headline');
				        $mCta  = get_sub_field('cta_text');
				        //$mCtaLink  = get_sub_field('cta_link');
				        $catLink = get_sub_field('category_link');
				        if( $catLink === 'yes' ) {
				        	$mCtaLink = get_sub_field('cta_internal_link');
				        } else {
				        	$mCtaLink = get_sub_field('cta_link');
				        }
$mImg_mp4  = get_sub_field('background_video');
				?>
					<div class="slide centered-area">
                    <script type="text/javascript">
                        jQuery(document).ready(function ($) {	
                            $( window ).load(function() {
                            $( '#theVideo<?php echo $countvideo;?>' ).get(0).play();
                            });
                        });
                    </script>
                    <video width="100%" loop muted id="theVideo<?php echo $countvideo;?>" poster="<?php echo $mImg; ?>">
                        <source src="<?php echo $mImg_mp4; ?>" type="video/mp4" />                           
                    </video>
						<div class="content">
							<p class="headline nm"><?php echo $mHead; ?></p>
							<p class="small-copy nm"><?php echo $mSubHead; ?></p>
							<a href="<?php echo $mCtaLink; ?>" class="cta"><?php echo $mCta; ?></a>
						</div>
					</div>
				<?php
                                          $countvideo++;
					endwhile;
					endif;
				?>
			</div>
		</div>
	</div>
</section>
<section id="product-slider-container">
	<div class="row">
		<div class="twelve columns">
			<h1 class="center"><?php the_title(); ?></h1>
			<p class="center small"><?php echo get_the_content(); ?></p>
			<div id="product-slider">
				<?php if( have_rows('secondary_slider', 'option') ):
					// loop through the rows of data
				    while ( have_rows('secondary_slider', 'option') ) : the_row();
				        $sImg  = get_sub_field('image');
				        $sHead  = get_sub_field('headline');
				        $sContent  = get_sub_field('content');
				        $sCta  = get_sub_field('cta_text');
				        $sCtaLink  = get_sub_field('cta_link');
				?>
					<div class="slide center">
						<div class="eleven columns">
							<div class="six columns wbg set-mh">
								<h3><?php echo $sHead; ?></h3>
								<?php echo $sContent; ?>
								<a href="<?php echo $sCtaLink; ?>" class="cta"><?php echo $sCta; ?></a>
							</div>
							<div class="fl np set-mh six columns with-img">
								<img src="<?php echo $sImg['url']; ?>" alt="<?php echo $sImg['alt']; ?>" width="415" height="417"/>
							</div>
						</div>
					</div>
				<?php
					endwhile;
					endif;
				?>
			</div>
		</div>
	</div>
</section>
<section id="inspiration-grid" class="custom-grid">
	<div class="row">
		<div class="twelve columns">
			<h2 class="h1-style center">Inspiration Gallery</h2>
			<p class="center small">Get inspired by the newest materials and styles in real homes to find the perfect flooring for your living spaces.</p>
		</div>
	</div>

	<div class="row added">
		<?php
			if(function_exists('get_field')) {
				if( have_rows('grid') ) {
					while( have_rows('grid') ): the_row();
						if(get_row_layout() == 'five_left_seven_right') {
							if( have_rows('column_details_five_seven') ) {
								// FIVE COLUMNS LEFT
								$i = 0;
								// $rows = get_sub_field('column_details_five_seven');
								// $row_count = count($rows);
								// echo '<pre>'.print_r($rows, true).'</pre>';
								// var_dump($row_count);
								echo '<div class="five columns npr">';
								while( have_rows('column_details_five_seven') ): the_row();
									$fiveSevenBg      = get_sub_field('background_five_seven');
									$fiveSevenHead    = get_sub_field('headline_five_seven');
									$fiveSevenSubHead = get_sub_field('sub_headline_five_seven');
									$fiveSevenLink    = get_sub_field('page_link_five_seven');
									$i++;
									if( $i %2 == 0 ) {
										//even
										$classes = 'nmr-mobile';
									} else {
										//odd
										$classes = 'added';
									}
					?>
									<div class="grid-bg short msnry <?php echo $classes; ?>" style="background: url(<?php echo $fiveSevenBg; ?>) no-repeat center center;">
										<a href="<?php echo $fiveSevenLink; ?>" class="contain-white short">
											<?php echo $fiveSevenHead; ?>
											<span class="small"><?php echo $fiveSevenSubHead; ?></span>
										</a>
									</div>
					<?php
								endwhile;
								echo '</div>';
							}
							// SEVEN COLUMNS RIGHT
							$fiveSevenBgNr      = get_sub_field('background_five_seven_nr');
							$fiveSevenHeadNr    = get_sub_field('headline_five_seven_nr');
							$fiveSevenSubHeadNr = get_sub_field('sub_headline_five_seven_nr');
							$fiveSevenLinkNr    = get_sub_field('page_link_five_seven_nr');
					?>
							<div class="seven columns npr-mobile">
								<div class="grid-bg tall" style="background: url(<?php echo $fiveSevenBgNr; ?>) no-repeat center center;">
									<a href="<?php echo $fiveSevenLinkNr; ?>" class="contain-white tall">
										<?php echo $fiveSevenHeadNr; ?>
										<span class="small"><?php echo $fiveSevenSubHeadNr; ?></span>
									</a>
								</div>
							</div>
					<?php
						} elseif (get_row_layout() == 'seven_left_five_right') {
							// SEVEN COLUMNS LEFT
							$sevenFiveBgNr      = get_sub_field('background_seven_five_nr');
							$sevenFiveHeadNr    = get_sub_field('headline_seven_five_nr');
							$sevenFiveSubHeadNr = get_sub_field('sub_headline_seven_five_nr');
							$sevenFiveLinkNr    = get_sub_field('page_link_seven_five_nr');
					?>
							<div class="seven columns tall npl-mobile">
								<div class="grid-bg tall" style="background: url(<?php echo $sevenFiveBgNr; ?>) no-repeat center center;">
									<a href="<?php echo $sevenFiveLinkNr; ?>" class="contain-white tall">
										<?php echo $sevenFiveHeadNr; ?>
										<span class="small"><?php echo $sevenFiveSubHeadNr; ?></span>
									</a>
								</div>
							</div>
					<?php
							if( have_rows('column_details_seven_five') ) {
								// FIVE COLUMNS RIGHT
								$i = 0;
								echo '<div class="five columns npl">';
								while( have_rows('column_details_seven_five') ): the_row();
									$sevenFiveBg      = get_sub_field('background_seven_five');
									$sevenFiveHead    = get_sub_field('headline_seven_five');
									$sevenFiveSubHead = get_sub_field('sub_headline_seven_five');
									$sevenFiveLink    = get_sub_field('page_link_seven_five');
									$i++;
									if( $i %2 == 0 ) {
										//even
										$classes = 'nmr-mobile';
									} else {
										//odd
										$classes = 'added';
									}
					?>
									<div class="grid-bg short msnry <?php echo $classes; ?>" style="background: url(<?php echo $sevenFiveBg; ?>) no-repeat center center;">
										<a href="<?php echo $sevenFiveLink; ?>" class="contain-white short">
											<?php echo $sevenFiveHead; ?>
											<span class="small"><?php echo $sevenFiveSubHead; ?></span>
										</a>
									</div>
					<?php
								endwhile;
								echo '</div>';
							}
						}
						echo '<div class="clear added"></div>';
					endwhile;
				}
			}
		?>
	</div>
</section>
<?php
	$paraImg  = get_field('parallax_bg');
	$paraHead  = get_field('headline');
	$paraSubHead  = get_field('sub_headline');
	$paraContent  = get_field('content');
	$paraOffset  = get_field('parallax_vertical_offset');
?>
	<section id="about-americas-finest" class="parallax" data-stellar-vertical-offset="<?php echo $paraOffset; ?>" data-stellar-background-ratio="0.5" style="background-image: url(<?php echo $paraImg['url']; ?>);">
		<div class="row">
			<div class="six columns">
				<div class="table">
					<div class="table-cell">
						<h3 class="h1-style bold"><?php echo $paraHead; ?></h3>
						<h4><?php echo $paraSubHead; ?></h4>
						<?php echo $paraContent; ?>
					</div>
				</div>
			</div>
		</div>
	</section>

<?php
get_footer();
