<?php
/**
 * afcc functions and definitions
 *
 * @package afcc
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}

if ( ! function_exists( 'afcc_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function afcc_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on afcc, use a find and replace
	 * to change 'afcc' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'afcc', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	add_theme_support( 'woocommerce' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	//add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'afcc' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'afcc_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif; // afcc_setup
add_action( 'after_setup_theme', 'afcc_setup' );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function afcc_widgets_init() {
	// register_sidebar( array(
	// 	'name'          => __( 'Sidebar', 'afcc' ),
	// 	'id'            => 'sidebar-1',
	// 	'description'   => '',
	// 	'before_widget' => '<aside id="%1$s" class="widget %2$s">',
	// 	'after_widget'  => '</aside>',
	// 	'before_title'  => '<h1 class="widget-title">',
	// 	'after_title'   => '</h1>',
	// ) );
	register_sidebar( array(
		'name'          => __( 'Footer Block 1', 'afcc' ),
		'id'            => 'footer-block-1',
		'description'   => '',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '<li class="caps">',
		'after_title'   => '</li>',
	) );
	register_sidebar( array(
		'name'          => __( 'Footer Block 2', 'afcc' ),
		'id'            => 'footer-block-2',
		'description'   => '',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '<li class="caps">',
		'after_title'   => '</li>',
	) );
	register_sidebar( array(
		'name'          => __( 'Footer Block 3', 'afcc' ),
		'id'            => 'footer-block-3',
		'description'   => '',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '<li class="caps">',
		'after_title'   => '</li>',
	) );
	register_sidebar( array(
		'name'          => __( 'Footer Block 4', 'afcc' ),
		'id'            => 'footer-block-4',
		'description'   => '',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '<li class="caps">',
		'after_title'   => '</li>',
	) );
	register_sidebar( array(
		'name'          => __( 'Copyright Links', 'afcc' ),
		'id'            => 'copright-links',
		'description'   => '',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	) );
	register_sidebar( array(
		'name'          => __( 'Sitemap', 'afcc' ),
		'id'            => 'site-map',
		'class'         => 'sitemap-links',
		'description'   => '',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '<span class="hide">',
		'after_title'   => '</span>',
	) );
}
add_action( 'widgets_init', 'afcc_widgets_init' );

// remove wp jquery
function remove_scripts() {
	/* jQuery */
	if (!is_admin()) {
		wp_deregister_script('jquery');
		wp_register_script('jquery', get_template_directory_uri() . '/assets/js/lib/jquery.js', false, '1.11.1');
		wp_enqueue_script('jquery');
	}
}

add_action('wp_enqueue_scripts', 'remove_scripts');


add_filter( 'wp_head', 'favicon_filter' );
function favicon_filter() {
	echo '<link rel="Shortcut Icon" href="'.get_bloginfo('stylesheet_directory').'/assets/img/browser/favicon.jpg?'.date('m/j/y-h:i:s').'" type="image/x-icon" />' . "\n";
	echo '<link rel="apple-touch-icon-precomposed" sizes="114x114" href="'.get_bloginfo('stylesheet_directory').'/assets/img/browser/apple-touch-icon-114x114-precomposed.png?'.date('m/j/y-h:i:s').'" />'."\n";
	echo '<link rel="apple-touch-icon-precomposed" sizes="72x72" href="'.get_bloginfo('stylesheet_directory').'/assets/img/browser/apple-touch-icon-72x72-precomposed.png?'.date('m/j/y-h:i:s').'" />'."\n";
	echo '<link rel="apple-touch-icon-precomposed" href="'.get_bloginfo('stylesheet_directory').'/assets/img/browser/apple-touch-icon-precomposed.png?'.date('m/j/y-h:i:s').'" />'."\n";
}

/**
 * Enqueue scripts and styles.
 */

add_action( 'wp_head', 'ie8_scripts');
function ie8_scripts() { ?>

<!--[if lt IE 9]>
<script src="<?php bloginfo('template_directory'); ?>/assets/js/lib/html5.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/assets/js/lib/nwmatcher-min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/assets/js/lib/selectivizr-min.js"></script>
<![endif]-->

<?php }

add_action( 'wp_footer', 'all_but_ie8');
function all_but_ie8() { ?>

<!--[if lt IE 6 | gt IE 8]><!-->
<script src="<?php bloginfo('template_directory'); ?>/assets/js/lib/hammer.js"></script>
<!--<![endif]-->

<?php }


function afcc_scripts() {
	// Dequeue Styles and Scripts
	wp_dequeue_style('grid-list-button');
	wp_dequeue_script('grid-list-scripts');

	// Synapse Styles
	wp_enqueue_style( 'foundation-framework', get_template_directory_uri() . '/assets/css/lib-foundation/foundation.css', array(), '20150212' );
	wp_enqueue_style( 'mmenu-all-style', get_template_directory_uri() . '/assets/css/lib-mmenu/jquery.mmenu.all.css', array(), '20150217' );
	wp_enqueue_style( 'mmenu-style', get_template_directory_uri() . '/assets/css/lib-mmenu/jquery.mmenu.css', array(), '20150217' );
	wp_enqueue_style( 'mmenu-dragopen', get_template_directory_uri() . '/assets/css/lib-mmenu/jquery.mmenu.dragopen.css', array(), '20150217' );
	wp_enqueue_style( 'slick-styles', get_template_directory_uri() . '/assets/css/lib-slick-slider/slick.css', array(), '20150212' );
	wp_enqueue_style( 'fancybox-styles', get_template_directory_uri() . '/assets/css/lib-fancybox/jquery.fancybox.css', array(), '20150224' );
	wp_enqueue_style( 'ui-styles', get_template_directory_uri() . '/assets/css/lib-jquery/jquery-ui.css', array(), '20150313' );

	wp_enqueue_style( 'afcc-style', get_stylesheet_uri() );

	wp_enqueue_script( 'afcc-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );
	wp_enqueue_script( 'afcc-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

//	wp_enqueue_script( 'cf7', plugins_url().'/contact-form-7/includes/js/scripts.js', array('jquery-form'), '20130115', true );

	// Synapse Scripts
	wp_enqueue_script( 'ui', get_template_directory_uri() . '/assets/js/lib/jquery-ui.js', array('jquery'), '20150313', true );
	wp_enqueue_script( 'site-scripts', get_template_directory_uri() . '/assets/js/lib/scripts.js', array('jquery'), '20150212', true );
	wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/assets/js/lib/modernizr.min.js', array(), '20150325', true );
	wp_enqueue_script( 'foundation-script', get_template_directory_uri() . '/assets/js/lib-foundation/foundation.min.js', array('jquery'), '20150212', true );
	wp_enqueue_script( 'fancybox-script', get_template_directory_uri() . '/assets/js/lib-fancybox/jquery.fancybox.min.js', array('jquery'), '20150224', true );
	wp_enqueue_script( 'stellar', get_template_directory_uri() . '/assets/js/lib/stellar.js', array('jquery'), '20150216', true );
	wp_enqueue_script( 'mmenu-all', get_template_directory_uri() . '/assets/js/lib-mmenu/jquery.mmenu.min.all.js', array('jquery'), '20150217', true );
	wp_enqueue_script( 'mmenu-drag', get_template_directory_uri() . '/assets/js/lib-mmenu/jquery.mmenu.dragopen.min.js', array('jquery'), '20150217', true );
	wp_enqueue_script( 'slick-scripts', get_template_directory_uri() . '/assets/js/lib-slick-slider/slick.min.js', array('jquery'), '20150212', true );
	wp_enqueue_script( 'mix-it-up', get_template_directory_uri() . '/assets/js/lib-mix-it-up/jquery.mixitup.js', array('jquery'), '20150303', true );
	wp_enqueue_script( 'swipe', get_template_directory_uri() . '/assets/js/lib/swipe.js', array('jquery'), '20150330', true );
	wp_enqueue_script( 'placeholder', get_template_directory_uri() . '/assets/js/lib/placeholder.js', array('jquery'), '20150331', true );

	wp_register_script( 'gmaps','https://maps.googleapis.com/maps/api/js?key=AIzaSyCPjn2cql9LnlIPgjITW8GIJJIc34ZvEwk', false, false, true );
	wp_register_script( 'marker-labels', get_template_directory_uri() . '/assets/js/lib/markerwithlabel.js', array('gmaps'), '20150310', true );
	wp_register_script( 'locations-map', get_template_directory_uri() . '/assets/js/lib/locations.js', array('gmaps', 'marker-labels', 'jquery'), '20150306', true );
	if ( is_singular('our-locations') ) {
		$locData = array(get_location_data());
		wp_enqueue_script( 'gmaps');
		wp_localize_script( 'locations-map', 'AFCC_Map_Data', $locData );
		wp_enqueue_script( 'locations-map');
	}

	if ( is_page_template('page-templates/locations-landing-page.php') ) {
		wp_enqueue_script( 'gmaps');
		wp_enqueue_script( 'marker-labels');
		wp_localize_script( 'locations-map', 'AFCC_Map_Data', get_all_locations_data() );
		wp_enqueue_script( 'locations-map');
	}

	if ( is_product_category() || is_product_tag() || is_product_taxonomy() ) {
		$category = get_active_product_category();

		if ( ! is_tax( 'product_cat', 'window-treatments') && ( ! is_object( $category )
				|| ! property_exists( $category, 'parent' ) || $category->parent !== 1288 ) ) {
			wp_enqueue_script(
				'grid-list-scripts-new',
				get_template_directory_uri() . '/assets/js/lib/jquery.gridlistview.js',
				array( 'jquery', 'cookie', 'site-scripts' ),
				'20150224',
				true
			);
			wp_enqueue_script( 'pointertouch', get_template_directory_uri() . '/assets/js/lib-toggle/pointer.js', array('jquery'), '20150316', true );
			wp_enqueue_script( 'toggle-script', get_template_directory_uri() . '/assets/js/lib-toggle/toggle.js', array('jquery', 'pointertouch'), '20150316', true );
			wp_enqueue_script( 'underscore' );
			wp_enqueue_script(
				'availability-filter',
				get_template_directory_uri() . '/assets/js/lib/availability-filter.js',
				array( 'jquery', 'underscore', 'toggle-script', 'pointertouch' ),
				'20150324',
				true
			);
		}
	}
}
add_action( 'wp_enqueue_scripts', 'afcc_scripts', PHP_INT_MAX );

if( function_exists('acf_add_options_page') ) {
	 acf_add_options_page(array(
		'page_title' => 'Global Content',
		'menu_title' => 'Global Content',
		'menu_slug' => 'global-content',
		'capability' => 'edit_posts',
		'position'=> '',
		'icon_url'=> 'dashicons-admin-site',
		'redirect'=> true
	));
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Coupons',
		'menu_title'	=> 'Coupons',
		'parent_slug'	=> 'global-content',
	));
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Sliders',
		'menu_title'	=> 'Sliders',
		'parent_slug'	=> 'global-content',
	));
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Social',
		'menu_title'	=> 'Social',
		'parent_slug'	=> 'global-content',
	));
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Footer',
		'menu_title'	=> 'Footer',
		'parent_slug'	=> 'global-content',
	));

}
add_filter('widget_text', 'do_shortcode');

add_action( 'init', 'my_add_excerpts_to_pages' );
function my_add_excerpts_to_pages() {
	 add_post_type_support( 'page', 'excerpt' );
}

add_filter( 'mce_buttons_2', 'fb_mce_editor_buttons' );
function fb_mce_editor_buttons( $buttons ) {
	array_unshift( $buttons, 'styleselect' );
	return $buttons;
}
add_filter( 'tiny_mce_before_init', 'fb_mce_before_init' );
function fb_mce_before_init( $settings ) {

	$style_formats = array(
		array(
			'title' => 'Large Title',
			'block' => 'p',
			'attributes' => array(
				'class' => 'lg-title-custom'
			)
		),
		array(
			'title' => 'Bold Large Title',
			'block' => 'p',
			'attributes' => array(
				'class' => 'bold-lg-title-custom'
			)
		),
		array(
			'title' => 'Small Title',
			'block' => 'p',
			'attributes' => array(
				'class' => 'sm-title-custom'
			)
		),
		array(
			'title' => 'Internal Page Title Centered',
			'block' => 'p',
			'attributes' => array(
				'class' => 'internal-page-title'
			)
		),
		array(
			'title' => 'Internal Page Sub Title Centered',
			'block' => 'p',
			'attributes' => array(
				'class' => 'internal-page-sub-title'
			)
		),
		array(
			'title' => 'Internal Page Headline',
			'block' => 'p',
			'attributes' => array(
				'class' => 'internal-page-headline'
			)
		),
		array(
			'title' => 'Internal Page Paragraph',
			'block' => 'p',
			'attributes' => array(
				'class' => 'internal-page-para'
			)
		),
		array(
			'title' => 'CTA',
			'selector' => 'a',
			'attributes' => array(
				'class' => 'cta'
			)
		),
		array(
			'title' => 'Non Bulleted List',
			'selector' => 'ul',
			'attributes' => array(
				'class' => 'custom-no-bull'
			)
		),
	);

	$settings['style_formats'] = json_encode( $style_formats );

	return $settings;

}

// add custom editor stylesheet
add_action( 'admin_init', 'add_my_editor_style' );
function add_my_editor_style() {
	add_editor_style( get_bloginfo('stylesheet_directory') . '/assets/css/lib-wp/custom-editor.css' );
}

function enqueue_admin_scripts() {
	wp_enqueue_style('admin', get_bloginfo('stylesheet_directory') . '/assets/css/lib-wp/custom-admin.css', false, '');
}
add_action( 'admin_enqueue_scripts', 'enqueue_admin_scripts' );

// Remove custom WooCommerce actions
remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_title', 5);
remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);
remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 50);

if( is_product_category() ) {
	remove_action('woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
	remove_action('woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);
}

/**
 * Remove default "Add to Cart" button as products cannot be purchased online
 */
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );

/**
 * Add "Get Coupon" button to product listing pages
 *
 * @param array $args
 */
function woocommerce_template_loop_get_coupon( $args = array() ) {
	wc_get_template( 'loop/afcc-get-coupon.php', $args );
}

add_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_get_coupon', 10 );

add_action('woocommerce_before_single_product_summary', 'afcc_template_product_attributes', 30);
add_action('woocommerce_before_single_product_summary', 'woocommerce_template_single_price', 40);
add_action('woocommerce_before_single_product_summary', 'afcc_template_product_forms', 50);
add_action('woocommerce_single_product_summary', 'afcc_template_product_gallery', 10);
add_action('woocommerce_after_single_product_summary', 'afcc_template_product_desc', 50);
add_action( 'afcc_prod_title', 'set_prod_title', 5 );
add_action( 'woocommerce_before_main_content', 'afcc_custom_breadcrumbs', 5 );
add_action( 'woocommerce_before_main_content', 'afcc_availability_filter', 40 );

function afcc_availability_filter() {
	wc_get_template( 'afcc-availability-filter.php' );
}
function afcc_template_product_desc() {
	wc_get_template( 'single-product/afcc-product-desc.php' );
}
function afcc_custom_breadcrumbs() {
	wc_get_template( 'single-product/afcc-product-crumbs.php' );
}
function afcc_template_product_gallery() {
	wc_get_template( 'single-product/afcc-product-gallery.php' );
}
function afcc_template_product_forms() {
	wc_get_template( 'single-product/afcc-product-forms.php' );
}
function afcc_template_product_attributes() {
	wc_get_template( 'single-product/afcc-product-attributes.php' );
}
function set_prod_title() {
	wc_get_template( 'single-product/title.php' );
}

function move_actions() {
	global $WC_List_Grid;

	if ( ! is_shop() && ! is_product_category() && ! is_product_tag() && ! is_product_taxonomy() ) {
		return;
	}

	remove_action( 'woocommerce_before_shop_loop', array( $WC_List_Grid, 'gridlist_toggle_button' ), 30 );
	remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_single_excerpt', 5 );
	add_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_single_excerpt', 50 );

	$category = get_active_product_category();

	if ( is_shop() || is_tax( 'product_cat', 'window-treatments' ) || ( is_object( $category )
			&& property_exists( $category, 'parent' ) && $category->parent === 1288 ) ) {
		remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
		remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
	} else {
		add_action( 'woocommerce_before_main_content', array( $WC_List_Grid, 'gridlist_toggle_button' ), 30 );
	}
}
add_action( 'wp', 'move_actions', 30 );

function getPageHeaderBg($default = '') {
	$currentCat = get_active_product_category();
	$bgImg = '';
	if( !is_null($currentCat) ) {
			$bgImg = get_field('catalog_background_image', 'product_cat_'.$currentCat->term_id);
	}
	if( empty($bgImg) ) {
		if( !empty($default) ){
			$bgImg = $default;
		} else {
			$bgImg = get_bloginfo('stylesheet_directory').'/assets/img/backgrounds/wt-main.jpg';
		}
	}
	return $bgImg;
}

add_filter('wc_get_template', 'getWinTreatmentTemplate', 10, 5);
function getWinTreatmentTemplate($located, $template_name, $args, $template_path, $default_path) {
	if( is_product_category('window-treatments') && !empty($args) && isset($args['category']) && $template_name == 'content-product_cat.php' ) {
		$located = str_replace($template_name, 'content-product_cat-wt.php', $located);
	}
	return $located;
}
function get_location_data($postId = 0) {
	$locData = array(
		'location' => array(
			'address' => '730 Design Court, Chula Vista, CA, United States',
			'lat'     => '32.5954043',
			'lng'     => '-117.0224641'
		),
		'locationName' => 'America\'s Finest Carpet Company',
		'locationNum'  => '(619) 427-2420'
	);
	if( $postId === 0 ){
		global $post;
		if( !is_a($post, 'WP_Post') ) {
			return $locData;
		} else {
			$postId = $post->ID;
		}
	}

	$location = get_field('location_address', $postId);
	$locationName = get_field('location_title', $postId);
	$locationNum = get_field('location_phone_number', $postId);
	if( !empty($location) ) {
		$locData['location'] = $location;
	}
	if( !empty($locationName) ) {
		$locData['locationName'] = $locationName;
	}
	if( !empty($locationNum) ) {
		$locData['locationNum'] = $locationNum;
	}
	$today = date('l');
	$storeHours = get_store_hours();
	if( array_key_exists($today, $storeHours) ) {
		$locData['currentDay'] = $storeHours[$today];
	}
	return $locData;
}

function get_all_locations_data() {
	$args = array(
		'post_type'   => 'our-locations',
		'posts_per_page'   => -1
	);
	$allLocData = array();

	$query = new WP_Query( $args );
	if ( $query->have_posts() ) {
		while ( $query->have_posts() ) {
			$query->the_post();
			$locData = get_location_data( get_the_id() );
			$locData['links'] = array(
				'permalink' => get_the_permalink(),
				'google_directions' => 'https://www.google.com/maps/place/' . urlencode( $locData['location']['address'] ),
				'appointment' => get_permalink(68),
				'estimate' => get_permalink(1423),
				'coupon' => get_permalink(1058)
			);
			$allLocData[] = $locData;
		}
	}
	return $allLocData;
}

function get_store_hours() {
	global $post;
	$dotw = array();
	if( !is_a($post, 'WP_Post') ) {
		return $dotw;
	}
	if( have_rows('store_hours', $post->ID) ){
		while ( have_rows('store_hours') ) {
			the_row();
			$hourArr  = get_sub_field_object('day_of_the_week');
			$hours  = get_sub_field('store_hours_open_to_close');
			$dotw[$hourArr['value']] = $hours;
		}
	}
	return $dotw;
}

/*
* Add domain replacement variable to every cf7 email
*/
add_filter('wpcf7_mail_components', 'wpcf7_mail_components_add_domain');

/*
* Components comes in as an array of the email parts:
* 'subject', 'sender', 'body', 'recipient', 'additional_headers', 'attachments'
*/
function wpcf7_mail_components_add_domain($components) {
	$replacements = array(
		'[domain]'    => $_SERVER['SERVER_NAME'],
		'[year]'          => date('Y')
	);

	$components['body'] = str_replace(array_keys($replacements), array_values($replacements), $components['body']);
	return $components;
}

function my_login_stylesheet() {
	wp_enqueue_style( 'custom-login', get_template_directory_uri() . '/assets/css/lib-wp/custom-login.css' );
}
add_action( 'login_enqueue_scripts', 'my_login_stylesheet' );

/**
 * Add query var for requested product
 *
 * @param array $query_vars
 * @return array
 */
function add_requested_product_query_var( $query_vars ) {
	$query_vars[] = 'requested_product';

	return $query_vars;
}

add_filter( 'query_vars', 'add_requested_product_query_var' );

/**
 * Add query var for requested product category
 *
 * @param array $query_vars
 * @return array
 */
function add_requested_product_category_query_var( $query_vars ) {
	$query_vars[] = 'requested_product_category';

	return $query_vars;
}

add_filter( 'query_vars', 'add_requested_product_category_query_var' );

/**
 * Add query var for requested store location
 *
 * @param array $query_vars
 * @return array
 */
function add_requested_store_location_query_var( $query_vars ) {
	$query_vars[] = 'requested_location';

	return $query_vars;
}

add_filter( 'query_vars', 'add_requested_store_location_query_var' );

/**
 * Setup the requested product rewrite
 */
function add_requested_product_rewrite() {
	$page_ids = array( 1060, 1429 );

	foreach ($page_ids as $page_id) {
		$page = get_post( $page_id );

		if ( is_null( $page ) ) {
			continue;
		}

		$slug = $page->post_name;

		add_rewrite_rule(
			$slug . '/requested-product:([0-9]+)/?$',
			'index.php?pagename=' . $slug. '&requested_product=$matches[1]',
			'top'
		);
	}
}

add_action( 'init', 'add_requested_product_rewrite' );

/**
 * Setup the requested product category rewrite
 */
function add_requested_product_category_rewrite() {
	$page_ids = array( 6821 );

	foreach ($page_ids as $page_id) {
		$page = get_post( $page_id );

		if ( is_null( $page ) ) {
			continue;
		}

		$slug = $page->post_name;

		add_rewrite_rule(
			$slug . '/requested-product-category:([0-9]+)/?$',
			'index.php?pagename=' . $slug . '&requested_product_category=$matches[1]',
			'top'
		);
	}
}

add_action( 'init', 'add_requested_product_category_rewrite' );

/**
 * Setup the requested store location rewrite
 */
function add_requested_store_location_rewrite() {
	$page_ids = array( 1436 );

	foreach ($page_ids as $page_id) {
		$page = get_post( $page_id );

		if ( is_null( $page ) ) {
			continue;
		}

		$slug = $page->post_name;

		add_rewrite_rule(
			$slug . '/requested-store-location:([0-9]+)/?$',
			'index.php?pagename=' . $slug . '&requested_location=$matches[1]',
			'top'
		);
	}
}

add_action( 'init', 'add_requested_store_location_rewrite' );

/**
 * Add the requested product to the page permalink
 *
 * @param  int $page_id
 * @param  int $category_id
 * @return string
 */
function get_permalink_with_requested_product( $page_id, $product_id ) {
	return user_trailingslashit(
		untrailingslashit( get_permalink( $page_id ) ) . '/requested-product:' . $product_id
	);
}

/**
 * Add the requested product category to the page permalink
 *
 * @param  int $page_id
 * @param  int $category_id
 * @return string
 */
function get_permalink_with_requested_product_category( $page_id, $category_id ) {
	return user_trailingslashit(
		untrailingslashit( get_permalink( $page_id ) ) . '/requested-product-category:' . $category_id
	);
}

/**
 * Add the requested store location to the page permalink
 *
 * @param  int $page_id
 * @param  int $category_id
 * @return string
 */
function get_permalink_with_requested_store_location( $page_id, $location_id ) {
	return user_trailingslashit(
		untrailingslashit( get_permalink( $page_id ) ) . '/requested-store-location:' . $location_id
	);
}

/**
 * Handle the [requested_product] shortcode
 *
 * @param  array $attributes
 * @return string
 */
function requested_product_shortcode_handler( $attributes ) {
	$defaults     = array(
		'id' => 0,
	);
	$attributes   = shortcode_atts( $defaults, $attributes, 'requested_product' );
	$product_name = '';
	$product_id   = 0;

	if ( ! is_null( $attributes['id'] ) ) {
		$product_id = (int) $attributes['id'];
	} else {
		$product_id = (int) get_query_var( 'requested_product', 0 );
	}

	if ( $product_id !== 0 ) {
		$product = wc_get_product( $product_id );

		if ( $product !== false ) {
			$product_name = $product->get_title();
		}
	}

	return $product_name;
}

add_shortcode( 'requested_product', 'requested_product_shortcode_handler' );

/**
 * Show the requested item in the form title
 *
 * @param 	string	$title
 * @param	int   	$post_id
 * @return	string
 */
function add_requested_item_to_form_page_title( $title, $post_id = null ) {
	$allowed_form_pages = array(
		1060 => 'requested_product',
		1429 => 'requested_product',
		6821 => 'requested_product_category',
		1436 => 'requested_location',
	);

	if ( is_null( $post_id ) || ! array_key_exists( $post_id, $allowed_form_pages ) ) {
		return $title;
	}

	$item_id = (int) get_query_var( $allowed_form_pages[ $post_id ], 0 );

	if ( 0 === $item_id ) {
		return $title;
	}

	if ( 1429 === $post_id ) {
		$format = __( '%1$s of %2$s', 'afcc' );
	} else {
		$format = __( '%1$s About %2$s', 'afcc' );
	}

	$format_args  = array( $title );
	$format_title = false;

	if ( 'requested_product' === $allowed_form_pages[ $post_id ] ) {
		$product = wc_get_product( $item_id );

		if ( $product !== false ) {
			$color         = $product->get_attribute( 'color' );
			$format_args[] = $product->get_title();
			$format_title  = true;

			if ( '' !== $color ) {
				if ( 1429 === $post_id ) {
					$format = __( '%1$s of %2$s in %3$s', 'afcc' );
				} else {
					$format = __( '%1$s About %2$s in %3$s', 'afcc' );
				}

				$format_args[] = $color;
			}
		}
	} elseif ( 'requested_product_category' === $allowed_form_pages[ $post_id ] ) {
		$product_category = get_term( $item_id, 'product_cat' );

		if ( ! is_null( $product_category ) && ! is_wp_error( $product_category ) ) {
			$format_args[] = $product_category->name;
			$format_title  = true;
		}
	} elseif ( 'requested_location' === $allowed_form_pages[ $post_id ] ) {
		$location_name = get_the_title( $item_id );

		if ( '' !== $location_name ) {
			$format        = __( '%1$s In %2$s', 'afcc' );
			$format_args[] = $location_name;
			$format_title  = true;
		}
	}

	if ( $format_title ) {
		$title = vsprintf( $format, $format_args );
	}

	return $title;
}

add_filter( 'the_title', 'add_requested_item_to_form_page_title', 10, 2 );

function fix_product_filter_query( $query ) {
	if ( $query->is_post_type_archive() === false ) {
		return $query;
	}

	if ( ! array_key_exists( 'product_cat', $query->query_vars ) ) {
		return $query;
	}

	/**
	 * Ensure that the product category is actually being queried
	 */
	if ( ! empty( $query->query_vars['tax_query'] ) ) {
		$have_category_query = false;

		foreach ( $query->query_vars['tax_query'] as $tax_query ) {
			if ( is_array( $tax_query ) && array_key_exists( 'taxonomy', $tax_query )
					&& 'product_cat' === $tax_query['taxonomy'] ) {
				$have_category_query = true;
				break;
			}
		}

		if ( ! $have_category_query ) {
			$category_taxonomy_query = array(
				'taxonomy' => 'product_cat',
				'field'    => 'slug',
				'terms'    => $query->query_vars['product_cat']
			);

			array_unshift( $query->query_vars['tax_query'], $category_taxonomy_query );
		}
	}

	/**
	 * Fix issue with product category page appearing to be the Shop page
	 */
	$query->is_post_type_archive = false;

	return $query;
}

add_filter( 'pre_get_posts', 'fix_product_filter_query', PHP_INT_MAX, 1 );

function afcc_encode_reg_symbol( $title, $post_id = null ) {
	if ( is_admin() ) {
		return $title;
	}

	$title = str_replace( array( '&reg;', '®' ), '<sup class="reg">&reg;</sup>', $title);

	return $title;
}

add_filter( 'the_title', 'afcc_encode_reg_symbol', 10, 2 );

/**
 * Set the default in stock filter
 */
function set_default_availability_filter( $query ) {
	if ( is_admin() || ! ( ! $query->is_single() && $query->is_main_query()
				&& ( ! is_null( $query->get( 'product_cat', null ) ) || 'product' === $query->get( 'post_type' ) ) )
			|| isset( $_GET['instock_products'] ) ) {
		return $query;
	}

	$have_availability_filter = false;

	foreach ( $query->query_vars['meta_query'] as $meta_query ) {
		if ( is_array( $meta_query ) && array_key_exists( 'key', $meta_query )
				&& '_stock_status' === $meta_query['key'] ) {
			$have_availability_filter = true;
			break;
		}
	}

	if ( ! $have_availability_filter ) {
		$query->query_vars['meta_query'][] = array(
			'key'     => '_stock_status',
			'value'   => 'instock',
			'compare' => '=',
		);
	}

	return $query;
}

add_filter( 'pre_get_posts', 'set_default_availability_filter', PHP_INT_MAX, 1 );

/*
 * Function to grab all brands associated with a category
 */
function get_category_brands($category_obj) {
	$brands = array();
	$brand_names = array();
	$args = array(
		'post_type' => 'product',
		'posts_per_page' => -1,
		'product_cat' => $category_obj->slug,
		'orderby' => 'rand' );

	$loop = new WP_Query( $args );
	while ( $loop->have_posts() ) :
		$loop->the_post();
		global $product;
		$brand_terms = get_the_terms($product->id, 'pa_brand');
		if( $brand_terms !== false ) {
			foreach($brand_terms as $bterm) {
				if(!in_array($bterm->name, $brand_names)) {
					$brand_names[] = $bterm->name;
					$brands[] = $bterm;
				}
			}
		}
	endwhile;
	wp_reset_query();

	return $brands;
}

function loginpage_custom_link() {
	return get_bloginfo( 'url' );
}

add_filter( 'login_headerurl', 'loginpage_custom_link' );


add_filter('woocommerce_get_availability', 'availability_filter_func');

function availability_filter_func($arr)
{
	$arr['availability'] = str_ireplace('Out of Stock', 'Special Order', $arr['availability']);
	return $arr;
}

/**
 * Include the Product Filter Navigation functionality
 */
require get_template_directory() . '/inc/product-filter-navigation.php';

/**
 * Include MobileMercury API Data Filters and Actions
 */
require get_template_directory() . '/inc/mm-api-data-hooks.php';

/**
 * Add Custom Fields to Contact Form 7
 */
require get_template_directory() . '/inc/cf7-form-fields.php';

/**
 * Setup custom breadcrumbs
 */
require get_template_directory() . '/inc/breadcrumbs.php';

/**
 * Implement the Custom Header feature.
 */
//require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';



function _remove_script_version( $src ) {
	$parts = explode( '?ver', $src );
	return $parts[0];
}
add_filter( 'script_loader_src', '_remove_script_version', 15, 1 );
add_filter( 'style_loader_src', '_remove_script_version', 15, 1 );

//Disable Updraft and Wordfence on localhost
add_action( 'init', function () {
    if ( !is_admin() && ( strpos( get_site_url(), 'localhost' ) > -1 ) ) {
        include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

        $updraft = WP_PLUGIN_DIR . '/updraftplus/updraftplus.php';
        if ( file_exists( $updraft ) ) deactivate_plugins( [ $updraft ] );

        $wordfence = WP_PLUGIN_DIR . '/wordfence/wordfence.php';
        if ( file_exists( $wordfence ) ) deactivate_plugins( [ $wordfence ] );
    }
} );

