<?php
/******************************************************************************
 *              ___   _   _   _ __     __ _   _ __    ___    ___              *
 *             / __| | | | | | '_ \   / _` | | '_ \  / __|  / _ \             *
 *             \__ \ | |_| | | | | | | (_| | | |_) | \__ \ |  __/             *
 *             |___/  \__, | |_| |_|  \__,_| | .__/  |___/  \___|             *
 *                     __/ |                 | |                              *
 *                    |___/                  |_|                              *
 *                                                                            *
 *                    m a r k e t i n g  s o l u t i o n s                    *
 ******************************************************************************/

/**
 * Contact Form 7 Tag Generator for Store Locations field
 *
 * @author     Joseph Leedy <jleedy@synapseresults.com>
 * @category   AFCC
 * @package    AFCC_Theme
 * @copyright  Copyright 2015 Synapse Marketing Solutions (http://synapseresults.com/)
 * @license    Proprietary
 */
?>
<div id="afcc-wpcf7-tg-pane-store-locations" class="hidden">
	<form>
		<table>
			<tr>
				<td>
					<label>
						<input type="checkbox" name="required" />&nbsp;<?php echo esc_html( __( 'Required field?', 'afcc' ) ); ?>
					</label>
				</td>
			</tr>
			<tr>
				<td>
					<label>
						<?php echo esc_html( __( 'Name', 'afcc' ) ); ?>
						<br />
						<input type="text" name="name" class="tg-name oneline" />
					</label>
				</td>
			</tr>
		</table>

		<table>
			<tr>
				<td>
					<label>
						<code>id</code> (<?php echo esc_html( __( 'optional', 'afcc' ) ); ?>)
						<br />
						<input type="text" name="id" class="idvalue oneline option" />
					</label>
				</td>

				<td>
					<label>
						<code>class</code> (<?php echo esc_html( __( 'optional', 'afcc' ) ); ?>)
						<br />
						<input type="text" name="class" class="classvalue oneline option" />
					</label>
				</td>
			</tr>
			<tr>
				<td>
					<label>
						<code>limit</code> (<?php echo esc_html( __( 'optional', 'afcc' ) ); ?>)
						<br />
						<input type="number" name="limit" class="oneline option" min="-1" max="100" />
					</label>
				</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td colspan="2">
					<label>
						<input type="checkbox" name="multiple" class="option" />&nbsp;<?php echo esc_html( __( 'Allow multiple selections?', 'afcc' ) ); ?>
					</label>
					<br />
					<label>
						<input type="checkbox" name="include_blank" class="option" />&nbsp;<?php echo esc_html( __( 'Add a blank item as the first option?', 'afcc' ) ); ?>
					</label>
				</td>
			</tr>
		</table>

		<div class="tg-tag">
			<label>
				<?php echo esc_html( __( 'Copy this code and paste it into the Form field on the left.', 'afcc' ) ); ?>
				<br />
				<input type="text" name="store-locations" class="tag wp-ui-text-highlight code" readonly="readonly" onfocus="this.select()" />
			</label>
		</div>

		<div class="tg-mail-tag">
			<label>
				<?php echo esc_html( __( 'Copy this code and paste it into the Mail fields below.', 'afcc' ) ); ?>
				<br />
				<input type="text" class="mail-tag wp-ui-text-highlight code" readonly="readonly" onfocus="this.select()" />
			</label>
		</div>
	</form>
</div>
