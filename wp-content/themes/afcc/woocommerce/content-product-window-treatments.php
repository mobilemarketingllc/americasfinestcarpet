<?php
/**
 * The template for displaying product content within loops.
 *
 * Override this template by copying it to yourtheme/woocommerce/content-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $woocommerce_loop;

// Store loop count we're currently on
if ( empty( $woocommerce_loop['loop'] ) )
	$woocommerce_loop['loop'] = 0;

// Store column count for displaying the grid
if ( empty( $woocommerce_loop['columns'] ) )
	$woocommerce_loop['columns'] = apply_filters( 'loop_shop_columns', 4 );

// Increase loop count
$woocommerce_loop['loop']++;

// Extra post classes
$classes = array();
if ( 0 == ( $woocommerce_loop['loop'] - 1 ) % $woocommerce_loop['columns'] || 1 == $woocommerce_loop['columns'] )
	$classes[] = 'first';
if ( 0 == $woocommerce_loop['loop'] % $woocommerce_loop['columns'] )
	$classes[] = 'last';

$attrs = array('tone', 'color', 'title', 'brand');

do_action( 'woocommerce_before_subcategory', $category );
$thumbnail_id = get_woocommerce_term_meta( $category->term_id, 'thumbnail_id', true );
if( $thumbnail_id !== false ):
	$image = wp_get_attachment_url( $thumbnail_id );
else :
	$image = false;
endif;
?>


<li class="center product wt">
	<a href="<?php echo get_term_link( $category->slug, 'product_cat' ); ?>">
		<?php if( $image !== false ): ?>
		<img src="<?php echo $image ?>" />
		<?php endif; ?>
		<h3><?php echo $category->name; ?></h3>
	</a>
	<?php if( !empty($category->description) ) : ?>
	<p><?php echo $category->description ?></p>
	<?php endif; ?>
	<!-- <a href="<?php //echo get_permalink(1058) ?>" class="cta"><?php //_e( 'Get Coupon', 'afcc' ) ?></a>
	<div class="clear"></div> -->
    <p style="padding-top: 40px;"><a href="<?php echo get_term_link( $category->slug, 'product_cat' ); ?><?php //echo get_permalink_with_requested_product_category( 6821, $category->term_id ) ?>" class="center detail-link"><?php _e( 'Get Details', 'afcc' ) ?></a></p>
</li>

