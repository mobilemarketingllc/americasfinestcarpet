<?php
/**
 * The Template for displaying products in a product category. Simply includes the archive template.
 *
 * Override this template by copying it to yourtheme/woocommerce/taxonomy-product_cat.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

global $wp_query;
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}




$term =	$wp_query->queried_object;





if( !empty($term) && $term->parent == 1288 ){
	wc_get_template( 'archive-product-window-treatments-sub.php' );
} else {
	wc_get_template( 'archive-product.php' );
}
