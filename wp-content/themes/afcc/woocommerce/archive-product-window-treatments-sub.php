<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive.
 *
 * Override this template by copying it to yourtheme/woocommerce/archive-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */
global $wp_query, $post;
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

// functions.php
function my_product_count() {
	$cat = get_queried_object()->term_id;
   	$catid = get_term($cat, 'product_cat' );
    $product_count = 0;

    // Get count for all product categories
    foreach (get_terms($cat, 'product_cat') as $term)
        $product_count = $term->count;

    return $product_count;
}

$bgImg = getPageHeaderBg();

get_header( 'shop' ); ?>


<section id="slider">
	<div class="row wide">
		<div class="twelve columns np">
			<div id="home-slide" class="wt-main-slide">
				<?php
					$cat_obj = $wp_query->get_queried_object();
					$args = array(
						'post_type'       => 'product',
						'orderby'         => 'menu_order',
						'order'           => 'ASC',
						'posts_per_page'  => -1,
						'product_cat'     => $cat_obj->slug
					);

					$the_query = new WP_Query( $args );

					// The Loop
					if ( $the_query->have_posts() ) {
						while ( $the_query->have_posts() ) { $the_query->the_post();
							$featured = get_field('featured_product');
							$bgImg = get_field('slider_bg');
							if ( $featured == 'yes' ) {

				?>

					<div class="slide centered-area sub-cat-slide" style="background-image: url(<?php echo $bgImg; ?>);">
						<div class="content">
							<p class="headline nm"><?php the_title(); ?></p>
							<p class="nm"><?php echo $cat_obj->name; ?></p>
							<?php
								$terms = get_the_terms( $post->ID, 'pa_brand' );
							    if( !empty($terms) ) {
									foreach ($terms as $term) {
									    echo '<p class="small-copy brand-space small nm">' . $term->name . '</p>';
									    break;
									}
								}
							?>
							<a href="<?php echo get_permalink_with_requested_product( 1429, $post->ID ); ?>" class="cta"><?php _e( 'Request a Measurement', 'afcc' ) ?></a>
						</div>
					</div>
				<?php
						}
					}

					} else { ?>
					<p>Sorry there were no products to be found.</p>
					<?php }
					/* Restore original Post Data */
					wp_reset_postdata();
				?>
			</div>
		</div>
	</div>
</section>
<section id="breadcrumbs">
	<div class="row">
		<div class="twelve columns">
			<?php
				/**
				 * woocommerce_before_main_content hook
				 *
				 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
				 * @hooked woocommerce_breadcrumb - 20
				 */
				do_action( 'woocommerce_before_main_content' );
			?>
		</div>
	</div>
</section>



		<?php if ( have_posts() ) : ?>
			<section id="contain-sub-cat-prods">
				<div class="row">
					<div class="twelve columns center little-para">
						<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
								<h1 class="page-title h1-style"><?php woocommerce_page_title(); ?> in San Diego, CA</h1>
								<?php echo category_description( ); ?>

						<?php endif; ?>
					</div>
				</div>

			<div class="row">
				<div class="twelve columns">
					<?php woocommerce_product_loop_start(); ?>

						<?php woocommerce_product_subcategories(); ?>

						<?php while ( have_posts() ) : the_post(); ?>

							<?php wc_get_template_part( 'content', 'product-window-treatments-child' ); ?>

						<?php endwhile; // end of the loop. ?>

					<?php woocommerce_product_loop_end(); ?>

					<?php
						/**
						 * woocommerce_after_shop_loop hook
						 *
						 * @hooked woocommerce_pagination - 10
						 */
						do_action( 'woocommerce_after_shop_loop' );
					?>

				<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

					<?php wc_get_template( 'loop/no-products-found.php' ); ?>

				<?php endif; ?>
				</div>
			</div>

	<?php
		/**
		 * woocommerce_after_main_content hook
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
		do_action( 'woocommerce_after_main_content' );
	?>

	<?php
		/**
		 * woocommerce_sidebar hook
		 *
		 * @hooked woocommerce_get_sidebar - 10
		 */
		do_action( 'woocommerce_sidebar' );
	?>
</section>

<?php get_footer( 'shop' ); ?>
