<?php
/**
 * Loop Price
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;
$curCat = get_active_product_category();
if( is_object($curCat) ) {	
	$displayPrice = get_field('show_pricing_on_cat', 'product_cat_'.$curCat->term_id);
} else {
	$displayPrice = 'no';
}
?>

<?php if ( $displayPrice === 'yes' && $product->is_in_stock() ) : ?>
	<?php if ( $price_html = $product->get_price_html() ) : ?>
		<span class="price"><span class="bold"><?php echo $price_html; ?></span> sq/ft</span>
		<?php if( $product->is_in_stock() ): ?>
			<p class="small nm installed"><?php _e('Installed')?></p>
		<?php endif; ?>
	<?php endif; ?>
<?php endif; ?>
