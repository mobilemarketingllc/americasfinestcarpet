<?php
/**
 * Loop Get Coupon
 *
 * @author 	Synapse Marketing Solutions <webtech@synapseresults.com>
 * @package	AFCC
 */

if ( ! defined( 'ABSPATH' ) ) :
	exit; // Exit if accessed directly
endif;
?>
<a href="<?php echo get_permalink( 1058 ); ?>" class="cta get-coupon-button"><?php _e( 'Get Coupon', 'afcc' ) ?></a>
