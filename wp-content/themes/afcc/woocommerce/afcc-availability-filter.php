<?php
	$curCat = get_active_product_category();
	if( is_product_category() || is_product_tag() || is_product_taxonomy() ) :
		if ( !is_tax( 'product_cat', 'window-treatments') && ( !is_object( $curCat )
				|| ! property_exists( $curCat, 'parent' ) || $curCat->parent !== 1288 ) ) :
?>
<p class="ie8-text">
    Click and drag to toggle
</p>
<!-- <div class="onoffswitch">
	<input type="checkbox" class="onoffswitch-checkbox <?php ?>" id="myonoffswitch" <?php //checked('in', (empty($_GET['instock_products']) ? 'in' : $_GET['instock_products']  ) ); ?> />
	<label class="onoffswitch-label" for="myonoffswitch">
		<span class="onoffswitch-inner"></span>
		<span class="onoffswitch-switch"></span>
	</label>
</div> -->
<?php 
	endif;
endif; 
?>
