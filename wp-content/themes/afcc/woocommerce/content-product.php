<?php
/**
 * The template for displaying product content within loops.
 *
 * Override this template by copying it to yourtheme/woocommerce/content-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product, $woocommerce_loop, $post;

// Store loop count we're currently on
if ( empty( $woocommerce_loop['loop'] ) )
	$woocommerce_loop['loop'] = 0;

// Store column count for displaying the grid
if ( empty( $woocommerce_loop['columns'] ) )
	$woocommerce_loop['columns'] = apply_filters( 'loop_shop_columns', 4 );

// Ensure visibility
if ( ! $product || ! $product->is_visible() )
	return;

// Increase loop count
$woocommerce_loop['loop']++;

// Extra post classes
$classes = array();
if ( 0 == ( $woocommerce_loop['loop'] - 1 ) % $woocommerce_loop['columns'] || 1 == $woocommerce_loop['columns'] )
	$classes[] = 'first';
if ( 0 == $woocommerce_loop['loop'] % $woocommerce_loop['columns'] )
	$classes[] = 'last';

$attrs = array( 'color', 'title', 'brand');


?>
<li <?php post_class( $classes ); ?>>
	<?php do_action( 'woocommerce_before_shop_loop_item' ); ?>
	<div class="col-9">
		<a href="<?php the_permalink(); ?>" class="center">

			<div class="col-4" style="position: relative;">
				<?php
					/**
					 * woocommerce_before_shop_loop_item_title hook
					 *
					 * @hooked woocommerce_show_product_loop_sale_flash - 10
					 * @hooked woocommerce_template_loop_product_thumbnail - 10
					 */
					//do_action( 'woocommerce_before_shop_loop_item_title' );
				?>
<?php
 global $product;
 if ( $product->is_in_stock() ) { ?>
	<div class="in-stock" style="font-size: 90%; position: absolute; top: 0; left: 0; padding: 5px 15px;; background-color:#dc4439; color:  #fff;">In-Stock</div>
 <?php } ?>
<?php if ( get_field('swatch_image_url') ) { 

$imageUrl= get_field('swatch_image_url');
		
		if (strpos($imageUrl, 'http://www.usfloorsllc.com') !== false || strpos($imageUrl, 'http://s7d4.scene7.com') !== false ) {
			$imageUrl = 'https://mobilem.liquifire.com/mobilem?source=url[' . $imageUrl . ']&scale=size[550x550]&sink';
			
		}
?>

<img width="300" height="300" src="<?php echo $imageUrl; ?>" class="attachment-shop_single size-shop_single wp-post-image" alt="<?php the_title(); ?>" title="<?php the_title(); ?>" srcset="<?php echo $imageUrl; ?> 300w">

<?php } elseif ( has_post_thumbnail() ) {

			$image_title = esc_attr( get_the_title( get_post_thumbnail_id() ) );
			$image_link  = wp_get_attachment_url( get_post_thumbnail_id() );
			$image       = get_the_post_thumbnail( $post->ID, apply_filters( 'single_product_large_thumbnail_size', 'thumbnail' ), array(
				'title' => $image_title
				) );

			$attachment_count = count( $product->get_gallery_attachment_ids() );

			if ( $attachment_count > 0 ) {
				$gallery = '[product-gallery]';
			} else {
				$gallery = '';
			}

			//echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<a href="%s" itemprop="image" class="woocommerce-main-image zoom" title="%s" data-rel="prettyPhoto' . $gallery . '">%s</a>', $image_link, $image_title, $image ), $post->ID );
			echo $image;

		

		 } else {

			echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<img src="%s" alt="%s" />', wc_placeholder_img_src(), __( 'Placeholder', 'woocommerce' ) ), $post->ID );

		}
	?>



			</div>

			<div class="col-8 mr">
				<div class="prod-attrs">
					<?php
						foreach ($attrs as $attr) {
							if( $attr === 'title' ) {
								 echo '<h3 class="product-title">'.get_the_title().'</h3>';
							} else {
								$terms = get_the_terms( $post->ID, 'pa_'.$attr );
							    if( !empty($terms) ) {
									foreach ($terms as $term) {
									    echo '<p class="'. $attr .'-space small nm">' . $term->name . '</p>';
									    break;
									}
								}
							}

						}
					?>
				</div>
				<hr class="small-line">
				<?php
					/**
					 * woocommerce_after_shop_loop_item_title hook
					 *
					 * @hooked woocommerce_template_loop_rating - 5
					 * @hooked woocommerce_template_loop_price - 10
					 */
					do_action( 'woocommerce_after_shop_loop_item_title' );
				?>
			</div>
		</a>
	</div>
	<div class="col-3">
		<?php

			/**
			 * woocommerce_after_shop_loop_item hook
			 *
			 * @hooked woocommerce_template_loop_add_to_cart - 10
			 */
			do_action( 'woocommerce_after_shop_loop_item' );

		?>
		<a href="<?php echo get_permalink_with_requested_product( 1060, $post->ID ); ?>" class="center req-info"><?php _e( 'Request Info', 'afcc' ) ?></a>
	</div>
</li>