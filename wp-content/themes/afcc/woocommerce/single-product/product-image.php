<?php
/**
 * Single Product Image
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.14
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post, $woocommerce, $product;

?>
<div class="images">


<?php if ( get_field('swatch_image_url') ) { 
		$imageUrl= get_field('swatch_image_url');
		
		if (strpos($imageUrl, 'http://www.usfloorsllc.com') !== false || strpos($imageUrl, 'http://s7d4.scene7.com') !== false ) {
			$imageUrl = 'https://mobilem.liquifire.com/mobilem?source=url[' . $imageUrl . ']&scale=size[550x550]&sink';
			
		}
?>



<img width="300" height="300" src="<?php echo $imageUrl; ?>" class="attachment-shop_single size-shop_single wp-post-image" alt="<?php the_title(); ?>" title="<?php the_title(); ?>" srcset="<?php echo $imageUrl; ?> 300w">

<?php } elseif ( has_post_thumbnail() ) {

			$image_title = esc_attr( get_the_title( get_post_thumbnail_id() ) );
			$image_link  = wp_get_attachment_url( get_post_thumbnail_id() );
			$image       = get_the_post_thumbnail( $post->ID, apply_filters( 'single_product_large_thumbnail_size', 'shop_single' ), array(
				'title' => $image_title
				) );

			$attachment_count = count( $product->get_gallery_attachment_ids() );

			if ( $attachment_count > 0 ) {
				$gallery = '[product-gallery]';
			} else {
				$gallery = '';
			}

			//echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<a href="%s" itemprop="image" class="woocommerce-main-image zoom" title="%s" data-rel="prettyPhoto' . $gallery . '">%s</a>', $image_link, $image_title, $image ), $post->ID );
			echo $image;
	

		

		 } else {

			echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<img src="%s" alt="%s" />', wc_placeholder_img_src(), __( 'Placeholder', 'woocommerce' ) ), $post->ID );

		}
	?>

	<?php //do_action( 'woocommerce_product_thumbnails' ); ?>

</div>
