<?php
/**
 * Single Product title
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
global $post, $woocommerce, $product;
$attrs = array('color');
echo '<div class="dib center">';
foreach ($attrs as $attr) {
	$terms = get_the_terms( $post->ID, 'pa_'.$attr );
    if( !empty($terms) ) {
		foreach ($terms as $term) {
		    echo '<p class="h1-style">' . $term->name . '</p>';
		    break;
		}
	}
}
	

?>

<h1 itemprop="name" class="product_title entry-title caps"><?php the_title(); ?></h1>
</div>
