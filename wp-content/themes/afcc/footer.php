<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package afcc
 */
?>

	</div><!-- #content -->
	
	<section id="footer-ctas">
		<div class="row">
			<?php if( have_rows('top_tier', 'option') ):
				// loop through the rows of data
			    while ( have_rows('top_tier', 'option') ) : the_row();
			        $tierHead  = get_sub_field('headline');
			        if( strpos($tierHead, '[') !== false ) {
			        	$tierHead = apply_filters('the_content', $tierHead);
					}
			        $tierSubHead  = get_sub_field('sub_headline');
			        $tierLink  = get_sub_field('link');
			        $tierType  = get_sub_field('icon_type');
			?>
				<div class="four columns">
					<a href="<?php echo $tierLink; ?>" class="large-cta <?php echo $tierType; ?>">
						<?php echo $tierHead; ?>
						<span class="small"><?php echo $tierSubHead; ?></span>
					</a>
				</div>
			<?php
				endwhile;
				endif;
			?>
		</div>
	</section>
	<footer>
		<div class="row added">
			<div class="two columns">
				<ul>
					<li class="caps">Products</li>
					<?php
						$args = array(
						'taxonomy'     => 'product_cat',
						'orderby'      => 'name',
						'show_count'   => 0,
						'pad_counts'   => 0,
						'hierarchical' => 1,
						'title_li'     => '',
						'hide_empty'   => 0
						);
					?>
					<?php $all_categories = get_categories( $args );
						//print_r($all_categories);
						foreach ($all_categories as $cat) {
						    //print_r($cat);
						    if($cat->category_parent == 0) {
						        $category_id = $cat->term_id;
									echo '<li><a href="'. get_term_link($cat->slug, 'product_cat') .'">'. $cat->name .'</a></li>';
							}
						}
					?> 
				</ul>
			</div>
			<div class="three columns">
				<ul>
					 <?php dynamic_sidebar( 'Footer Block 1' ); ?>
				</ul>
			</div>
			<div class="three columns">
				<ul>
					<?php dynamic_sidebar( 'Footer Block 2' ); ?>
				</ul>
			</div>
			<div class="two columns">
				<ul>
					<?php dynamic_sidebar( 'Footer Block 3' ); ?>
				</ul>
			</div>
			<div class="two columns">
				<ul>
					<?php dynamic_sidebar( 'Footer Block 4' ); ?>
				</ul>
			</div>
		</div>
		<div class="row">
			<div class="nine columns">
				<p class="nm small fl"><?php dynamic_sidebar( 'Copyright Links' ); ?></p>
			</div>
			<div class="three columns fr">
				<ul class="fr social-icons nm white">
					<?php if( have_rows('social_icons', 'option') ):
						// loop through the rows of data
					    while ( have_rows('social_icons', 'option') ) : the_row();
					        $sTitle  = get_sub_field('social_platform_name');
					        $sLink  = get_sub_field('social_platform_link');
					?>
						<li class="<?php echo $sTitle; ?>"><a href="<?php echo $sLink; ?>" target="_blank"><span class="icon-<?php echo $sTitle; ?>"></span></a></li>
					<?php
						endwhile;
						endif;
					?>
				</ul>
			</div>
		</div>
	</footer>
	<section id="fixed-footer">
		<div class="row">
			<?php if( have_rows('fixed_footer', 'option') ):
				// loop through the rows of data
			    while ( have_rows('fixed_footer', 'option') ) : the_row();
			        $fixedHead  = get_sub_field('link_text');
			        $fixedLink  = get_sub_field('link_location');
			        $fixedType  = get_sub_field('link_icon');
			?>
				<div class="four columns">
					<a class="large-cta <?php echo $fixedType; ?>" href="<?php echo $fixedLink; ?>"><?php echo $fixedHead; ?></a>
				</div>
			<?php
				endwhile;
				endif;
			?>
		</div>
	</section>
</div><!-- #page -->

<?php wp_footer(); ?>
<?php if ($_SERVER['HTTP_HOST'] == 'americasfinestcarpet.com.synapseresults.com') : ?>
<script type='text/javascript'>
	(function (d, t) {
		var bh = d.createElement(t), s = d.getElementsByTagName(t)[0];
		bh.type = 'text/javascript';
		bh.src = '//www.bugherd.com/sidebarv2.js?apikey=wicr4zeikzddsdle2xhb0w';
		s.parentNode.insertBefore(bh, s);
	})(document, 'script');
</script>
<?php endif; ?>

</body>
</html>