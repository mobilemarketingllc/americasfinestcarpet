// detect useragent + platform
var b = document.documentElement;
b.className = b.className.replace('no-js', 'js');
b.setAttribute("data-useragent", navigator.userAgent.toLowerCase());
b.setAttribute("data-platform", navigator.platform.toLowerCase());

var $html = $('html');
var ua = ($html.data('useragent')),
pf = ($html.data('platform'));

function is(string) {
	return ua.indexOf(string) > -1
}

var browser = {
	isIE             : is('msie') || is('trident/7.0'),
	isIE7            : is('msie 7.0'),
	isIE8            : is('msie 8.0'),
	isIE9            : is('msie 9.0'),
	isIE10           : is('msie 10.0'),
	isIE11           : is('rv:11') && is('trident/7.0'),
	isChrome         : is('chrome'),
	isSafari         : is('safari') && !is('chrome'),
	isFirefox        : is('firefox'),
	isAndroidChrome  : is('android') && is('chrome'),
	isAndroidStandard : is('android') && !is('chrome'),
	isWin7           : is('windows nt 6.1'),
	isWin8           : is('windows nt 6.2'),
	isWin            : pf.indexOf('win32') > -1,
	isWebkit         : is('webkit'),
	isIPad           : is('ipad'),
	isIPhone         : is('iphone'),
	isAndroid        : is('android')
}


for (var title in browser) {
	var helperClass = title.slice(2).toLowerCase();
	if (browser[title]) {
		$html.addClass(helperClass);
	}
}

var hasBeenTrigged = false;
equalheight = function(container){

	var currentTallest = 0,
	     currentRowStart = 0,
	     rowDivs = new Array(),
	     $el,
	     topPosition = 0;
	 $(container).each(function() {
	
		$el = $(this);
		$($el).height('auto')
		topPostion = $el.position().top;
		
		if (currentRowStart != topPostion) {
			for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
				rowDivs[currentDiv].height(currentTallest);
			 }
			 rowDivs.length = 0; // empty the array
			 currentRowStart = topPostion;
			 currentTallest = $el.height();
			 rowDivs.push($el);
		} else {
			rowDivs.push($el);
			currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
		}
		for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
			rowDivs[currentDiv].height(currentTallest);
		}
	});
}



// Get Max Height
function getMaxHeight(el){
	var maxH;
	if (el.length > 0) {
		el.each(function() {
		  maxH = maxH > $(this).outerHeight(true) ? maxH : $(this).outerHeight(true);
		});

		el.css('height', maxH);
	}
}

function posMainNav(){
	if( $(window).width() > 768 ) {
		var tBarH = $('#top-bar').outerHeight(true);
		$('#main-navigation').animate({'top':tBarH}, function(){
			$('#main-navigation').fadeIn();
		});
	}
	$('#primary_nav_wrap, #slider').animate({'left':0}, 1000, function(){
		$('#product-slider-container').animate({'opacity':1});
	});
}

function mobileNavInit(){
	var $menu = $("#primary_nav_wrap").clone();
	$menu.attr( "id", "primary_nav_wrap_mobile" );
	$menu.mmenu({
		classes : "mm-light",
		counters : false,
		searchfield : false,
		clone : true,
		header: {
			add: false,
			update: false
		},
		dragOpen: {
		   open: false
		}
	});
}

function initMembersSlider() {
	$('.team-members').slick({
		dots: false,
		infinite: true,
		speed: 300,
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: true
	});

}

function couponForm() {
	var couponH = $('.contain-coupon-form').innerHeight()/2;
	$('.contain-coupon-form').animate({'margin-bottom':'-'+couponH}, function(){
		$(this).animate({'opacity':1}, 10);
	});
}

function selectBoxStyle() {
	// Wrap select boxes in styled container
	$('select:not([multiple])').each(function() {
		var el = $(this),
			opt = el.find('option:selected');

		el.wrap('<div class="select-wrap"></div>');
		el.before('<span>'+opt.text()+'</span>');
	}).change(function() {
		var el = $(this),
		opt = el.children('option:selected').text();

		el.parents('.select-wrap').removeClass('placeholder');
		el.parent().find('span').fadeOut(200, function() {
			el.parent().find('span').text(opt).addClass('changed').fadeIn(200);
		});
	});
	// Wrap checkbox inputs in styled container
	$('.form-container-all input:checkbox, .contain-coupon-form input:checkbox').each(function(){
		var el = $(this);
		el.wrap('<div class="check-wrap"></div>');
		if (el.is(':checked')) {
			el.parent().addClass('checked');
		} else {
			el.attr("checked", false);
		}

	// Give parent container checked class
	});
	$('.form-container-all .check-wrap, .contain-coupon-form .check-wrap').click(function() {
		var el = $(this);
		console.log($(this).attr("id"));
		if (el.hasClass('checked')) {
			el.removeClass('checked');
			el.find('input:checkbox').attr("checked", true);
		} else {
			el.addClass('checked');
			el.find('input:checkbox').attr("checked", false);
		}
	});
}

function setMaxHeightSingle(el){
	getMaxHeight( $('.customH') );
}

function setMaxHeight(el){
	getMaxHeight( $('.prod-attrs') );
	getMaxHeight( $('#vid-mixer .mix.four.columns') );
}
// Run functions after a series of events finishes
var waitForFinalEvent = (function() {
	var timers = {};

	return function(callback, ms, uniqueId) {
		if (!uniqueId) {
			uniqueId = "Don't call this twice without a uniqueId";
		}

		if (timers[uniqueId]) {
			clearTimeout(timers[uniqueId]);
		}

		timers[uniqueId] = setTimeout(callback, ms);
	};
})();

// Position Main Slider Content
function slideContent(){
	$('#home-slide .slide .content, .centered-area .content').each(function(){
		var slideContentW = $(this).innerWidth()/2;
		$(this).animate({'margin-left':'-'+slideContentW}, function(){
			$(this).animate({'opacity':1})
		});
	});
}
// Position Main Slider Content
function gridContent(){
	$('.contain-white, .contain-black').each(function(){
		var gridContentW = $(this).innerWidth()/2;
		var gridContentH = $(this).innerHeight()/2;
		$(this).animate({'margin-left':'-'+gridContentW, 'margin-top':'-'+gridContentH}, function(){
			$(this).animate({'opacity':1})
		});
	});
}
function gridContentSub(){
	$('.contain-white-sub').each(function(){
		var gridContentSubW = $(this).innerWidth()/2;
		var gridContentSubH = $(this).parent().find('img').innerHeight()/2;
		$(this).animate({'margin-left':'-'+gridContentSubW, 'margin-top':'-'+gridContentSubH}, function(){
			$(this).animate({'opacity':1})
		});
	});
}

function checkbox_error_fix() {
	$( '.wpcf7' ).on( 'invalid.wpcf7', function() {
		var $self, $checkbox_error, $terms_container;

		$self = $( this );
		$terms_container = $self.find( '.special-case.terms' );

		if ( $terms_container.length === 0 ) {
			$terms_container = $self.find( '.terms-accept' );
		}

		if ( $terms_container.length === 0 ) {
			return;
		}

		$checkbox_error = $terms_container.find( '.wpcf7-not-valid-tip' );

		if ( $checkbox_error.length > 0 ) {
			$checkbox_error.detach().prependTo( $terms_container );
		}
	} );

	$( '.wpcf7 button.cta' ).removeAttr( 'disabled' );
}

function update_submit_button() {
	// Change CF7 submit to show submission in process
	$('.wpcf7').on('submit', function(){
		var button = $(this).find('button.cta');
		button.data('old-name', button.html());
		button.html('Please wait...').prop('disabled', true);
	});

	$( '.wpcf7' ).on( 'invalid.wpcf7', function() {
		var button = $(this).find('button.cta');
		button.html(button.data('old-name')).prop('disabled', false);
	});
}

// Set Max Height on Progressive Load
$('body').on('paginateEnd', '#sb-infinite-scroll-load-more', function(event) {
	setMaxHeight();
	gridContentSub();
});

$('#grid').on('show_grid', function(){
	setMaxHeight();
});

$(document).ready(function() {
	$('input, textarea').placeholder();
	$('#vid-mixer').mixItUp();
	$('.video-filter-nav li a').on('click', function(e){
		e.preventDefault();
		$('.video-filter-nav li a').removeClass('active');
		$(this).addClass('active');
	});

	$('#vid-mix-mobile').change(function(){
		var selected = $(this).val();
		$('[data-filter="' + selected + '"]').click();
	});

	$('a.contain-videos').on('click', function(event) {
		event.preventDefault();
		$.fancybox({
			'type' : 'iframe',
			// hide the related video suggestions and autoplay the video
			'href' : this.href.replace(new RegExp('watch\\?v=', 'i'), 'embed/') + '?rel=0&autoplay=1',
			'overlayShow' : true,
			'centerOnScroll' : true,
			'fitToView' : false,
			'speedIn' : 100,
			'speedOut' : 50,
			'width' : 640,
			'fixed' : true,
			'height' : 480
		});
	});

	$(".inspiration-gal").fancybox({
		helpers : {
			title: {
				type: 'outside',
				position: 'top'
			}
		},
		afterLoad: function() {
			var el = $(this.element[0]);
			var prodBrand = el.data('product-brand');
			this.title = this.title +'<br><span class="brand">'+prodBrand+'</span>';
		},
		beforeShow  : function(){
			$('.fancybox-inner').swipe('destroy');
		},
		afterShow   : function() {
			if ( Modernizr.touch ) {
				$('.fancybox-inner').swipe({
					swipe : function(e, dir, dis, dur, finC, finD) {
						if ( dir == 'left' ) {
							$.fancybox.next();

						} else if ( dir == 'right' ) {
							$.fancybox.prev();
						}
					}
				});
			}
		},
		prevEffect	: 'elastic',
		nextEffect	: 'elastic',
		padding     : 0,
		openEffect  : 'elastic',
		closeEffect : 'elastic',
		nextEffect  : 'elastic',
		prevEffect  : 'elastic'
	});

	// Hide and Show for FAQs & Inventors
	$('.faq-title').on('click', function() {
		var el = $(this);
		var menu = el.next('.faq-answer');

		if ( !el.hasClass('open') ) {
			el.addClass('open');
			menu.stop(true, true).slideDown();

		} else {
			el.removeClass('open');
			menu.stop(true, true).slideUp();
		}
	});

	var prodNavLeft = $('#product-filter-navigation-left').html();
	var prodNavRight = $('#product-filter-navigation-right').html();
	var combined = prodNavLeft + prodNavRight;
	if( !isNaN(combined) && $(window).width() <= 640 ){
		$('#product-filter-navigation-left, #product-filter-navigation-right').remove();
		$('<div class="contain-arrows-only-prods">'+combined+'</div>').insertBefore('#product-filter-navigation-container');
	}

	$('.no-link').on('click', function(e){
		e.preventDefault();
	});
	selectBoxStyle();
	//$('.req-info, .req-measurement').fancybox({
	$('.req-measurement').fancybox({
		'type': 'ajax',
		'wrapCSS': 'popup-form',
		'autoScale': false,
		'autoCenter': false,
		'ajax': {
			'dataFilter': function(data) {
				return $(data).find('#req-info-wrap').first();
			}
		},
		beforeShow   : function() {
			selectBoxStyle();
			var $form = $('div.wpcf7 > form');
			$form.length && $form.wpcf7InitForm && $form.wpcf7InitForm();
			checkbox_error_fix();
			update_submit_button();
			addMasks();
		},
		afterLoad: function(current, previous) {
			var formWrap, form, prodIDField, prodSKUField, prodID, prodSKU;
			if ( current.content.length ){
				formWrap = $(current.content[0]);
			} else {
				return;
			}
			form = formWrap.find('form');
			if( !form.length ){
				return;
			}
			prodIDField  = form.find('#product-id');
			prodSKUField = form.find('#product-sku');
			if( !prodIDField.length && !prodSKUField.length ){
				return;
			}
			prodID = $(current.element).data('product_id');
			prodSKU = $(current.element).data('product_sku');

			prodIDField.val(prodID);
			prodSKUField.val(prodSKU);
		},
		afterShow: function(){
			if( browser.isIE8 ) {
				var getH = $('.popup-form .fancybox-inner').outerHeight();
				$('.popup-form .form-container-all').css({'height':getH});
			}
		}
	});
	$('.req-info-wt').fancybox({
		'type': 'ajax',
		'autoResize': true,
		'wrapCSS': 'popup-form',
		'autoCenter': false,
		'ajax': {
			'dataFilter': function(data) {
				return $(data).find('#wt-req-info-wrap').first();
			}
		},
		beforeShow   : function() {
			selectBoxStyle();
			var $form = $('div.wpcf7 > form');
			$form.length && $form.wpcf7InitForm && $form.wpcf7InitForm();
			checkbox_error_fix();
			update_submit_button();
			addMasks();
		},
		afterLoad: function(current, previous) {
			var formWrap, form, prodIDField, prodSKUField, prodID, prodSKU;
			if ( current.content.length ){
				formWrap = $(current.content[0]);
			} else {
				return;
			}
			form = formWrap.find('form');
			if( !form.length ){
				return;
			}
			prodIDField  = form.find('#product-id');
			prodSKUField = form.find('#product-sku');
			if( !prodIDField.length && !prodSKUField.length ){
				return;
			}
			prodID = $(current.element).data('product_id');
			prodSKU = $(current.element).data('product_sku');

			prodIDField.val(prodID);
			prodSKUField.val(prodSKU);
		},
		afterShow: function(){
			if( browser.isIE8 ) {
				var getH = $('.popup-form .fancybox-inner').outerHeight();
				$('.popup-form .form-container-all').css({'height':getH});
			}
		}
	});

	if (!Modernizr.touch && !browser.isIE && $(window).width() > 769 ) {
		// Parallax
		$.stellar({
			horizontalScrolling: false
		});
	}
	// Init Mobile Nav
	mobileNavInit();
	if (Modernizr.touch){
		initMembersSlider();
	}
	if( $(window).width() < 769 ) {

		// Footer Drops
		$('footer p.nm a').on('click', function(e){
			e.preventDefault();
			if( !$(this).parent().next('ul').hasClass('open') ) {
				$(this).parent().next('ul').slideDown(function(){
					$(this).addClass('open');
				});
				$(this).addClass('active');
			} else {
				$(this).parent().next('ul').slideUp(function(){
					$(this).removeClass('open');
				});
				$(this).removeClass('active');
			}
		});
	} else {
		$("#primary_nav_wrap_mobile").trigger('close.mm');
	}

	$( "#datepicker" ).datepicker();


	// Init Home Slider
	$('#home-slide').slick({
		dots: true,
		infinite: true,
		speed: 300,
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		fade: true,
		autoplay: true,
		autoplaySpeed: 7000
	});

	// Init Home Secondary Slider
	$('#product-slider').slick({
		dots: true,
		infinite: true,
		speed: 300,
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: true,
		prevArrow : '<button type="button" class="slick-prev"></button>',
		nextArrow : '<button type="button" class="slick-next"></button>'
	});

	checkbox_error_fix();
	update_submit_button();
});
if(  $(document).height() < 1400 ){
	$('#fixed-footer').css({'bottom':0, 'position':'relative'});
	$('footer').css({'margin-bottom':0});
} else {
	if( $(window).width() > 768 ) {
		$(window).scroll(function() {
			if ($(this).scrollTop() >= 750 && !hasBeenTrigged) { // if scroll is greater/equal then 100 and hasBeenTrigged is set to false.
				$('#fixed-footer').animate({'bottom':0});
				hasBeenTrigged = true;
			}
		});
	}
}

$(window).load(function() {
	// Position Main Navigation
	posMainNav();

	$('.gridlist-toggle, .onoffswitch').animate({'opacity': 1});
	// Position Dotted Nav
	var dotsW = $('.slick-dots').width()/2;
	$('.slick-dots').animate({'margin-left':'-'+dotsW+'px'}, function(){
		$('.slick-dots').animate({'opacity':1});
	});
	// Set Equal Heights
	setTimeout(function(){
		slideContent();
		gridContent();
		gridContentSub();
		setMaxHeight();
		if( !Modernizr.touch ) {
			setMaxHeightSingle();
		}
		equalheight( '.woocommerce.term-window-treatments ul.products li.product, .woocommerce ul.products.grid li.product, .set-mh, .getH' );
	}, 500);
});

couponForm();

// Listen for orientation changes
$(window).on('orientationchange', function() {
	if( window.orientation !== 0 ) {
		initMembersSlider();
	}
});

$(window).resize(function() {
	waitForFinalEvent(function() {
	// Position Main Nav
	posMainNav();
		// Set Equal Heights
		setTimeout(function(){
			setMaxHeight();
			if( !Modernizr.touch ) {
				setMaxHeightSingle();
			}
		}, 500);
		slideContent();
		gridContent();
		equalheight( '.woocommerce.term-window-treatments ul.products li.product, .woocommerce ul.products.grid li.product, .set-mh, .getH' );
		gridContentSub();
		if( $(window).width() < 769 ) {
			initMembersSlider();
		} else {
			$("#primary_nav_wrap_mobile").trigger('close.mm');
		}
	}, 500, 'afcc');
});
