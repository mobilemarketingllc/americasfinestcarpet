function initialize() {
	var lat = $('#locations .six.columns').data('lat');
	var lng = $('#locations .six.columns').data('lng');
	var myLatlng = new google.maps.LatLng(lat,lng);
	var mapOptions = {
		zoom: 17,
		center: myLatlng,
		scrollwheel: false
	}
	var map = new google.maps.Map(document.getElementById('multiple-map-canvas'), mapOptions);

	var iconBase = 'http://'+window.location.host+'/wp-content/themes/afcc/assets/img/backgrounds/';
	var marker = new google.maps.Marker({
		position: myLatlng,
		map: map,
		icon: iconBase + 'pin.png'
	});


	var contentString =
		'<div id="infobox-content" class="center">'+
			'<div id="siteNotice">'+
			'</div>'+
			'<h6 id="firstHeading" class="firstHeading">America\'s Finest Carpet Company</h6>'+
			'<p class="nm">3927 Convoy Street Kearny Mesa, CA 92111</p>'+
			'<p class="nm">(619) 427-2420</p>'+
			'<hr class="small">'+
			'<div id="bodyContent">'+
				'<p class="h-title caps nm">Todays Hours</p>' +
				'<p class="nm">9:00am - 6:00pm</p>'+
			'</div>'+
		'</div>'
	;

	var infowindow = new google.maps.InfoWindow({
		content: contentString
	});

	google.maps.event.addListener(marker, 'click', function() {
		infowindow.open(map,marker);
	});
}

google.maps.event.addDomListener(window, 'load', initialize);
