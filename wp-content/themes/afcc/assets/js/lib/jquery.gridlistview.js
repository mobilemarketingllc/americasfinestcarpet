// The toggle

( function( $ ) {
    var haveCookieLib = ( 'cookie' in window.jQuery );

    function setActiveView() {
        var gridCookie;

        if ( ! haveCookieLib ) {
            return;
        }

        gridCookie = $.cookie( 'gridcookie' ) || '';

        if ( gridCookie.length ) {
            $( 'ul.products' ).addClass( gridCookie );
            $( '.gridlist-toggle a' ).removeClass( 'active' );
            $( '.gridlist-toggle #' + gridCookie ).addClass( 'active' );
        }
    }

    setActiveView();

    $( '#list, #grid' ).on( 'click', function( event ) {
        var $self    = $( this ),
            id       = $self.attr( 'id' ),
            opposite = id === 'grid' ? 'list' : 'grid';

        if ( $self.hasClass('active') ) {
            event.preventDefault();
            return;
        }

        $self.addClass( 'active' );
        $( '#' + opposite ).removeClass( 'active' );

        if ( haveCookieLib ) {
            $.cookie( 'gridcookie', id, { path: '/' } );
        }

        $( 'ul.products' ).fadeOut( 300, function() {
            $( this ).removeClass( opposite ).addClass( id ).fadeIn( 300, function() {
                $self.trigger( 'show_' + id );
            } );
        } );

        event.preventDefault();
    } );
} )( jQuery );
