<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package afcc
 */

get_header(); ?>

	<div class="row">
		<div class="twelve columns">
	

			<section class="error-404 not-found top-section">
				<header class="page-header">
					<h1 class="h1-style"><?php _e( 'Oops! That page can&rsquo;t be found.', 'afcc' ); ?></h1>
					<p>This may be because of a mistyped URL, faulty referral or out-of-date search engine listing. Try a few of the links below.</p>
					<ul>
						<li><a href="#">Products</a></li>
						<li><a href="#">About Us</a></li>
						<li><a href="#">Contact Us</a></li>
					</ul>
				</header><!-- .page-header -->

				
			</section><!-- .error-404 -->

		</div><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
