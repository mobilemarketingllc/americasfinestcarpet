<?php
/******************************************************************************
 *              ___   _   _   _ __     __ _   _ __    ___    ___              *
 *             / __| | | | | | '_ \   / _` | | '_ \  / __|  / _ \             *
 *             \__ \ | |_| | | | | | | (_| | | |_) | \__ \ |  __/             *
 *             |___/  \__, | |_| |_|  \__,_| | .__/  |___/  \___|             *
 *                     __/ |                 | |                              *
 *                    |___/                  |_|                              *
 *                                                                            *
 *                    m a r k e t i n g  s o l u t i o n s                    *
 ******************************************************************************/

/**
 * Product Filter Navigation
 *
 * @author     Joseph Leedy <jleedy@synapseresults.com>
 * @category   AFCC
 * @package    AFCC_Theme
 * @copyright  Copyright 2015 Synapse Marketing Solutions (http://synapseresults.com/)
 * @license    Proprietary
 */

/**
 * Checks if the product filters are active
 *
 * @return boolean
 */
function products_are_filtered()
{
	$filters = get_query_var( 'product_filter' );

	return ( ! empty( $filters ) );
}

/**
 * Parse the product filter variables into an array
 *
 * @return array
 */
function get_product_filters()
{
	$filters     = array();
	$filter_vars = get_query_var( 'product_filter' );

	if ( ! empty( $filter_vars ) ) {
		$filter_vars = explode( ';', $filter_vars );

		foreach ( $filter_vars as $filter_var ) {
			list( $key, $value ) = explode( ':', $filter_var );

			if ( strpos( $value, ',' ) !== false ) {
				$value = explode( ',', $value );
			}

			$filters[ $key ] = $value;
		}
	}

	return $filters;
}

/**
 * Remove empty query arguments
 *
 * @param  mixed $item
 * @return mixed
 */
function clean_product_filter_query_args( $item )
{
	if ( is_array( $item ) ) {
		return array_filter( $item, 'clean_product_filter_query_args' );
	}

	if ( ! empty( $item ) ) {
		return true;
	}
}

/**
 * Retrieve an array containing the products matching the selected filters
 *
 * @return array
 */
function get_filtered_products()
{
	$products   = array();
	$filters    = get_product_filters();
	$query_args = array(
		'post_type'   => 'product',
		'post_status' => 'publish',
		'nopaging'    => true,
		'orderby'     => 'menu_order title',
		'order'       => 'ASC',
		'meta_query'  => array(),
	);

	$query_args['meta_query'][] = array(
		'key'     => '_visibility',
		'value'   => array( 'visible', 'catalog' ),
		'compare' => 'IN',
	);

	if ( empty( $filters ) ) {
		return $products;
	}

	if ( ! empty( $filters['orderby'] ) ) {
		switch ( $filters['orderby'] ) {
			case 'price' :
			case 'price-desc' :
				$orderby  = 'meta_value_num';
				$order    = 'price-desc' === $filters['orderby'] ? 'DESC' : 'ASC';
				$meta_key = '_price';
				break;
			case 'rating' :
				add_filter( 'posts_clauses', array( WC()->query, 'order_by_rating_post_clauses' ) );
				break;
			case 'popularity' :
				$orderby  = 'meta_value_num';
				$order    = 'DESC';
				$meta_key = 'total_sales';
				break;
			default :
				$orderby  = $filters['orderby'];
				$order    = empty( $filters['order'] ) ? $filters['order'] : 'DESC';
				$meta_key = '';
		}

		if ( ! empty( $meta_key ) ) {
			$query_args['meta_key'] = $meta_key;
		}

		$query_args['order']   = $order;
		$query_args['orderby'] = $orderby;

		unset( $filters['orderby'] );

		if ( isset( $filters['order'] ) ) {
			unset( $filters['order'] );
		}
	}

	if ( ! empty( $filters['product_cat'] ) ) {
		$query_args['product_cat'] = $filters['product_cat'];

		unset( $filters['product_cat'] );
	}

	if ( ! empty( $filters['product_tag'] ) ) {
		$query_args['product_tag'] = $filters['product_tag'];

		unset( $filters['product_tag'] );
	}

	if ( ! empty( $filters['characteristics'] ) ) {
		$query_args['characteristics'] = $filters['characteristics'];

		unset( $filters['characteristics'] );
	}

	if ( ! empty( $filters['min_price'] )
			&& false !== ( ! isset( $filters['sale_products'] ) || $filters['sale_products'] !== 'yes' ) ) {
		$meta_key  = '_price';
		$meta_type = 'DECIMAL(10,2)';

		if ( isset( $filters['max_price'] ) ) {
			$meta_value   = array( floatval( $filters['min_price'] ), floatval( $filters['max_price'] ) );
			$meta_compare = 'BETWEEN';

			unset( $filters['max_price'] );
		} else {
			$meta_value   = floatval( $filters['min_price'] );
			$meta_compare = '>=';
		}

		$query_args['meta_query'][] = array(
			'key'     => $meta_key,
			'value'   => $meta_value,
			'compare' => $meta_compare,
			'type'    => $meta_type,
		);

		unset( $filters['min_price'] );
	}

	if ( ! empty( $filters['sale_products'] ) ) {
		add_filter( 'posts_join' , 'prdctfltr_join_sale' );
		add_filter( 'posts_where' , 'prdctfltr_sale_filter', 10, 2 );

		unset( $filters['sale_products'] );
	}

	if ( ! empty( $filters['instock_products'] ) ) {
		$stock_status = 'in' === $filters['instock_products'] ? 'instock' : 'outofstock';
		$query_args['meta_query'][] = array(
			'key'     => '_stock_status',
			'value'   => $stock_status,
			'compare' => '=',
		);

		unset( $filters['instock_products'] );
	}

	if ( count( $filters ) > 0 ) {
		$taxonomy_query = array();

		foreach ($filters as $taxonomy => $filter) {
			if ( 'pa_' === substr( $taxonomy, 0, 3 ) && ! empty( $filter ) ) {
				$taxonomy_query[] = array(
					'taxonomy' => $taxonomy,
					'field'    => 'slug',
					'terms'    => $filter,
				);
			}
		}

		if ( ! empty( $taxonomy_query ) ) {
			$query_args['tax_query'] = array(
				'relation' => 'AND',
			);
			$query_args['tax_query'] = array_merge( $query_args['tax_query'], $taxonomy_query );
		}
	}

	$query_args = array_filter( $query_args, 'clean_product_filter_query_args' );

	if ( ! empty( $query_args ) ) {
		$product_query = new WP_Query( $query_args );

		if ( $product_query->post_count > 0 ) {
			$posts = $product_query->posts;

			foreach ($posts as $post) {
				$products[ $post->post_name ] = $post;
			}
		}
	}

	return $products;
}

/**
 * Retrieve the next and previous products within the current filter
 *
 * @return array
 */
function get_product_siblings_in_filter()
{
	$siblings     = array();
	$product      = get_query_var( 'product' );
	$products     = get_filtered_products();
	$prev_product = false;
	$next_product = false;

	if ( empty( $product ) || empty( $products ) || ! array_key_exists( $product, $products ) ) {
		return $siblings;
	}

	$keys  = array_keys( $products );
	$index = array_search( $product, $keys );

	if ( false !== $index ) {
		if ( $index > 0 ) {
			$prev_key     = $keys[ $index - 1 ];
			$prev_product = $products[ $prev_key ];
		}

		if ( $index !== ( count( $keys ) - 1 ) ) {
			$next_key     = $keys[ $index + 1 ];
			$next_product = $products[ $next_key ];
		}
	}

	if ( false !== $prev_product ) {
		$siblings['previous'] = $prev_product;
	}

	if ( false !== $next_product ) {
		$siblings['next'] = $next_product;
	}

	return $siblings;
}

/**
 * Tell WooCommerce to use a custom template to display the product title
 */
function add_filter_nav_template() {
	wc_get_template( 'single-product/filter-navigation.php' );
}

/**
 * Setup the product filter navigation
 *
 * @todo Refactor this to better handle rule setting changes
 */
function add_filter_nav_rewrite()
{
	$wc_permalinks     = get_option( 'woocommerce_permalinks' );
	$product_permalink = empty( $wc_permalinks['product_base'] ) ?
		_x( 'product', 'slug', 'woocommerce' ) : $wc_permalinks['product_base'];
	$product_permalink = trailingslashit( ltrim( $product_permalink, '/' ) );

	if ( strpos( $product_permalink, '%product_cat%' ) !== false ) {
		$product_permalink = str_replace( '/%product_cat%', '', $product_permalink );
		add_rewrite_rule(
			$product_permalink . '(.+?)/([^/]+)/filter/([^/]+)/?$',
			'index.php?product_cat=$matches[1]&product=$matches[2]&product_filter=$matches[3]',
			'top'
		);
	} else {
		add_rewrite_rule(
			$product_permalink . '([^/]+)/filter/([^/]+)/?$',
			'index.php?product=$matches[1]&product_filter=$matches[2]',
			'top'
		);
	}
}

/**
 * Setup the product filter navigation
 */
function setup_filter_nav_template()
{
	if ( is_product() && products_are_filtered() ) {
		remove_action( 'afcc_prod_title', 'set_prod_title', 5 );
		add_action( 'afcc_prod_title', 'add_filter_nav_template', 5 );
	}
}

/**
 * Register the "filter" query variable
 *
 * @param   array $query_vars
 * @return  array
 */
function add_filter_nav_query_vars( $query_vars )
{
	$query_vars[] = 'product_filter';

	return $query_vars;
}

/**
 * Add the active filters to the product URLs
 *
 * @param   string $url
 * @return  string
 */
function append_filter_nav_query_string( $url ) {
	if ( ( !is_shop() && !is_product_category() && !is_product_tag() && !is_product_taxonomy() && !in_the_loop() )
			|| empty( $_GET ) ) {
		return $url;
	}

	$query_vars = filter_input_array( INPUT_GET, FILTER_UNSAFE_RAW );

	if ( isset( $query_vars['post_type'] ) ) {
		unset( $query_vars['post_type'] );
	}

	if ( empty( $query_vars ) ) {
		return $url;
	}

	if ( is_product_category() || is_product_tag() || is_product_taxonomy() ) {
		$category = get_active_product_category();

		if ( ! is_null( $category ) ) {
			if ( ! isset( $query_vars[ $category->taxonomy ] ) ) {
				$query_vars[ $category->taxonomy ] = $category->slug;
			/*} elseif ( $query_vars[ $category->taxonomy ] === $category->slug && count( $query_vars ) === 1 ) {
				return $url;*/
			}
		}
	}

	$filters = http_build_query( $query_vars, '', ';' );
	$filters = str_replace( '=', ':', $filters );
	$filters = urldecode( $filters );

	if ( '' !== get_option( 'permalink_structure' ) ) {
		$url = user_trailingslashit( $url . 'filter/' . $filters );
	} else {
		$url = add_query_arg( 'product_filter', $filters, $url );
	}

	return $url;
}

add_action( 'init', 'add_filter_nav_rewrite' );
add_action( 'wp', 'setup_filter_nav_template', 70 );
add_filter( 'query_vars', 'add_filter_nav_query_vars' );
add_filter( 'the_permalink', 'append_filter_nav_query_string' );
