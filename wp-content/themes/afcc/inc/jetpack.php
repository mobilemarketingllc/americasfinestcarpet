<?php
/**
 * Jetpack Compatibility File
 * See: http://jetpack.me/
 *
 * @package afcc
 */

/**
 * Add theme support for Infinite Scroll.
 * See: http://jetpack.me/support/infinite-scroll/
 */
function afcc_jetpack_setup() {
	add_theme_support( 'infinite-scroll', array(
		'container' => 'main',
		'footer'    => 'page',
	) );
}
add_action( 'after_setup_theme', 'afcc_jetpack_setup' );
