<?php
/******************************************************************************
 *              ___   _   _   _ __     __ _   _ __    ___    ___              *
 *             / __| | | | | | '_ \   / _` | | '_ \  / __|  / _ \             *
 *             \__ \ | |_| | | | | | | (_| | | |_) | \__ \ |  __/             *
 *             |___/  \__, | |_| |_|  \__,_| | .__/  |___/  \___|             *
 *                     __/ |                 | |                              *
 *                    |___/                  |_|                              *
 *                                                                            *
 *                    m a r k e t i n g  s o l u t i o n s                    *
 ******************************************************************************/

/**
 * MobileMercury API Data Filters & Actions
 *
 * @author     Joseph Leedy <jleedy@synapseresults.com>
 * @category   AFCC
 * @package    AFCC_Theme
 * @copyright  Copyright 2015 Synapse Marketing Solutions (http://synapseresults.com/)
 * @license    Proprietary
 */

/**
 * Modify form data before it is sent to MobileMercury
 *
 * @param	$values 	Array of form values
 * @param	$service	Reference to service detail array
 * @param	$cf7    	Reference to Contact Form 7 object
 * @return	array
 */
function mobilemercury_form_data_filter( $values, $service, $cf7 ) {
	$submission  = WPCF7_Submission::get_instance();
	$posted_data = $submission->get_posted_data();

	if ( empty( $values ) || empty( $posted_data ) ) {
		return $values;
	}

	if ( ! array_key_exists( 'mobile_number', $values ) || empty( $values['mobile_number'] ) ) {
		unset( $values['country_code'] );
		return $values;
	}

	if ( ! array_key_exists( 'mobile-opt-in', $posted_data ) || 'Yes' !== $posted_data['mobile-opt-in'][0] ) {
		unset( $values['mobile_number'] );
		unset( $values['country_code'] );
	} else {
		$values['mobile_number'] = preg_replace( '/[^\d]+/', '', $values['mobile_number'] );
		$values['mobile_number'] = substr( $values['mobile_number'], 0, 10 );
	}

	return $values;
}

/**
 * Handle response from MobileMercury
 *
 * Will store MM's request ID in the contact form database if possible
 *
 * @param $response	Response data from MobileMarketing
 * @param $result  	Reference to array to store error and/or success data in
 */
function mobilemercury_form_reponse_action( $response, $result ) {
	if ( empty( $response ) ) {
		$result['errors'] = array( __( 'Response data is empty - expected to receive Request ID!', 'afcc' ) );
		return;
	}

	$response = json_decode( $response );

	if ( ! is_object( $response ) || ! property_exists( $response, 'requestID' ) || empty( $response->requestID ) ) {
		$result['errors'] = array( __( 'Could not get Request ID from response!', 'afcc' ) );
		return;
	}

	$result['success'] = array( sprintf( __( 'Received Request ID: %s', 'afcc' ), $response->requestID ) );

	require_once ABSPATH . 'wp-admin/includes/plugin.php';

	if ( ! is_plugin_active( 'contact-form-7-to-database-extension/contact-form-7-db.php' ) ) {
		return;
	}

	$cf7dbplugin_path = WP_PLUGIN_DIR . '/contact-form-7-to-database-extension/CF7DBPlugin.php';

	if ( ! file_exists( $cf7dbplugin_path ) ) {
		return;
	}

	require_once $cf7dbplugin_path;

	$cf7dbplugin = new CF7DBPlugin();
	$cf7dbplugin_submits_table = $cf7dbplugin->getSubmitsTableName();

	global $wpdb;

	$last_submission = $wpdb->get_row(
		sprintf( 'SELECT submit_time, form_name FROM %s ORDER BY submit_time DESC LIMIT 1',  $cf7dbplugin_submits_table )
	);

	if ( is_null( $last_submission ) ) {
		return;
	}

	$insert_id = $wpdb->insert( $cf7dbplugin_submits_table, array(
		'submit_time' => $last_submission->submit_time,
		'form_name'   => $last_submission->form_name,
		'field_name'  => __( 'MobileMecury Request ID', 'afcc' ),
		'field_value' => $response->requestID,
		'field_order' => 10001
	) );

	if ( $insert_id === false ) {
		$result['errors'] = array( __( 'Could not save MobileMercury\'s Request ID to the contact form database!', 'afcc' ) );
	} else {
		$result['success'][] = __( 'Succesfully saved MobileMercury\'s Request ID to the contact form database!', 'afcc' );
	}
}

add_filter('Forms3rdPartyIntegration_service_filter_post_0', 'mobilemercury_form_data_filter', 10, 3);
add_action('Forms3rdPartyIntegration_service_a0', 'mobilemercury_form_reponse_action', 10, 2);
