<?php
/******************************************************************************
 *              ___   _   _   _ __     __ _   _ __    ___    ___              *
 *             / __| | | | | | '_ \   / _` | | '_ \  / __|  / _ \             *
 *             \__ \ | |_| | | | | | | (_| | | |_) | \__ \ |  __/             *
 *             |___/  \__, | |_| |_|  \__,_| | .__/  |___/  \___|             *
 *                     __/ |                 | |                              *
 *                    |___/                  |_|                              *
 *                                                                            *
 *                    m a r k e t i n g  s o l u t i o n s                    *
 ******************************************************************************/

/**
 * Contact Form 7 Custom Form Fields
 *
 * @author     Joseph Leedy <jleedy@synapseresults.com>
 * @category   AFCC
 * @package    AFCC_Theme
 * @copyright  Copyright 2015 Synapse Marketing Solutions (http://synapseresults.com/)
 * @license    Proprietary
 */

/**
 * Render WPCF7 form tag for the custom select fields
 *
 * Usage (in WPCF7 form):
 *     [afcc_select my-select options...]
 *     [afcc_select* my-select options...] (Use "*" to make select field required)
 *
 * Available options:
 *     class:my-class	CSS class name of the generated select tag
 *     id:my-id      	Unique ID for the generated select tag
 *     tabindex:1    	Order in which the field will be selected with the
 *                   	keyboard
 *     multiple      	Whether to allow multiple items to be selected
 *     include_blank 	Whether to add a blank default item
 *     limit:10      	Maximum number of items to return
 *     default:1     	Index of item to pre-select (separate with "_" for
 *                   	multiples, ex. default:1_4_7)
 *
 * @param 	array  	$tag        	Array containing the data in the WPCF7 field
 *                              	tag
 * @param 	array 	$data       	The data to populate the select with
 * @param 	string	$blank_label	Default label for the select field
 * @param 	array 	$defaults   	Default option indices
 * @return	string
 */
function afcc_select_shortcode_handler( $tag, array $data, $blank_label = '', array $defaults = array() ) {
	$html   = '';
	$labels = $data['labels'];
	$values = $data['values'];

	if ( is_array( $tag ) || ! ( $tag instanceof WPCF7_Shortcode ) ) {
		$tag = new WPCF7_Shortcode( $tag );
	}

	if ( empty( $tag->name ) ) {
		return $html;
	}

	$validation_error = wpcf7_get_validation_error( $tag->name );

	$class = wpcf7_form_controls_class( $tag->type );
	$class = str_replace( '_', '-', $class );

	if ( $validation_error ) {
		$class .= ' wpcf7-not-valid';
	}

	$attributes = array(
		'class'    => $tag->get_class_option( $class ),
		'id'       => $tag->get_id_option(),
		'tabindex' => $tag->get_option( 'tabindex', 'int', true ),
	);

	if ( $tag->is_required() ) {
		$attributes['aria-required'] = 'true';
	}

	$attributes['aria-invalid'] = $validation_error ? 'true' : 'false';

	$multiple      = $tag->has_option( 'multiple' );
	$include_blank = $tag->has_option( 'include_blank' );

	if ( count( $defaults === 0 ) ) {
		$default_options = (array) $tag->get_option( 'default' );

		foreach ( $default_options as $value ) {
			$key = array_search( $value, $values, true );

			if ( false !== $key ) {
				$defaults[] = (int) $key + 1;
			}
		}

		$matches = $tag->get_first_match_option( '/^default:([0-9_]+)$/' );

		if ( ! empty( $matches ) ) {
			$defaults = array_merge( $defaults, explode( '_', $matches[1] ) );
		}
	}

	$defaults = array_unique( $defaults );

	if ( ! $multiple && count( $defaults ) !== 0 ) {
		$defaults = $defaults[0];
	}

	if ( $blank_label === '' ) {
		$blank_label = __( '-- Please select --', 'afcc' );
	}

	$shifted = false;

	if ( $include_blank || empty( $values ) ) {
		array_unshift( $labels, $blank_label );
		array_unshift( $values, '' );

		$shifted = true;
	}

	$hangover = wpcf7_get_hangover( $tag->name );
	$i        = 0;

	foreach ( $values as $key => $grouped_values ) {
		$selected = false;

		if ( ! is_array( $grouped_values ) ) {
			$grouped_values = array( $grouped_values );
		}

		if ( is_string( $key ) ) {
			$html .= '<optgroup label="' . $key . '">';
		}

		foreach ( $grouped_values as $value ) {
			if ( $hangover ) {
				if ( $multiple ) {
					$selected = in_array( esc_sql( $value ), (array) $hangover );
				} else {
					$selected = ( $hangover === esc_sql( $value ) );
				}
			} else {
				if ( ! $shifted && in_array( $i + 1, (array) $defaults ) ) {
					$selected = true;
				} elseif ( $shifted && in_array( $i, (array) $defaults ) ) {
					$selected = true;
				}
			}

			$item_attributes = array(
				'value'    => strip_tags( $value ),
				'selected' => $selected ? 'selected' : ''
			);

			$item_attributes = wpcf7_format_atts( $item_attributes );

			$label = isset( $labels[ $i ] ) ? $labels[ $i ] : $value;

			$html .= sprintf(
				'<option %1$s>%2$s</option>',
				$item_attributes,
				strip_tags( $label )
			);

			$i++;
		}

		if ( is_string( $key ) ) {
			$html .= '</optgroup>';
		}
	}

	if ( $multiple ) {
		$attributes['multiple'] = 'multiple';
	}

	$attributes['name'] = $tag->name . ( $multiple ? '[]' : '' );

	$attributes = wpcf7_format_atts( $attributes );

	$html = sprintf(
		'<span class="wpcf7-form-control-wrap %1$s"><select %2$s>%3$s</select>%4$s</span>',
		sanitize_html_class( $tag->name ),
		$attributes,
		$html,
		$validation_error
	);

	return $html;
}

/**
 * Render WPCF7 form tag for the Store Locations field
 *
 * Usage (in WPCF7 form):
 *     [store_locations locations options...]
 *     [store_locations* locations options...] (Use "*" to make locations field
 *     											required)
 *
 * Available options:
 *     class:my-class	CSS class name of the generated select tag
 *     id:my-id      	Unique ID for the generated select tag
 *     tabindex:1    	Order in which the field will be selected with the
 *                   	keyboard
 *     multiple      	Whether to allow multiple locations to be selected
 *     include_blank 	Whether to add a blank default item
 *     limit:10      	Maximum number of locations to return
 *     default:1     	Index of location to pre-select (separate with "_" for
 *                   	multiples, ex. default:1_4_7)
 *
 * @param 	array	$tag	Array containing the data in the locations field tag
 * @return	string
 */
function store_locations_select_shortcode_handler( $tag ) {
	$html = '';

	$tag = new WPCF7_Shortcode( $tag );

	if ( empty( $tag->name ) ) {
		return $html;
	}

	$limit       = $tag->get_option( 'limit', 'int', true );
	$has_default = $tag->has_option( 'default' );

	$query_arguments = array(
		'post_type'   => 'our-locations',
		'post_status' => 'publish',
	);

	if ( $limit !== false ) {
		$query_arguments['posts_per_page'] = $limit;
	} else {
		$query_arguments['nopaging'] = true;
	}

	$locations       = array();
	$locations_query = new WP_Query( $query_arguments );

	if ( $locations_query->have_posts() ) {
		$locations = $locations_query->posts;
	}

	$location_data      = array(
		'labels' => array(),
		'values' => array(),
	);
	$default            = array();
	$requested_location = 0;
	$i                  = 0;

	if ( ! $has_default ) {
		$requested_location = (int) get_query_var( 'requested_location', 0 );
	}

	foreach ( $locations as $location ) {
		$location_address              = get_field( 'location_address', $location->ID );
		$location_data['labels'][ $i ] = apply_filters( 'the_title', $location->post_title );
		$location_data['values'][ $i ] = $location_address['address'];

		if ( $requested_location !== 0 && $requested_location == $location->ID ) {
			$default[] = $i + 1;
		}

		$i++;
	}

	$blank_label = apply_filters(
		'afcc_wpcf7_locations_select_blank_label',
		__( '-- Please select a location --', 'afcc' )
	);

	$html = afcc_select_shortcode_handler( $tag, $location_data, $blank_label, $default );

	return $html;
}

/**
 * Render WPCF7 form tag for the Products select field
 *
 * Usage (in WPCF7 form):
 *     [products products options...]
 *     [products* products options...] (Use "*" to make products field required)
 *
 * Available options:
 *     class:my-class   	CSS class name of the generated select tag
 *     id:my-id         	Unique ID for the generated select tag
 *     tabindex:1       	Order in which the field will be selected with the
 *                      	keyboard
 *     multiple         	Whether to allow multiple products to be selected
 *     include_blank    	Whether to add a blank default item
 *     limit:10         	Maximum number of products to return
 *     default:1        	Index of product to pre-select (separate with "_"
 *                      	for multiples, ex. default:1_4_7)
 *     group_by:category	Group products by category, a tag, or an attribute
 *     stock_status:instock	Availability of products ("instock" or "outofstock")
 *
 * @todo Add option for price filter and maybe other meta info
 *
 * @param 	array	$tag	Array containing the data in the products field tag
 * @return	string
 */
function products_select_shortcode_handler( $tag ) {
	$html = '';

	$tag = new WPCF7_Shortcode( $tag );

	if ( empty( $tag->name ) ) {
		return $html;
	}

	$limit        = $tag->get_option( 'limit', 'int', true );
	$group_by     = $tag->get_option( 'group_by', '', true );
	$stock_status = $tag->get_option( 'stock_status', '', true );
	$has_default  = $tag->has_option( 'default' );

	if ( $group_by !== false && ( substr( $group_by, 0, 3 ) === 'pa_' ) ) {
		$group_by = substr( $group_by, 3 );
	}

	$query_arguments = array(
		'post_type'   => 'product',
		'post_status' => 'publish',
		'orderby'     => 'menu_order title',
		'order'       => 'ASC',
	);

	if ( $limit !== false ) {
		$query_arguments['posts_per_page'] = $limit;
	} else {
		$query_arguments['nopaging'] = true;
	}

	$valid_statuses = array( 'instock', 'outofstock' );

	if ( $stock_status !== false && in_array( $stock_status, $valid_statuses ) ) {
		$query_arguments['meta_query'] = array( array(
			'key'     => '_stock_status',
			'value'   => $stock_status,
			'compare' => '=',
		) );
	}

	$products       = array();
	$products_query = new WP_Query( $query_arguments );

	if ( $products_query->have_posts() ) {
		$products = $products_query->posts;
	}

	$product_data = array(
		'labels' => array(),
		'values' => array(),
	);

	$default           = array();
	$requested_product = 0;
	$i                 = 0;

	if ( ! $has_default ) {
		$requested_product = (int) get_query_var( 'requested_product', 0 );
	}

	foreach ( $products as $product ) {
		$value_set      = false;
		$product_colors = get_the_terms( $product->ID, 'pa_color' );
		$product_name   = $product->post_title;

		if ( $product_colors !== false && ! is_wp_error( $product_colors ) ) {
			$colors = array();

			foreach ( $product_colors as $product_color ) {
				$colors[] = $product_color->name;
			}

			$product_name .= ' - ' . implode( ',', $colors );
		}

		$product_name  = apply_filters( 'the_title', $product_name );

		if ( $group_by !== false ) {
			switch ( $group_by ) {
				case 'category':
				case 'cat':
					$taxonomy = 'product_cat';
					break;
				case 'tag':
					$taxonomy = 'product_tag';
					break;
				default:
					$taxonomy = 'pa_' . $group_by;
			}

			if ( taxonomy_exists( $taxonomy ) ) {
				$terms = get_the_terms( $product->ID, $taxonomy );

				if ( $terms !== false && ! is_wp_error( $terms ) ) {
					$group = array_pop( $terms );

					if ( ! array_key_exists( $group, $product_data['values'] ) ) {
						$product_data['values'][ $group ] = array();
					}

					$product_data['values'][ $group ][ $i ] = $product_name;
					$value_set = true;
				}
			}
		}

		if ( ! $value_set ) {
			$product_data['values'][ $i ] = $product_name;
		}

		$product_data['labels'][ $i ] = $product_name;

		if ( $requested_product !== 0 && $requested_product == $product->ID ) {
			$default[] = $i + 1;
		}

		$i++;
	}

	$blank_label = apply_filters(
		'afcc_wpcf7_products_select_blank_label',
		__( '-- Please select a product --', 'afcc' )
	);

	$html = afcc_select_shortcode_handler( $tag, $product_data, $blank_label, $default );

	return $html;
}

/**
 * Render WPCF7 form tag for the Product Categories select field
 *
 * Usage (in WPCF7 form):
 *     [product_categories product-categories options...]
 *     [product_categories* product-categories options...] (Use "*" to make
 *                                                          product categories
 *                                                          field required)
 *
 * Available options:
 *     class:my-class    	CSS class name of the generated select tag
 *     id:my-id          	Unique ID for the generated select tag
 *     tabindex:1        	Order in which the field will be selected with the
 *                       	keyboard
 *     multiple          	Whether to allow multiple products to be selected
 *     include_blank     	Whether to add a blank default item
 *     limit:10          	Maximum number of product categories to return
 *     default:1         	Index of product category to pre-select (separate
 *                       	with "_" for multiples, ex. default:1_4_7)
 *     parent_category:10	Only show children of this category (provide ID)
 *     heirarchical:true 	Show all categories in tree-like structure
 *     max_depth:10      	Maximum level of categories to display
 *     hide_empty:false    	Whether or not to show categories with no products
 *
 * @todo Implement "heirachical", "hide_empty" and "max_depth" parameters
 *
 * @param 	array	$tag	Array containing the data in the products field tag
 * @return	string
 */
function product_categories_select_shortcode_handler( $tag ) {
	$html = '';

	$tag = new WPCF7_Shortcode( $tag );

	if ( empty( $tag->name ) ) {
		return $html;
	}

	$limit       = $tag->get_option( 'limit', 'int', true );
	$parent_id   = $tag->get_option( 'parent_category', 'id', true );
	$has_default = $tag->has_option( 'default' );

	$arguments = array();

	if ( $limit !== false ) {
		$arguments['number'] = $limit;
	}

	if ( $parent_id !== false && is_numeric( $parent_id ) ) {
		$arguments['parent'] = $parent_id;
	}

	$categories = get_terms( 'product_cat', $arguments );

	if ( empty( $categories ) || is_wp_error( $categories ) ) {
		return $html;
	}

	$category_data = array(
		'labels' => array(),
		'values' => array(),
	);

	$default            = array();
	$requested_category = 0;
	$i                  = 0;

	if ( ! $has_default ) {
		$requested_category = (int) get_query_var( 'requested_product_category', 0 );
	}

	foreach ( $categories as $category ) {
		$category_name  = apply_filters( 'the_title', $category->name );

		if ( $parent_id === false ) {
			$ancestor_count = count( get_ancestors( $category->term_id, 'product_cat' ) );
			$category_name  = str_repeat( '--', $ancestor_count ) . $category_name;
		}

		$category_data['values'][ $i ] = $category->name;
		$category_data['labels'][ $i ] = $category_name;

		if ( $requested_category !== 0 && $requested_category == $category->term_id ) {
			$default[] = $i + 1;
		}

		$i++;
	}

	$blank_label = apply_filters(
		'afcc_wpcf7_product_categories_select_blank_label',
		__( '-- Please select a product category --', 'afcc' )
	);

	$html = afcc_select_shortcode_handler( $tag, $category_data, $blank_label, $default );

	return $html;
}

/**
 * Render WPCF7 Tag Generator Pane for the custom fields
 *
 * @param object	$contact_form
 * @param string	$field
 * @param array 	$data
 */
function afcc_wpcf7_tg_pane_renderer( $contact_form, $field, array $data = array() ) {
	$template = locate_template( "cf7-custom-fields/$field/tag-generator.php", false );

	include $template;
}

/**
 * Render WPCF7 Tag Generator Pane for the Store Locations field
 *
 * @param object $contact_form
 */
function afcc_wpcf7_tg_pane_store_locations( $contact_form ) {
	afcc_wpcf7_tg_pane_renderer( $contact_form, 'store-locations' );
}

/**
 * Render WPCF7 Tag Generator Pane for the Products field
 *
 * @param object $contact_form
 */
function afcc_wpcf7_tg_pane_products( $contact_form ) {
	afcc_wpcf7_tg_pane_renderer( $contact_form, 'products' );
}

/**
 * Render WPCF7 Tag Generator Pane for the Product Categories field
 *
 * @todo Fix the $categories array to order by hierarchy
 *
 * @param object $contact_form
 */
function afcc_wpcf7_tg_pane_product_categories( $contact_form ) {
	$data               = array( 'categories' => array() );
	$args               = array(
		'taxonomy'    => 'product_cat',
		'hide_empty'  => false,
		'hierachical' => true,
		'orderby'     => 'id',
	);
	$product_categories = get_categories( $args );

	if ( ! empty( $product_categories ) ) {
		$data['categories'] = $product_categories;

		afcc_wpcf7_tg_pane_renderer( $contact_form, 'product-categories', $data );
	}
}

/**
 * Register form field shortcodes
 */
function afcc_wpcf7_add_shortcodes() {
	if ( ! function_exists( 'wpcf7_add_shortcode' ) ) {
		return;
	}

	wpcf7_add_shortcode(
		array( 'store_locations', 'store_locations*' ),
		'store_locations_select_shortcode_handler',
		true
	);

	wpcf7_add_shortcode(
		array( 'products', 'products*' ),
		'products_select_shortcode_handler',
		true
	);

	wpcf7_add_shortcode(
		array( 'product_categories', 'product_categories*' ),
		'product_categories_select_shortcode_handler',
		true
	);
}

/**
 * Add Tag Generators to WPCF7 to create our custom form fields
 */
function afcc_wpcf7_add_tag_generators() {
	if ( ! function_exists( 'wpcf7_add_tag_generator' ) ) {
		return;
	}

	wpcf7_add_tag_generator(
		'locations',
		__( 'Store locations menu', 'afcc' ),
		'afcc-wpcf7-tg-pane-store-locations',
		'afcc_wpcf7_tg_pane_store_locations'
	);

	wpcf7_add_tag_generator(
		'products',
		__( 'Products menu', 'afcc' ),
		'afcc-wpcf7-tg-pane-products',
		'afcc_wpcf7_tg_pane_products'
	);

	wpcf7_add_tag_generator(
		'product-categories',
		__( 'Product categories menu', 'afcc' ),
		'afcc-wpcf7-tg-pane-product-categories',
		'afcc_wpcf7_tg_pane_product_categories'
	);
}

/**
 * Validate the required fields
 *
 * @param 	object	$result
 * @param 	object	$tag
 * @return	object
 */
function afcc_wpcf7_validation_filter( $result, $tag ) {
	$tag = new WPCF7_Shortcode( $tag );

	$name = $tag->name;

	if ( isset( $_POST[ $name ] ) && is_array( $_POST[ $name ] ) ) {
		foreach ( $_POST[ $name ] as $key => $value ) {
			if ( '' === $value ) {
				unset( $_POST[ $name ][ $key ] );
			}
		}
	}

	$empty = ! isset( $_POST[ $name ] ) || empty( $_POST[ $name ] ) && '0' !== $_POST[ $name ];

	if ( $tag->is_required() && $empty ) {
		if ( 'store_locations*' === $tag->type ) {
			$result->invalidate( $tag, __( 'Please select a store location.', 'afcc' ) );
		} elseif ( 'products*' === $tag->type ) {
			$result->invalidate( $tag, __( 'Please select at least one product.', 'afcc' ) );
		} elseif ( 'product_categories*' === $tag->type ) {
			$result->invalidate( $tag, __( 'Please select at least one product category.', 'afcc' ) );
		} else {
			$result->invalidate( $tag, wpcf7_get_message( 'invalid_required' ) );
		}
	}

	return $result;
}

/*
 * Opt-In checkbox was always returning "Yes"
 */
function afcc_posted_data($posted_data) {
	// Fix for checkboxes
	if(!isset($_POST['mobile-opt-in'])) {
		$posted_data['mobile-opt-in'] = array('No');
	}

	return $posted_data;
}

add_action( 'wpcf7_init', 'afcc_wpcf7_add_shortcodes' );
add_action( 'admin_init', 'afcc_wpcf7_add_tag_generators', 100 );
add_filter( 'wpcf7_validate_store_locations', 'afcc_wpcf7_validation_filter', 10, 2 );
add_filter( 'wpcf7_validate_store_locations*', 'afcc_wpcf7_validation_filter', 10, 2 );
add_filter( 'wpcf7_validate_products', 'afcc_wpcf7_validation_filter', 10, 2 );
add_filter( 'wpcf7_validate_products*', 'afcc_wpcf7_validation_filter', 10, 2 );
add_filter( 'wpcf7_validate_product_categories', 'afcc_wpcf7_validation_filter', 10, 2 );
add_filter( 'wpcf7_validate_product_categories*', 'afcc_wpcf7_validation_filter', 10, 2 );
add_filter( 'wpcf7_posted_data', 'afcc_posted_data' );
