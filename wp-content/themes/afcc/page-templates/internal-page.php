<?php
/**
 * Template Name: Internal
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
get_header(); ?>
<?php 
if ( has_post_thumbnail() ) {
	$large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
}
?>
	<section>
		<div class="row">
			<div class="twelve columns np">
				<div class="centered-area">
					<div class="content">
						<h1 class="headline nm"><?php echo the_title(); ?></h1>
						<p class="nm large"><?php echo get_the_content(); ?></p>
					</div>
				</div>
			</div>
		</div>
	</section>

<?php
get_footer();
