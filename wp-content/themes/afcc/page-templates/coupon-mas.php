<?php
/**
 * Template Name: Coupon MAS
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
get_header(); ?>
<style>
.four.columns.contain-coupon-form {
    margin-bottom: -400px !important;
}
</style>
<?php 
if ( has_post_thumbnail() ) {
	$large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
}
$subHead = get_field('main_sub_headline');
?>
	<section id="coupon-header" class="parallax" data-stellar-background-ratio="0.5" style="background-image: url(<?php echo $large_image_url[0]; ?>); background-size: cover;">
		<div class="row">
			<div class="twelve columns">
				<div class="eight columns coupon-content"style="top:20% !important;">
                    <h1 style="color: #fff;">Karastan All Pet Sale</h1>
					<?php
						/* $header = get_field('coupon_page_header', 'option');
						$deats  = get_field('coupon_details', 'option');
						if($header){
							echo $header;

						} */?>
						 <p class="lg-title-custom" style="line-height:0.9em;font-size: 2.8em;">Luxury Carpet for All Pets*</p>
                    <!-- <p class="bold-lg-title-custom"> </p>--><br/>
                    <p class="sm-title-custom"  style="line-height:0.9em;font-size: 1.5em;">Instant in-store Rebate, $3.00 sq./yard rebate</p>
                   <p style="margin-top: 20px; font-size: 14px;">Offer ends: September 4, 2018. </p>
						<?php echo '<p class="small coupon-deats">
                        
                        <br><br>

* Instant In-Store Rebate - $3.00 square/yard rebate on all qualifying installed carpet orders on select Karastan SmartStrand® Forever Clean Styles. <br><br>

Offer valid from <b>July 30, 2018</b> through <b>September 4, 2018</b> on select SmartStrand Forever Clean products, all reserves the right, in its sole discretion, to require the original receipt as proof of purchase.
<br/><br/>
Offer subject to change or cancellation in whole or in part without notice. Offer cannot be combined with any other manufacturer’s offer. All offers may be audited, and unsubstantiated offers may be denied. Fraudulent submissions of multiple requests could result in prosecution. Void where taxed, prohibited or restricted by law. Offer is available only to retail purchasers of Karastan  residential SmartStrand Forever Clean products. Manufacturers, distributors, and dealers of Mohawk Industry products and their families may not participate.                    
                        </p>';
					?>
				</div>
				<div class="four columns contain-coupon-form">

					<?php
						/* $formHeader = get_field('coupon_page_form_header', 'option');
						if($formHeader){
							echo $formHeader;
						} */
					?>
					<h3 class="center"><span class="caps"></span>Karastan All Pet Sale</h3>
                    <p class="center small">To receive our flooring coupon, fill out<br>
                        the form below.</p>
					<?php //echo do_shortcode('[contact-form-7 id="8" title="Get Coupon"]'); ?>
                    <?php echo do_shortcode('[gravityform id="22" title="false" description="false"]'); ?>

				</div>
			</div>
		</div>
	</section>
	<section id="breadcrumbs">
		<div class="row">
			<div class="twelve columns">
				<?php
					if ( function_exists('yoast_breadcrumb') ) {
						yoast_breadcrumb();
					}
				?>
			</div>
		</div>
	</section>

	<section id="product-slider-container">
	<div class="row">
		<div class="twelve columns">
			<h1 class="center"><?php echo get_the_content(); ?></h1>
			<p class="center small"><?php //echo $subHead; ?></p>
			<div id="product-slider">
				<?php if( have_rows('secondary_slider', 'option') ):
					// loop through the rows of data
				    while ( have_rows('secondary_slider', 'option') ) : the_row();
				        $sImg  = get_sub_field('image');
				        $sHead  = get_sub_field('headline');
				        $sContent  = get_sub_field('content');
				        $sCta  = get_sub_field('cta_text');
				        $sCtaLink  = get_sub_field('cta_link');
				?>
					<div class="slide center">
						<div class="eleven columns">
							<div class="six columns wbg getH">
								<h3><?php echo $sHead; ?></h3>
								<?php echo $sContent; ?>
								<a href="<?php echo $sCtaLink; ?>" class="cta"><?php echo $sCta; ?></a>
							</div>
							<div class="fl np set-mh with-image six columns">
								<img src="<?php echo $sImg['url']; ?>" alt="<?php echo $sImg['alt']; ?>" width="415" height="417"/>
							</div>
						</div>
					</div>
				<?php
					endwhile;
					endif;
				?>
			</div>
		</div>
	</div>
</section>

<?php
get_footer();
