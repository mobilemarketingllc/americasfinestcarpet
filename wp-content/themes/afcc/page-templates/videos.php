<?php
/**
 * Template Name: Video Gallery
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
get_header();
$subHead = get_field('main_sub_headline');
?>
	<section id="breadcrumbs" class="top-section">
		<div class="row">
			<div class="twelve columns">
				<?php
					if ( function_exists('yoast_breadcrumb') ) {
						yoast_breadcrumb();
					}
				?>
			</div>
		</div>
	</section>
	<section>
		<div class="row">
			<div class="twelve columns center lh-lots">
				<h1 class="h1-style"><?php the_title(); ?></h1>
				<p class="large sm"><?php echo $subHead; ?></p>
				<hr class="small">
				<p><?php echo get_the_content(); ?></p>
			</div>
		</div>
	</section>

	<section id="vid-mixing-contain">
		<div class="row">
			<div class="twelve columns">
				<ul class="video-filter-nav">
					<li class="filter" data-filter="all"><a href="#" class="active">All</a></li>
					<?php
					if( have_rows('video_gallery') ):
						// loop through the rows of data
						$filters = array();
						while ( have_rows('video_gallery') ) : the_row();
							$videoType  = get_sub_field('video_type');
							$mixCat = '';
							if( !empty($videoType) ) :
								if( strpos($videoType, ',') !== false ) :
									$videoTypes = explode(',', $videoType);
									foreach ($videoTypes as $type) :
										$mixCat = trim($type);
										$mixCat = sanitize_title($mixCat);
										if( !array_key_exists($mixCat, $filters) ) :
											$filters[$mixCat] = $type;
										endif;
									endforeach;
								else :
									$mixCat = sanitize_title($videoType);
									if( !array_key_exists($mixCat, $filters) ) :
										$filters[$mixCat] = $videoType;
									endif;
								endif;
							endif;
						endwhile;
						if( !empty($filters) ):
							ksort($filters);
							foreach ($filters as $filter => $filterName) :
					?>
					<li class="filter" data-filter=".<?php echo $filter; ?>"><a href="#"><?php echo $filterName; ?></a></li>
					<?php
							endforeach;
						endif;
					endif;
					?>
				</ul>
				<?php if( !empty($filters) ): ?>
				<select id="vid-mix-mobile">
					<option class="filter" data-filter="all" value="all">All</option>
					<?php foreach ($filters as $filter => $filterName) : ?>
						<option class="filter" data-filter=".<?php echo $filter; ?>" value=".<?php echo $filter; ?>"><?php echo $filterName; ?></option>
					<?php endforeach; ?>
				</select>
				<?php endif; ?>
			</div>
		</div>
		<div class="row videos" id="vid-mixer">
			<?php
			if( have_rows('video_gallery') ):
				// loop through the rows of data
				while ( have_rows('video_gallery') ) : the_row();
					$vidLink  = get_sub_field('video_link');
					$vidTitle  = get_sub_field('video_title');
					$vidType  = get_sub_field('video_type');
					$vidTime  = get_sub_field('video_time');
					$vidImg  = get_sub_field('video_thumbnail');
					$typeClass = '';
					$vidCat = '';
					if( strpos($vidType, ',') !== false ) :
						$types = explode(',', $vidType);
						$finalTypes = array();
						foreach ($types as $type) :
							$typeKey = sanitize_title($type);
							$typeKey = trim($typeKey);
							$finalTypes[$typeKey] = $type;
						endforeach;
						if( !empty($finalTypes) ):
							$typeClass = implode(' ', array_keys($finalTypes));
							$vidCat = implode(', ', $finalTypes);
						endif;
					else :
						$typeClass = sanitize_title($vidType);
						$vidCat = $vidType;
					endif;
			?>
			<div class="four columns mix <?php echo  $typeClass; ?>">
				<a href="<?php echo $vidLink; ?>" rel="<?php echo $typeClass; ?>" class="contain-videos">
					<div class="cutoff">
						<img src="<?php echo $vidImg['url']; ?>" alt="<?php echo $vidImg['alt']; ?>" width="<?php echo $vidImg['width']; ?>" height="<?php echo $vidImg['height']; ?>" />
					</div>
					<p class="vid-title"><?php echo $vidTitle; ?></p>
					<p class="vid-type"><?php echo $vidCat; ?></p>
					<p class="vid-time"><?php echo $vidTime; ?></p>
				</a>
			</div>
			<?php
				endwhile;
			endif;
			?>
		</div>
	</section>

<?php
get_footer();
