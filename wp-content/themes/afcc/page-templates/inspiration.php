<?php
/**
 * Template Name: Inspiration Gallery
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
get_header(); ?>
<div id="overlay"></div>
<section id="breadcrumbs" class="top-section">
	<div class="row">
		<div class="twelve columns">
			<?php
				if ( function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb();
				}
			?>
		</div>
	</div>
</section>
<section id="product-slider-container" class="top-section">
	<div class="row">
		<div class="twelve columns center lh-lots">
			<h1 class="nbm"><?php the_title(); ?></h1>
			<?php
				$inspiredCopy = get_field('sub_heading_copy');
				if($inspiredCopy) :
			?>
			<span class="large nm inspire-copy"><?php echo $inspiredCopy; ?></span>
			<?php endif; ?>
			<p><?php echo get_the_content(); ?></p>
		</div>
	</div>
</section>
<section id="inspiration-grid" class="custom-grid gallery-images">
	<div class="row added">
		<?php
			if(function_exists('get_field')) {
				if( have_rows('grid') ) {
					while( have_rows('grid') ): the_row();
						if(get_row_layout() == 'five_left_seven_right') {
							if( have_rows('column_details_five_seven') ) {
								// FIVE COLUMNS LEFT
								$i = 0;
								echo '<div class="five columns npr">';
								while( have_rows('column_details_five_seven') ): the_row();
									$fiveSevenBg      = get_sub_field('background_five_seven');
									$fiveSevenHead    = get_sub_field('headline_five_seven');
									$fiveSevenSubHead = get_sub_field('sub_headline_five_seven');
									$i++;
									if( $i %2 == 0 ) {
										//even
										$classes = 'nmr-mobile';
									} else {
										//odd
										$classes = 'added';
									}
					?>
									<a href="<?php echo $fiveSevenBg; ?>" class="inspiration-gal" rel="inspire" title="<?php echo $fiveSevenHead; ?>" data-product-brand="<?php echo $fiveSevenSubHead; ?>">
										<div class="grid-bg short msnry <?php echo $classes; ?>" style="background: url(<?php echo $fiveSevenBg; ?>) no-repeat;"></div>
									</a>
					<?php
								endwhile;
								echo '</div>';
							}
							// SEVEN COLUMNS RIGHT
							$fiveSevenBgNr      = get_sub_field('background_five_seven_nr');
							$fiveSevenHeadNr    = get_sub_field('headline_five_seven_nr');
							$fiveSevenSubHeadNr = get_sub_field('sub_headline_five_seven_nr');
					?>
							<div class="seven columns npr-mobile">
								<a href="<?php echo $fiveSevenBgNr; ?>" class="inspiration-gal" rel="inspire" title="<?php echo $fiveSevenHeadNr; ?>" data-product-brand="<?php echo $fiveSevenSubHeadNr; ?>">
									<div class="grid-bg tall" style="background: url(<?php echo $fiveSevenBgNr; ?>) no-repeat;"></div>
								</a>
							</div>
					<?php
						} elseif (get_row_layout() == 'seven_left_five_right') {
							// SEVEN COLUMNS LEFT
							$sevenFiveBgNr      = get_sub_field('background_seven_five_nr');
							$sevenFiveHeadNr    = get_sub_field('headline_seven_five_nr');
							$sevenFiveSubHeadNr = get_sub_field('sub_headline_seven_five_nr');
					?>
							<div class="seven columns tall npl-mobile">
								<a href="<?php echo $sevenFiveBgNr; ?>" class="inspiration-gal" rel="inspire" title="<?php echo $sevenFiveHeadNr; ?>" data-product-brand="<?php echo $sevenFiveSubHeadNr; ?>">
									<div class="grid-bg tall" style="background: url(<?php echo $sevenFiveBgNr; ?>) no-repeat;"></div>
								</a>
							</div>
					<?php
							if( have_rows('column_details_seven_five') ) {
								// FIVE COLUMNS RIGHT
								$i = 0;
								echo '<div class="five columns npl">';
								while( have_rows('column_details_seven_five') ): the_row();
									$sevenFiveBg      = get_sub_field('background_seven_five');
									$sevenFiveHead    = get_sub_field('headline_seven_five');
									$sevenFiveSubHead = get_sub_field('sub_headline_seven_five');
									$i++;
									if( $i %2 == 0 ) {
										//even
										$classes = 'nmr-mobile';
									} else {
										//odd
										$classes = 'added';
									}
					?>
									<a href="<?php echo $sevenFiveBg; ?>" class="inspiration-gal" rel="inspire" title="<?php echo $sevenFiveHead; ?>" data-product-brand="<?php echo $sevenFiveSubHead; ?>">
										<div class="grid-bg short msnry <?php echo $classes; ?>" style="background: url(<?php echo $sevenFiveBg; ?>) no-repeat;"></div>
									</a>
					<?php
								endwhile;
								echo '</div>';
							}
						}
						echo '<div class="clear added"></div>';
					endwhile;
				}
			}
		?>
	</div>
</section>
<?php
get_footer();
