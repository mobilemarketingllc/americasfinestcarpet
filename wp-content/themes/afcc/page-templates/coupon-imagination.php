<?php
/**
 * Template Name: Coupon Imagination
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
get_header(); ?>
<?php 
if ( has_post_thumbnail() ) {
	$large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
}
$subHead = get_field('main_sub_headline');
?>
	<section id="coupon-header" class="parallax" data-stellar-background-ratio="0.5" style="background-image: url(<?php echo $large_image_url[0]; ?>); background-size: cover;">
		<div class="row">
			<div class="twelve columns">
				<div class="eight columns coupon-content" style="top:25% !important;">

                    <h1 style="color: #fff;">Mohawk Floor Your Imagination Sale</h1>

                    <p class="lg-title-custom">Get up to</p>
                    <p class="bold-lg-title-custom">$1,000</p>
                    <p class="sm-title-custom">On select Flooring styles*</p>
                    <p style="margin-top: 20px; font-size: 14px;">Offer ends July 16, 2018</p>

					<?php /*
						$header = get_field('coupon_page_header', 'option');
						$deats  = get_field('coupon_details', 'option');
						if($header){
							echo $header;

						} */
						echo '<p class="small coupon-deats">*Save $100 for every $1,000 spent on select Mohawk flooring styles. Earn up to a $1,000 discount on a $10,000 purchase. This offer is valid on a minimum purchase of $1,000. First quality, current running line styles. Does not apply to prior purchases. Cannot be combined with other offers. Offer void where prohibited or restricted by law. Offer good only in the USA and Canada. Plus, special financing available at participating retailers on your purchase of any qualifying Mohawk flooring products. Offer Expires: July 16, 2018.</p>

<p class="small coupon-deats">Offer subject to change or cancellation in whole or in part without notice. Offer cannot be combined with any other manufacturer’s offer. All offers may be audited, and unsubstantiated offers may be denied. Fraudulent submissions of multiple requests could result in prosecution. Void where taxed, prohibited or restricted by law. Offer is available only to retail purchasers of Mohawk residential flooring. Manufacturers, distributors, and dealers of Mohawk products and their families may not participate.</p>';
					?>
                 
				</div>
				<div class="four columns contain-coupon-form">
					<?php /*
						$formHeader = get_field('coupon_page_form_header', 'option');
						if($formHeader){
							echo $formHeader;
						} */
					?>

                    <h3 class="center"><span class="caps">Get</span> up to $1,000</h3>
                    <p class="center small">To receive our flooring coupon, fill out<br>
                        the form below.</p>
					<?php //echo do_shortcode('[contact-form-7 id="8" title="Get Coupon"]'); ?>
                    <?php echo do_shortcode('[gravityform id="21" title="false" description="false"]'); ?>

				</div>
			</div>
		</div>
	</section>
	<section id="breadcrumbs">
		<div class="row">
			<div class="twelve columns">
				<?php
					if ( function_exists('yoast_breadcrumb') ) {
						yoast_breadcrumb();
					}
				?>
			</div>
		</div>
	</section>

	<section id="product-slider-container">
	<div class="row">
		<div class="twelve columns">
			<h1 class="center"><?php echo get_the_content(); ?></h1>
			<p class="center small"><?php echo $subHead; ?></p>
			<div id="product-slider">
				<?php if( have_rows('secondary_slider', 'option') ):
					// loop through the rows of data
				    while ( have_rows('secondary_slider', 'option') ) : the_row();
				        $sImg  = get_sub_field('image');
				        $sHead  = get_sub_field('headline');
				        $sContent  = get_sub_field('content');
				        $sCta  = get_sub_field('cta_text');
				        $sCtaLink  = get_sub_field('cta_link');
				?>
					<div class="slide center">
						<div class="eleven columns">
							<div class="six columns wbg getH">
								<h3><?php echo $sHead; ?></h3>
								<?php echo $sContent; ?>
								<a href="<?php echo $sCtaLink; ?>" class="cta"><?php echo $sCta; ?></a>
							</div>
							<div class="fl np set-mh with-image six columns">
								<img src="<?php echo $sImg['url']; ?>" alt="<?php echo $sImg['alt']; ?>" width="415" height="417"/>
							</div>
						</div>
					</div>
				<?php
					endwhile;
					endif;
				?>
			</div>
		</div>
	</div>
</section>

<?php
get_footer();
