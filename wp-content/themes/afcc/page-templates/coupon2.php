<?php
/**
 * Template Name: Coupon LYFS
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
get_header(); ?>
<?php 
if ( has_post_thumbnail() ) {
	$large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
}
$subHead = get_field('main_sub_headline');
?>
	<section id="coupon-header" class="parallax" data-stellar-background-ratio="0.5" style="background-image: url(<?php echo $large_image_url[0]; ?>);">
		<div class="row">
			<div class="twelve columns">

                    <div class="eight columns coupon-content">
                        <h1 style="color: #fff !important;font-family: 'roboto_slabthin', serif; margin-top: -40px; font-size: 2.5em;">Mohawk Love Your Floor Sale</h1>
                        <p class="lg-title-custom">Save up to</p>
                        <p class="bold-lg-title-custom">$500 Off*</p>
                        <p class="sm-title-custom">or take 50% off select styles*.</p>
                        <p class="small coupon-deats">*Present this flooring coupon to a sales associate at time of purchase. This coupon entitles you to receive up to $500 off any qualifying and get 50% off select styles any qualifying Mohawk Flooring product,


                            America’s Finest Carpet locations. In-store only, no online sales. Discount applies to flooring material only. This coupon cannot be combined with any other offer. One coupon limit per person, per order, at

                            America’s Finest Carpet locations. Coupon expires: 5/30/17</p>
					<?php
						/* $header = get_field('coupon_page_header', 'option');
						$deats  = get_field('coupon_details', 'option');
						if($header){
							echo $header;

						}
						echo '<p class="small coupon-deats">'.$deats.'</p>'; */
					?>
				</div>
				<div class="four columns contain-coupon-form">
					<?php
						$formHeader = get_field('coupon_page_form_header', 'option');
						if($formHeader){
							echo $formHeader;
						}
					?>
					<?php //echo do_shortcode('[contact-form-7 id="8" title="Get Coupon"]'); ?>
                    <?php echo do_shortcode('[gravityform id="11" title="false" description="false"]'); ?>

				</div>
			</div>
		</div>
	</section>


<?php
get_footer();
