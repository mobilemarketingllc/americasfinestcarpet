<?php
/**
 * Template Name: Product Landing Page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
get_header(); ?>
<?php 
if ( has_post_thumbnail() ) {
	$large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
}
?>
	<section class="parallax added" data-stellar-background-ratio="0.5" style="background-image: url(<?php echo $large_image_url[0]; ?>);">
		<div class="row">
			<div class="twelve columns np">
				<div class="centered-area">
					<div class="content">
						<h1 class="headline nm"><?php echo the_title(); ?></h1>
						<p class="nm large"><?php echo get_the_content(); ?></p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="breadcrumbs">
		<div class="row">
			<div class="twelve columns">
				<?php
					if ( function_exists('yoast_breadcrumb') ) {
						yoast_breadcrumb();
					}
				?>
			</div>
		</div>
	</section>
	<section class="custom-grid tall-five">
	<div class="row added">
			<?php
				$args = array(
				'taxonomy'     => 'product_cat',
				'orderby'      => 'name',
				'show_count'   => 0,
				'pad_counts'   => 0,
				'hierarchical' => 1,
				'title_li'     => '',
				'hide_empty'   => 0,
				'parent'       => 0
				);
				
				$all_categories = get_categories( $args );
				$i = 0;
				foreach ($all_categories as $cat) {
					$i++;
				    if($cat->category_parent == 0) {
				        $category_id = $cat->term_id;
					    $thumbnail_id = get_woocommerce_term_meta( $cat->term_id, 'thumbnail_id', true ); 
					    $image = wp_get_attachment_url( $thumbnail_id ); 
							if ( $i % 2 != 0 ) {
								$pad = ' npr';
							} else {
								$pad = '';
							}
							if ( $i % 4 == 0 || $i % 4 == 1 ) {
								$colW = 'five';
								$mar = 'msnry';
							}
							else {
								$colW = 'seven npr-mobile';
								$mar = '';
							}
			?>
							<div class="<?php echo $colW.$pad; ?> columns">
								<div class="grid-bg tall full-size <?php echo $mar; ?>" style="background: url(<?php echo $image; ?>) no-repeat">
									<a href="<?php  echo get_term_link($cat->slug, 'product_cat'); ?>" class="contain-white short">
										Explore <?php echo $cat->name; ?>
										<span class="small"><?php echo $cat->description; ?></span>
									</a>
								</div>
							</div>
			<?php
						if( $i % 2 == 0 ){
							echo '<div class="clear added"></div>';
						}
					}
				}
			?> 
		</div>
	</section>

<?php
get_footer();
