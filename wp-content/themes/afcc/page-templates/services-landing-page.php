<?php
/**
 * Template Name: Services Landing Page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
get_header(); ?>
<?php 
if ( has_post_thumbnail() ) {
	$large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
}
?>
	<section id="slider">
		<div class="row wide">
			<div class="twelve columns np lp-slider">
				<div id="home-slide">
					<?php if( have_rows('featured_product_slider') ):
						// loop through the rows of data
						$catalog = get_field('product_category');
					    while ( have_rows('featured_product_slider') ) : the_row();
					        $mImg  = get_sub_field('background_image');
					        $mHead  = get_sub_field('headline');
					        $mSubHead  = get_sub_field('sub_headline');
					        $mCta  = get_sub_field('cta_text');
					        $mCtaLink  = get_sub_field('cta_link');
					?>
						<div class="slide centered-area" style="background-image: url(<?php echo $mImg; ?>);">
							<div class="content">
								<p class="headline nm"><?php echo $mHead; ?></p>
								<p class="small-copy nm"><?php echo $mSubHead; ?></p>
								<a href="<?php echo $mCtaLink; ?>" class="cta"><?php echo $mCta; ?></a>
							</div>
						</div>
					<?php
						endwhile;
						endif;
					?>
				</div>
			</div>
		</div>
	</section>
	<section id="breadcrumbs">
		<div class="row">
			<div class="twelve columns">
				<?php
					if ( function_exists('yoast_breadcrumb') ) {
						yoast_breadcrumb();
					}
				?>
			</div>
		</div>
	</section>
	<section>
		<div class="row">
			<div class="twelve columns center">
				<h1 class="nm h1-style"><?php the_title(); ?></h1>
				<p class="small"><?php echo get_field('main_sub_headline'); ?></p>
			</div>
		</div>
	</section>

	<section class="lp-blks">
		<div class="row">
				<?php
					$args = array( 
						'post_parent' => $post->ID,
						'post_type'   => 'page',
						'orderby'     => 'menu_order',
						'order'       => 'ASC',
						'post__in'     => array(72,73,74)
					);

					$the_query = new WP_Query( $args );
					$i = 0;
					// The Loop
					if ( $the_query->have_posts() ) {
						while ( $the_query->have_posts() ) {
							$i++;
							$the_query->the_post();
							$imgUrl = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
							if ( $i % 3 != 0 ) {
								$pad = ' added';
							} else {
								$pad = '';
							}
							if ( $i % 6 == 0 || $i % 6 == 1 ) {
								$colW = 'seven npr-mobile';
								$mar = 'tall';
								$height = 'tall';
							}
							else {
								$colW = 'five npl';
								$mar = 'msnry short';
								$height = 'short';
							}
							if( has_post_thumbnail() ) {
				?>
								<div class="<?php echo $colW.$pad; ?> columns">
									<div class="grid-bg full-size <?php echo $mar; ?>" style="background: url(<?php echo $imgUrl; ?>) no-repeat">
										<a href="<?php the_permalink(); ?>" class="contain-white <?php echo $height; ?>">
											<?php the_title(); ?>
											<span class="small"><?php echo get_field('main_sub_headline'); ?></span>
										</a>
									</div>
								</div>
				<?php
							}
						}
					}
					wp_reset_postdata();
				?>
		</div>
	</section>

	<?php 
		if ( has_post_thumbnail() ) {
			$featImg = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
		}
		$vOff = get_field('parallax_bg_vertical_offset');
	?>
	<section data-stellar-vertical-offset="<?php echo $vOff; ?>" data-stellar-background-ratio="0.5" class=" parallax landing-parallax-contain" style="background-image: url(<?php echo $featImg[0]; ?>);">
		<div class="row">
			<div class="twelve columns center landing-parallax-contain">
				<div class="contain-black">
					<?php the_content();?>
				</div>
			</div>
		</div>
	</section>
	<section class="lp-blks second">
		<div class="row">
			<div class="twelve columns center below-scroll-headline">
				<?php echo get_field('below_scroll_headline'); ?>
			</div>
				<?php
					$args = array( 
						'post_parent' => $post->ID,
						'post_type'   => 'page',
						'orderby'     => 'menu_order',
						'order'       => 'ASC',
						'post__in'     => array(75,76)
					);

					$the_query = new WP_Query( $args );
					$i = 0;
					// The Loop
					if ( $the_query->have_posts() ) {
						while ( $the_query->have_posts() ) {
							$i++;
							$the_query->the_post();
							$imgUrl = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
							if ( $i % 3 != 0 ) {
								$pad = ' added';
							} else {
								$pad = '';
							}
							if ( $i % 6 == 0 || $i % 6 == 1 ) {
								$colW = 'seven npr-mobile';
								$mar = 'tall';
								$height = 'tall';
							}
							else {
								$colW = 'five npl';
								$mar = 'msnry short';
								$height = 'short';
							}
							if( has_post_thumbnail() ) {
				?>
								<div class="<?php echo $colW.$pad; ?> columns">
									<div class="grid-bg full-size <?php echo $mar; ?>" style="background: url(<?php echo $imgUrl; ?>) no-repeat">
										<a href="<?php the_permalink(); ?>" class="contain-white <?php echo $height; ?>">
											<?php the_title(); ?>
											<span class="small"><?php echo get_field('main_sub_headline'); ?></span>
										</a>
									</div>
								</div>
				<?php
							}
						}
					}
					wp_reset_postdata();
				?>
		</div>
	</section>

<?php
get_footer();
