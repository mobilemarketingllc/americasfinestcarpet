<?php
/******************************************************************************
 *              ___   _   _   _ __     __ _   _ __    ___    ___              *
 *             / __| | | | | | '_ \   / _` | | '_ \  / __|  / _ \             *
 *             \__ \ | |_| | | | | | | (_| | | |_) | \__ \ |  __/             *
 *             |___/  \__, | |_| |_|  \__,_| | .__/  |___/  \___|             *
 *                     __/ |                 | |                              *
 *                    |___/                  |_|                              *
 *                                                                            *
 *                    m a r k e t i n g  s o l u t i o n s                    *
 ******************************************************************************/

/**
 * Contact Form 7 Tag Generator for Products field
 *
 * @author     Joseph Leedy <jleedy@synapseresults.com>
 * @category   AFCC
 * @package    AFCC_Theme
 * @copyright  Copyright 2015 Synapse Marketing Solutions (http://synapseresults.com/)
 * @license    Proprietary
 */
?>
<div id="afcc-wpcf7-tg-pane-products" class="hidden">
	<form>
		<table>
			<tr>
				<td>
					<label>
						<input type="checkbox" name="required" />&nbsp;<?php echo esc_html( __( 'Required field?', 'afcc' ) ); ?>
					</label>
				</td>
			</tr>
			<tr>
				<td>
					<label>
						<?php echo esc_html( __( 'Name', 'afcc' ) ); ?>
						<br />
						<input type="text" name="name" class="tg-name oneline" />
					</label>
				</td>
			</tr>
		</table>

		<table>
			<tr>
				<td>
					<label>
						<code>id</code> (<?php echo esc_html( __( 'optional', 'afcc' ) ); ?>)
						<br />
						<input type="text" name="id" class="idvalue oneline option" />
					</label>
				</td>

				<td>
					<label>
						<code>class</code> (<?php echo esc_html( __( 'optional', 'afcc' ) ); ?>)
						<br />
						<input type="text" name="class" class="classvalue oneline option" />
					</label>
				</td>
			</tr>
			<tr>
				<td>
					<label>
						<code>limit</code> (<?php echo esc_html( __( 'optional', 'afcc' ) ); ?>)
						<br />
						<input type="number" name="limit" class="oneline option" min="-1" max="100" />
					</label>
				</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td colspan="2">
					<label>
						<input type="checkbox" name="multiple" class="option" />&nbsp;<?php echo esc_html( __( 'Allow multiple selections?', 'afcc' ) ); ?>
					</label>
					<br />
					<label>
						<input type="checkbox" name="include_blank" class="option" />&nbsp;<?php echo esc_html( __( 'Add a blank item as the first option?', 'afcc' ) ); ?>
					</label>
				</td>
			</tr>
			<tr>
				<td>
					<label>
						<code>group_by</code> (<?php echo esc_html( __( 'optional', 'afcc' ) ); ?>)
						<br />
						<select id="group-by-options">
							<option value=""></option>
							<option value="category"><?php _e( 'Category', 'afcc' ) ?></option>
							<option value="tag"><?php _e( 'Tag', 'afcc' ) ?></option>
							<option value="attribute"><?php _e( 'Attribute', 'afcc' ) ?></option>
						</select>
					</label>
					<div id="group-by-attribute-input-container" style="display:none">
						<label>
							<?php _e( 'Attribute name:', 'afcc' ); ?>
							<br />
							<input type="text" id="group-by-attribute-input" class="oneline" />
						</label>
					</div>
					<input type="hidden" name="group_by" value="" id="group-by-input" class="option" />
				</td>
				<td>
					<label>
						<code>stock_status</code> (<?php echo esc_html( __( 'optional', 'afcc' ) ); ?>)
						<br />
						<select id="stock-status-select">
							<option value="">Any</option>
							<option value="instock"><?php _e( 'In Stock', 'afcc' ) ?></option>
							<option value="outofstock"><?php _e( 'Out of Stock', 'afcc' ) ?></option>
						</select>
						<input type="hidden" name="stock_status" class="option" id="stock-status-input" />
					</label>
				</td>
			</tr>
		</table>

		<div class="tg-tag">
			<label>
				<?php echo esc_html( __( 'Copy this code and paste it into the Form field on the left.', 'afcc' ) ); ?>
				<br />
				<input type="text" name="products" class="tag wp-ui-text-highlight code" readonly="readonly" onfocus="this.select()" />
			</label>
		</div>

		<div class="tg-mail-tag">
			<label>
				<?php echo esc_html( __( 'Copy this code and paste it into the Mail fields below.', 'afcc' ) ); ?>
				<br />
				<input type="text" class="mail-tag wp-ui-text-highlight code" readonly="readonly" onfocus="this.select()" />
			</label>
		</div>
	</form>
</div>

<script>
(function( $ ) {
	$( 'body' ).on( 'change', '#group-by-options', function( event ) {
		var $group_by_attribute_container = $( '#group-by-attribute-input-container' ),
		    $group_by_input               = $( '#group-by-input' ),
		    group_by                      = $( this ).val();

		if ( group_by === 'attribute' ) {
			$group_by_attribute_container.show();
			$group_by_input.val( '' );
		} else {
			$group_by_input.val( group_by );

			if ( $group_by_attribute_container.is( ':visible' ) ) {
				$group_by_attribute_container.hide();
			}
		}

		$group_by_input.trigger( 'change' );
	} );

	$( 'body' ).on( 'change', '#group-by-attribute-input', function( event ) {
		var value           = $( this ).val(),
		    $group_by_input = $( '#group-by-input' );

		value = value.trim();

		if ( value.length !== 0 && value.substr( 0, 3 ) !== 'pa_' ) {
			value = 'pa_' + value;
		}

		$group_by_input.val( value );
		$group_by_input.trigger( 'change' );
	} );

	$( 'body' ).on( 'change', '#stock-status-select', function( event ) {
		var $stock_status_input = $( '#stock-status-input' );

		$stock_status_input.val( $.trim( $( this ).val() ) );
		$stock_status_input.trigger( 'change' );
	} );
})( jQuery );
</script>
